import Reactotron from "reactotron-react-native"

const dirImg = "./img/"

export const Img = {

	logoBlanco: require( dirImg + "logo-hp-blanco.png" ),
	logoAzul: require( dirImg + "logo-hp-azul.png" ),
	fondoAzul: require( dirImg + "fondo-azul.jpg" ),
	fondoBlanco: require( dirImg + "fondo-blanco.jpg" ),
	imgComputo: require( dirImg + "img-computo.png" ),
	imgImpresion: require( dirImg + "img-impresion.png" ),
	imgOferta: require( dirImg + "img-oferta.png" ),
	imgComputadora: require( dirImg + "img-computadora.jpg" ),
	imgNotebooks: require( dirImg + "img-notebooks.png" ),
	imgDesktop: require( dirImg + "img-desktop.png" ),
	imgMonitores: require( dirImg + "img-monitores.png" ),
	imgWorkstation: require( dirImg + "img-workstation.png" ),
	imgPos: require( dirImg + "img-pos.png" ),
	imgCarePacks: require( dirImg + "img-carepacks.png" ),
	
}
export const img = {
	star_solid:require(dirImg + "star-solid.png"),
	star_regular:require(dirImg + "star-regular.png"),

	planetparnerts_video:require(dirImg + "planetparnerts_video.png"),
	gota:require( dirImg + "gota.png"),
	impresora_morado:require( dirImg + "impresora_morado.png"),
	triangulo:require( dirImg + "triangulo.png"),
	mundo:require( dirImg + "mundo.png"),
	
	c1_p2:require( dirImg + "c1_p2.png"),
	c2_p2:require( dirImg + "c2_p2.png"),
	c3_p2:require( dirImg + "c3_p2.png"),
	
	c1_p:require( dirImg + "c1_p.png"),
	c2_p:require( dirImg + "c2_p.png"),
	c3_p:require( dirImg + "c3_p.png"),
	
	loading:require( dirImg + "loading.gif"),
	employee_ico:require( dirImg + "employee_ico.png"),
	
	emp_ico1:require( dirImg + "emp_ico1.png"),
	emp_ico2:require( dirImg + "emp_ico2.png"),
	emp_ico3:require( dirImg + "emp_ico3.png"),
	emp_ico4:require( dirImg + "emp_ico4.png"),
	emp_ico5:require( dirImg + "emp_ico5.png"),
	emp_ico6:require( dirImg + "emp_ico6.png"),
	
	sumi_top_1:require( dirImg + "sumi_top_1.png"),
	sumi_top_2:require( dirImg + "sumi_top_2.png"),
	sumi_top_3:require( dirImg + "sumi_top_3.png"),
	sumi_top_4:require( dirImg + "sumi_top_4.png"),
	sumi_top_5:require( dirImg + "sumi_top_5.png"),
	sumi_top_6:require( dirImg + "sumi_top_6.png"),
	sumi_top_7:require( dirImg + "sumi_top_7.png"),
	sumi_top_8:require( dirImg + "sumi_top_8.png"),
	sumi_top_9:require( dirImg + "sumi_top_9.png"),
	sumi_top_10:require( dirImg + "sumi_top_10.png"),
	sumi_top_11:require( dirImg + "sumi_top_11.png"),
	sumi_top_12:require( dirImg + "sumi_top_12.png"),
	
	
	mito1:require( dirImg + "mito1.jpg"),
	mito2:require( dirImg + "mito2.jpg"),
	mito3:require( dirImg + "mito3.jpg"),
	mito4:require( dirImg + "mito4.jpg"),
	mito5:require( dirImg + "mito5.jpg"),
	
	
	sumi_paso1:require( dirImg + "sumi_paso1.png"),
	sumi_paso2:require( dirImg + "sumi_paso2.png"),
	sumi_paso3:require( dirImg + "sumi_paso3.png"),
	sumi_paso4:require( dirImg + "sumi_paso4.png"),
	
	
	checksumi:require( dirImg + "checksumi.jpg"),
	lasertjet_cartucho:require( dirImg + "lasertjet_cartucho.jpg"),
	
	ico_sumi_1:require( dirImg + "ico_sumi_1.png"),
	ico_sumi_2:require( dirImg + "ico_sumi_2.png"),
	ico_sumi_3:require( dirImg + "ico_sumi_3.png"),
	ico_sumi_4:require( dirImg + "ico_sumi_4.png"),
	ico_sumi_5:require( dirImg + "ico_sumi_5.png"),
	ico_sumi_6:require( dirImg + "ico_sumi_6.png"),
	ico_sumi_7:require( dirImg + "ico_sumi_7.png"),
	ico_sumi_8:require( dirImg + "ico_sumi_8.png"),
	ico_sumi_9:require( dirImg + "ico_sumi_9.png"),
	ico_sumi_10:require( dirImg + "ico_sumi_10.png"),
	ico_sumi_11:require( dirImg + "ico_sumi_11.png"),
	ico_sumi_12:require( dirImg + "ico_sumi_12.png"),
	ico_sumi_13:require( dirImg + "ico_sumi_13.png"),
	ico_sumi_14:require( dirImg + "ico_sumi_14.png"),
	ico_sumi_15:require( dirImg + "ico_sumi_15.png"),
	ico_sumi_16:require( dirImg + "ico_sumi_16.png"),
	ico_sumi_17:require( dirImg + "ico_sumi_17.png"),
	ico_sumi_18:require( dirImg + "ico_sumi_18.png"),
	ico_sumi_19:require( dirImg + "ico_sumi_19.png"),
	ico_sumi_20:require( dirImg + "ico_sumi_20.png"),
	ico_sumi_21:require( dirImg + "ico_sumi_21.png"),
	ico_sumi_22:require( dirImg + "ico_sumi_22.png"),
	ico_sumi_23:require( dirImg + "ico_sumi_23.png"),
	ico_sumi_24:require( dirImg + "ico_sumi_24.png"),
	ico_sumi_25:require( dirImg + "ico_sumi_25.png"),
	ico_sumi_26:require( dirImg + "ico_sumi_26.png"),
	ico_sumi_27:require( dirImg + "ico_sumi_27.png"),



	boton_impresion_hp:require( dirImg + "boton-impresion-hp.jpg"),
	boton_officejet:require( dirImg + "boton_officejet.jpg"),
	boton_laserjet:require( dirImg + "boton_laserjet.jpg"),	
	boton_pagewide:require( dirImg + "boton_pagewide.jpg"),
	
	boton_officejet_interna:require( dirImg + "boton_officejet-interna.jpg"),
	boton_pagewide_interna:require( dirImg + "boton_pagewide-interna.jpg"),
	boton_laserjet_interna:require( dirImg + "boton_laserjet-interna.jpg"),
	
	



	interrogacion:require( dirImg + "interrogacion.png"),
	resultados_imp:require( dirImg + "resultados_imp.jpg"),
	decicion_maker_1:require( dirImg + "decicion_maker_1.jpg"),
	decicion_maker_2:require( dirImg + "decicion_maker_2.jpg"),
	decicion_maker_3:require( dirImg + "decicion_maker_3.jpg"),
	decicion_maker_4:require( dirImg + "decicion_maker_4.jpg"),
	decicion_maker_5:require( dirImg + "decicion_maker_5.jpg"),
	decicion_maker_6:require( dirImg + "decicion_maker_6.jpg"),
	
	
	
	favoritos:require( dirImg + "favoritos.jpg"),
	comparar_bg:require( dirImg + "comparar_bg.jpg"),
	
	
	print_top:require( dirImg + "print_top.jpg"),
	ico_print_3:require( dirImg + "ico_print_3.png"),
	ico_print_2:require( dirImg + "ico_print_2.png"),
	ico_print_1:require( dirImg + "ico_print_1.png"),
	heart_w:require( dirImg + "heart_w.png"),
	heart:require( dirImg + "heart.png"),
	chevron:require( dirImg + "chevron.png"),
	lupa:require( dirImg + "lupa.png"),
	ico_cot:require( dirImg + "ico_cot.png"),
	ico_cot_in:require( dirImg + "ico_cot_in.png"),
	ico_cd:require( dirImg + "ico_cd.png"),
	ico_hd:require( dirImg + "ico_hd.png"),
	
	
	
	home_bg:require( dirImg + "home_bg.jpg"),
	encuentra:require( dirImg + "encuentra.png"),
	
	
	ico_home_2:require( dirImg + "ico_home_2.png"),
	ico_home_1:require( dirImg + "ico_home_1.png"),
	impresion_btn:require( dirImg + "impresion_btn.jpg"),
	computo_btn:require( dirImg + "computo_btn.jpg"),
	
	publicidad1: require( dirImg + "publicidad1.jpg" ),
	publicidad2: require( dirImg + "publicidad2.jpg" ),
	publicidad3: require( dirImg + "publicidad3.jpg" ),
	publicidad4: require( dirImg + "publicidad4.jpg" ),
	
	cat1: require( dirImg + "cat1.jpg" ),
	cat2: require( dirImg + "cat2.jpg" ),
	cat3: require( dirImg + "cat3.jpg" ),
	cat4: require( dirImg + "cat4.jpg" ),
	cat5: require( dirImg + "cat5.jpg" ),
	cat6: require( dirImg + "cat6.jpg" ),
	

	top7: require( dirImg + "HP-ScanJet-Pro.jpg" ),
	top8: require( dirImg + "HP-ScanJet-Enterprise.jpg" ),
	top9: require( dirImg + "HP-Suministros.jpg" ),
	
	top10: require( dirImg + "HP-Notebook.jpg" ),
	top11: require( dirImg + "HP-Desktop.jpg" ),
	top12: require( dirImg + "HP-Monitores.jpg" ),
	top13: require( dirImg + "HP-Workstations.jpg" ),
	top14: require( dirImg + "HP-POS.jpg" ),
	top15: require( dirImg + "HP-Carepacks.jpg" ),
	

	impq1: require( dirImg + "imp1.jpg" ),
	impq2: require( dirImg + "imp2.jpg" ),
	impq3: require( dirImg + "imp3.jpg" ),
	impq4: require( dirImg + "imp4.jpg" ),
	impq5: require( dirImg + "imp5.jpg" ),
	impq6: require( dirImg + "imp6.jpg" ),
	impq7: require( dirImg + "imp7.jpg" ),
	impq8: require( dirImg + "imp8.jpg" ),
	impq9: require( dirImg + "imp9.jpg" ),
	
	plus: require( dirImg + "plus.png" ),
	impresora: require( dirImg + "impresora.png" ),
	pantalla: require( dirImg + "pantalla.png" ),
	escanee: require( dirImg + "escanee.png" ),
	impresiones: require( dirImg + "impresiones.png" ),
	
	toner: require( dirImg + "toner.png" ),
	printer: require( dirImg + "printer.png" ),
	comparar: require( dirImg + "comparar.png" ),
	usb_ico: require( dirImg + "usb_ico.png" ),
	specs1: require( dirImg + "specs1.jpg" ),
	specs2: require( dirImg + "specs2.jpg" ),
	specs3: require( dirImg + "specs3.jpg" ),
	specs4: require( dirImg + "specs4.jpg" ),
	specs5: require( dirImg + "specs5.jpg" ),
	specs6: require( dirImg + "specs6.jpg" ),
	specs7: require( dirImg + "specs7.jpg" ),
	
	winPro: require( dirImg + "windows_pro.png" ),
	topImpresion: require( dirImg + "top_impresion.png" ),
	logoBlanco: require( dirImg + "logo-hp-blanco.png" ),
	logoAzul: require( dirImg + "logo-hp-azul.png" ),
	fondoAzul: require( dirImg + "fondo-azul.jpg" ),
	fondoBlanco: require( dirImg + "fondo-blanco.jpg" ),
	imgComputo: require( dirImg + "img-computo.png" ),
	imgImpresion: require( dirImg + "img-impresion.png" ),
	imgOferta: require( dirImg + "img-oferta.png" ),
	imgComputadora: require( dirImg + "img-computadora.jpg" ),
	imgNotebooks: require( dirImg + "img-notebooks.png" ),
	imgDesktop: require( dirImg + "img-desktop.png" ),
	imgMonitores: require( dirImg + "img-monitores.png" ),
	imgWorkstation: require( dirImg + "img-workstation.png" ),
	imgPos: require( dirImg + "img-pos.png" ),
	imgCarePacks: require( dirImg + "img-carepacks.png" ),
	hp_nav: require( dirImg + "hp_logo_blue.png" ),
	battery: require( dirImg + "ico_battery.png" ),
	care_ico1: require( dirImg + "care_ico1.png" ),
	care_ico2: require( dirImg + "care_ico2.png" ),
	care_ico3: require( dirImg + "care_ico3.png" ),
	care_ico4: require( dirImg + "care_ico4.png" ),
	care_ico5: require( dirImg + "care_ico5.png" ),
	care_ico6: require( dirImg + "care_ico6.png" ),
	care_ico7: require( dirImg + "care_ico7.png" ),
	ico_cotizacion: require( dirImg + "ico_cotizacion.png" ),
	ico_cotizacion_in: require( dirImg + "ico_cotizacion_in.png" ),
	ico_impresora: require( dirImg + "ico_impresora.png" ),
	ico_settings: require( dirImg + "ico_settings.png" ),
	ico_tinta: require( dirImg + "ico_tinta.png" ),
	ico_search: require( dirImg + "ico_search.png" ),
	ico_comparar: require( dirImg + "ico_comparar.png" ),
	carepacks: require( dirImg + "carepacks.png" ),
	ico1: require( dirImg + "ico1.png" ),
	ico2: require( dirImg + "ico2.png" ),
	ico3: require( dirImg + "ico3.png" ),
	ico4: require( dirImg + "ico4.png" ),
	ico5: require( dirImg + "ico5.png" ),
	ico6: require( dirImg + "ico6.png" ),
	ico7: require( dirImg + "ico7.png" ),
	ico8: require( dirImg + "ico8.png" ),
	ico9: require( dirImg + "ico9.png" ),
	ico10: require( dirImg + "ico10.png" ),

	ico_spec1: require( dirImg + "ico_spec1.png" ),
	ico_spec2: require( dirImg + "ico_spec2.png" ),
	ico_spec3: require( dirImg + "ico_spec3.png" ),

	top_img: require( dirImg + "top_img.png" ),

}
