import _inicio from './Inicio';
export const Inicio = _inicio;

import _navbar from './Navbar';
export const Navbar = _navbar;

import _featuresbar from './FeaturesBar';
export const FeaturesBar = _featuresbar;

import _bottomBar from './BottomBar';
export const BottomBar = _bottomBar;

import _DrawerContent from './DrawerContent';
export const DrawerContent = _DrawerContent;

import _Selectable from './Selectable';
export const Selectable = _Selectable;

import _BackgroundView from './BackgroundView';
export const BackgroundView = _BackgroundView;

import _Logo from './Logo';
export const Logo = _Logo;

import _Register from './Register';
export const Register = _Register;

//import _RegisterForm from './RegisterForm';
//export const RegisterForm = _RegisterForm;

import _SearchList from './SearchList';
export const SearchList = _SearchList;

import _SearchItem from './SearchItem';
export const SearchItem = _SearchItem;

import _GridEmployee from './GridEmployee';
export const GridEmployee = _GridEmployee;

import _Grid from './Grid';
export const Grid = _Grid;
