import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  Platform,
  Dimensions,
  Image,
  TouchableOpacity
} from 'react-native'
import Reactotron from 'reactotron-react-native'

import { Img } from "@media"
import _style from "./style"
import { img } from "@media"

const {height, width} = Dimensions.get('window'); 
const aspectRatio = height/width;

var sizeFont = 15;
var sizeBigFont = 18 
if(aspectRatio>1.6) {

   // Code for Iphone

}
else {
	sizeFont = 22;
	sizeBigFont = 26;
   // Code for Ipad

}

export default class Grid extends Component {
	check(novedosos,idcategoria){
		for(var i=0;i<novedosos.length;i++){
			if(this.props.title=="Cómputo"){
				if(Number(novedosos[i].idcategoria)==Number(idcategoria)){
					return true;
				}
			}else{
				if(Number(novedosos[i].idproductogrupo)==Number(idcategoria)){
					return true;
				}
			}
				
		}
		return false;
	}
	content(){
		const{
			list,
			cols,
			novedosos,
			gap,
      navigation
		} = this.props

		if (list && list.length) {

	var boxtitle = (<View/>);
	
	
	
	
      return list.map( item => {
				return(<View  key={ item.key } style={[ _style.listItem, { flexBasis:(cols==3)?"33.3%":"50%", padding: (cols==3)?2:6 } ]}>
				
				
				
            <TouchableOpacity style={ _style.listItemContainer } onPress={()=>{
              
              if(item.key=='encuentra'){
	              
	              if(global.user.pais!='Colombia'){
		               this.props.onEncuentra();
	              }else{
		               this.props.onPlanetPartner();
	              }
	             
              }else{
	              this.props.onNext(item);
              }
              //
            }}>
            <ImageBackground
              style={ {flex:1 } }
              source={ item.image }
              resizeMode="cover">
              
              {item.key!='encuentra' &&
              <View style={{flex:1,justifyContent:'flex-end',alignItems:'center',backgroundColor:(cols==3)?'rgba(0,0,0,0.35)':'rgba(0,0,0,0)'}}>
              
              		<View style={{marginBottom:10,backgroundColor:'rgba(0,0,0,0.4)',borderColor:'white',
								borderWidth:(item.key!='encuentra')?0.5:0,paddingTop:(item.key!='encuentra')?5:0,paddingLeft:(item.key!='encuentra')?10:0,paddingRight:(item.key!='encuentra')?10:0,paddingBottom:(item.key!='encuentra')?3:0}}>
							<Text style={{color:'white',fontWeight:'200',fontSize:(cols==3)?sizeFont:sizeBigFont,lineHeight:(cols==3)?sizeFont:sizeBigFont,
								fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{(item.key!='encuentra')?item.title.toUpperCase():''}
								</Text>
					</View>
              </View>
              }
			  {item.key=='encuentra' &&
				  
				 
	            <View  style={{flex:1}} >
	            
	            
	            
	            {global.user.pais!='Colombia' ? (
					  
					  <View style={{flex:1,alignItems:'center',backgroundColor:'white'}}>
	            <View style={{ backgroundColor:'transparent', width:70, height:55,marginLeft:60,marginTop:10 }}>
	            	<Image style={{width:70,height:55,resizeMode:'contain'}} source={ img.interrogacion } />
	            </View>
	            <Text style={{color:'#6b6b6b',fontWeight:'200',width:110, backgroundColor:'transparent',fontSize:14,lineHeight:14,marginRight:18,
		            fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'ENCUENTRA\nTU IMPRESORA IDEAL'}</Text>
				</View>
					  
					  ):(
						  
						  <View style={{flex:1,alignItems:'center',backgroundColor:'#bf951e'}}>
	            <View style={{ backgroundColor:'transparent', width:55, height:55,marginLeft:80,marginTop:10,marginBottom:10 }}>
	            	<Image style={{width:55,height:55,resizeMode:'contain'}} source={ img.triangulo } />
	            </View>
	            <Text style={{color:'#fff',fontWeight:'200',width:110, backgroundColor:'transparent',fontSize:14,lineHeight:14,marginRight:18,
		            fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'DESCUBRE LO NUEVO DE HP PLANET PARTNER'}</Text>
				</View>
						  
					  )
				
				}
				
				
	            
	            	
	            
	            </View>
				
			}
				
				
            </ImageBackground>
            
            
            
            </TouchableOpacity>
            {this.check(novedosos,item.idcategoria) && (
					
					<View style={{left:6,top:6,position:'absolute',width:50,height:50,backgroundColor: 'transparent'}}>
							<View style={{
							    width: 0,
							    height: 0,
							    backgroundColor: 'transparent',
							    borderStyle: 'solid',
							    borderRightWidth: 50,
							    borderTopWidth: 50,
							    borderRightColor: 'transparent',
							    borderTopColor: '#a53894',
							    position:'absolute'
							  }}></View>
							  <Image style={{marginLeft:4,marginTop:4,width:20,height:20,resizeMode:'contain'}} source={ img.star_regular } />
					</View>
				)}
				</View>);

			})


		}
	}


	render() {
		const{
			list,
			cols,
			gap,
			style
		} = this.props

		return (<View style={_style.container}>
					<View style={{ flex:1,marginBottom: 0,alignContent: "stretch",
						flexWrap: "wrap",flexDirection: "row"}}>
						{this.content()}
					</View>

				</View>)

	}
}
/*
{this.content()}
*/
