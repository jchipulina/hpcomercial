import React, { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
	container: {
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: "center",
		backgroundColor:'red'
	},
	image:{
		minHeight: 144,
		width: 144,
		resizeMode: "contain",
	},
	text:{
		alignSelf: "stretch",
		color: "#000",
		fontSize: 20,
		textAlign: "center",
		marginBottom: 32,
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		lineHeight:20,
	},
	invertedText:{
		color: "#FFFFFF"
	}
})
