import React, { Component } from "react";
import {
  View,
  Image,
  Text
} from 'react-native';
//import { Img } from "@media"
import { img } from "@media"
import style from "./style"


export default class Logo extends Component {

	render() {
		const {
			invert,
			small,
			containerStyle,
			imageStyle,
			invertedText,
			textStyle
		} = this.props
		/*
		let img = Img.logoAzul

		if ( invert ) {
			img = Img.logoBlanco
		}
		*/
		if(small){
			
		}
		
		return (
				<View
					style={[ style.container, containerStyle ]}>
					<Image
						style={[ style.image, imageStyle ]}
						source={ img.hp_nav }/>
					<Text
						style={[
							style.text,
							invert && (style.invertedText),
							textStyle
						]}>
						{"SOLUCIONES DE CÓMPUTO\nE IMPRESIÓN EMPRESARIAL"}
					</Text>
		        </View>)
	}
}
