import React, { StyleSheet } from "react-native";

export default StyleSheet.create({
	container: {
		flexWrap:'wrap',width:'100%',backgroundColor:'white', flexDirection:'row'
	},
	item:{
		flexGrow: 0,
		flexBasis: "33.333%",
		opacity: 0.4,
		flex: 1,
		alignItems: "center",
		justifyContent: "center"

	},
	itemSelected:{
		opacity: 1
	}
})
