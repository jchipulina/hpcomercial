import React, { Component } from "react";
import {
  View,
  Image,
  Text,
	TouchableHighlight
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import style from "./style"
import Dash from 'react-native-dash';
import FastImage from 'react-native-fast-image'

export default class Selectable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selected : 0
		}
	}
	
	render() {
		var boxStyle = {flex:1,width:'100%',backgroundColor:'white',flexDirection:'row'};
		var labelStyle = {flex:1,marginLeft:5,marginRight:5,marginTop:5,marginBottom:0,height:25,textAlign:'center',fontSize:9,color:'#87898b',fontFamily:'HP Simplified',backgroundColor:'white' };
		return (<View style={ style.container }>
			{
				this.props.items.map( ( item, id ) =>{
					return (<TouchableHighlight
					        key={id}
					        style={[
					        	style.item,
					        	((this.state.selected==id)?style.itemSelected:null)
					        ]}
									onPress={ () => {
										this.setState({ selected:id });
										this.props.setAccesorio(id);
										//console.log( "selected", id )
									}}>
									
									
							<View  key={0} style={boxStyle}>
				                <View style={{width:'100%',alignItems: "center", justifyContent: "center"}}>
				                
				    					
				    						<FastImage
                	  style={{width:'100%',height:60,marginTop:0,backgroundColor:'white' }}
					  source={{
					    uri: item.item_img,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
				    						
				    						
											<Text style={[labelStyle,{color:(this.state.selected==id)?'#1259a1':'black'}]}>{item.label}</Text>
											<Dash style={{width:'100%', height:1}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={4}/>	
				                </View>
				                <Dash style={{flexDirection:'column',width:1, height:78}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={4}/>
				
				    		</View>
				    		
					</TouchableHighlight>)
				})
			}
			</View>)
	}
}
