import React, { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: "stretch",
	},
	title:{
		color: "#105FA6",
		paddingTop: 8,
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		fontWeight:'bold',
		paddingBottom: 8,
		paddingLeft: 16,
		paddingRight: 16,
		backgroundColor: "#e5f7fc",
	},
	result:{
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 16,
		paddingRight: 16,
		color: "#575757",
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
	},
	content:{
		paddingTop: 16,
		paddingBottom: 16
	},
	divider:{
		flexDirection: "column"
	}
})
