import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Platform
} from 'react-native';
import Dash from 'react-native-dash'
import { Img } from "@media"
import _style from "./style"
import FastImage from 'react-native-fast-image'
import Reactotron from 'reactotron-react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
let {width, height} = Dimensions.get('window')
export default class SearchList extends Component {

	constructor(){
		super();
		this.state = {
			showButtonUp:false
		}
	}
	content(list){
		var {navigation} = this.props;
		if (list.length>0) {
			var boxStyle = {flex:1,backgroundColor:'transparent',flexDirection:'row'};
			
			var btnUp = (<View/>);
			if(this.state.showButtonUp){
				btnUp = (<TouchableOpacity onPress={()=>{ 
					this.scrollView.scrollTo({y: 0})
					
				}} 
				style={{width:30, height:30,borderRadius:15,justifyContent:'center',alignItems:'center',
					backgroundColor:'rgba(0,0,0,0.5)',position:'absolute',bottom:100,right:10}}>
					<Icon name={'angle-up'} size={15} color="#fff"/>
				</TouchableOpacity>);
			}
			
			return(<View style={{flex:1}}>

			<ScrollView
				ref={ref => this.scrollView = ref}
				onScroll={(event)=>{
					Reactotron.log(event.nativeEvent.contentOffset.y);
					if(event.nativeEvent.contentOffset.y>height){
						this.setState({showButtonUp:true});
					}else{
						this.setState({showButtonUp:false});
					}
				}}
				contentContainerStyle={ _style.content }>
				
				
				
				
				{list.map(r => <View  key={r.productob.id} style={boxStyle}>
                <TouchableOpacity onPress={()=>{
	                
                  navigation.navigate('ItemProfileProduct',{type:r.productob.tipo,producto:r.productob});
                  
                }} style={{flex:1, alignItems:'center',backgroundColor:'transparent',flexDirection:'column'}}>
               
    				<View style={{flex:1,justifyContent:'center', alignItems:'center',backgroundColor:'transparent',flexDirection:'row'}}>
    				
    						
    				<View style={{width:100,height:100,marginRight:5}}>
	    				<FastImage style={{width:100,height:100,flex:1,marginTop:0}} source={{
						    uri: r.productob.imagenes[0].imagen,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }} resizeMode={FastImage.resizeMode.contain} />
    				</View>		
    				<View style={{width:160,height:100,flexDirection:'column',backgroundColor:'transparent',justifyContent:'center'}}>
    				
    				<Text style={{marginTop:0,fontSize:14, fontWeight:'400',
	                color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.productob.name}</Text>
	                
					<Text style={{marginBottom:0,fontSize:11,
					color:'#868686',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{(r.productob.tipo == 'Cómputo')?"Cómputo":"Impresión"}</Text>

    				</View>			
				</View>
				<View style={{width:'80%',backgroundColor:'#999',height:0.5}}></View>
				
				
				<View style={{height:100,width:50, backgroundColor:'white',justifyContent:'center',
					alignItems:'center',position:'absolute',top:0,right:0}}>
					<Icon name={'angle-right'} size={15} color="#0096d6"/>
				</View>

                </TouchableOpacity>
						
    					</View>)}


			</ScrollView>
			{btnUp}
			
			</View>)
		}
		else{
			return <View></View>
		}
	}
	render() {
		const {
			style,
			list,
			title,
			...props
		} = this.props

		return (<View
		        	{...props}
					style={[ _style.container, style ]}>
					{this.content(list)}
				</View>)
	}
}
