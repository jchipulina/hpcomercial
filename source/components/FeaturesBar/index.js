import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Platform,
	Dimensions,
	ScrollView,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
import FastImage from 'react-native-fast-image'

var {height, width} = Dimensions.get('window');

var hHeight = 130;
export default class FeaturesBar extends Component{
	constructor(props) {
    	super(props);
  	}
	render() {

		var center = (<View/>);
		var title = this.props.title;

		var boxContainerStyle = {flex:1,backgroundColor:'#ebf9ff', flexDirection:'row'};
		var boxStyle = {width:75, justifyContent:'center', alignItems:'center', flexDirection:'row'};
		var labelStyle = {flex:1,marginTop:0,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		var separatorStyle = {width:1, height:60,borderWidth:1, borderColor: "#ccc", borderStyle: "dotted"};

		var icoStyle = {width:40,height:40,resizeMode:'contain',tintColor:'#1259a1'};
		
		var styleIco = {width:40,height:40,resizeMode:'contain',tintColor:'#1259a1', backgroundColor:'transparent'}
		
		if(this.props.specs.length>0){
			
		}

		/*
		var settings = new Array();
		
	
		
		if(this.props.type=="computo"){
			settings.push({id:1,label:"12,3'",icon:'file-pdf-o',ico:0});
			settings.push({id:2,label:'Dos PRO 10',icon:'file-pdf-o',ico:1});
			settings.push({id:3,label:'Intel Core i7\n7600U',icon:'file-pdf-o',ico:3});
			settings.push({id:4,label:'360 GB',icon:'file-pdf-o',ico:2});
			settings.push({id:5,label:'16 GB\nde SDRAM',icon:'file-pdf-o',ico:4});
		}else{
			settings.push({id:1,label:'45000 pag',icon:'file-pdf-o',ico:5});
			settings.push({id:2,label:'35000 pag',icon:'file-pdf-o',ico:6});
			settings.push({id:3,label:'45000 pag',icon:'file-pdf-o',ico:7});
			settings.push({id:4,label:'45000 pag',icon:'file-pdf-o',ico:8});
			settings.push({id:5,label:'45000 pag',icon:'file-pdf-o',ico:9});
		}
		*/
		
		

		return (
			<View style={ {width:width,height:hHeight,
				paddingTop:0,backgroundColor:'#0096d6',justifyContent:'flex-start'} }>

				<TouchableOpacity onPress={()=>{
					this.props.onViewPDF();
				}} style={{height:30,justifyContent:'center', alignItems:'center', flexDirection:'row',backgroundColor:'#1259a1'}}>

					<Icon name={'file-pdf-o'} size={15} color={'white'} />
					<Text style={{color:'white',marginLeft:8,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Ver PDF'}</Text>
				</TouchableOpacity>

				<ScrollView horizontal={true} style={{backgroundColor:'#ebf9ff', flexDirection:'row'}}>
				<View style={boxContainerStyle}>
					{this.props.specs.map(r => <View  key={r.id} style={boxStyle}>
						<View style={{flex:1,paddingTop:15,justifyContent:'center', alignItems:'center', backgroundColor:'transparent'}}>
						<View style={{flex:1,justifyContent:'center', alignItems:'center',backgroundColor:'transparent'}}>
							<FastImage
		                	  style={{width:40,height:40}}
							  source={{ uri: r.especificacion, headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
						</View>
						<Text style={labelStyle}>{r.valor}</Text>
						</View>
						<View style={separatorStyle}/>
					</View>)}
					</View>
				</ScrollView>
			</View>
		);
	}
}
