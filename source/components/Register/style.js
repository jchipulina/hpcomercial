import React, { StyleSheet } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "stretch",
		backgroundColor:'transparent'
	},
	inputContainerInput: {
		width: "100%",fontFamily:'HP Simplified',
		height: 40, marginBottom: 8,borderColor: 'gray',
		borderWidth: 0,backgroundColor:'#F3F3F3',padding:10
	},
	input: {
		padding: 4,
		fontSize: 18,
	}
})
