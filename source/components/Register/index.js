import React, { Component } from "react";
import {
  View,
  Text,
  Alert,
  TextInput
} from 'react-native';
import { Input } from 'react-native-elements';
import style from "./style"
import {Select, Option} from "react-native-chooser";
import EvilIcons  from 'react-native-vector-icons/EvilIcons';
import Reactotron from 'reactotron-react-native';
var parseString = require('react-native-xml2js').parseString;
var paises = new Array();

var paises2 = new Array();
paises2.push({id:1});
paises2.push({id:2});
paises2.push({id:3});
paises2.push({id:4});
paises2.push({id:5});
paises2.push({id:6});

var scope;
var pais = "";
var tipocliente = "";
export default class Register extends Component {
	
  constructor(props) {
    super(props);
    scope = this;
    this.state = {value:"País",value2:"Tipo de cliente",paises:[],nombres:'Test',apellidos:'Test',email:'test@test.com'}
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  msg(msg){
	  Alert.alert(
		  'Alert Title',
		  msg,
		  [
		    {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
		    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
		    {text: 'OK', onPress: () => console.log('OK Pressed')},
		  ],
		  { cancelable: false }
		)
  }
  async registrarDatos () {
	  
	  	if(pais==""){
		  	this.msg("Seleccione su pais");
		  	return;
	  	}
	  	if(tipocliente==""){
		  	this.msg("Tipo de cliente es requerido");
		  	return;
	  	}
	  	if(this.state.nombres==""){
		  	this.msg("Ingrese sus nombres");
		  	return;
	  	}
	  	if(this.state.apellidos==""){
		  	this.msg("Ingrese sus apellidos");
		  	return;
	  	}
	  	if(this.state.email==""){
		  	this.msg("Email es requerido");
		  	return;
	  	}
	  	if(!this.validateEmail(this.state.email)){
		  	this.msg("Email invalido");
		  	return;
	  	}
        try {
	        var headers = {'Content-Type': 'text/xml;charset=UTF-8','SOAPAction':'"http://tempuri.org/IHPServicios/Usuario_Registro"'};
			var body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
			   body += '<soapenv:Header/>';
			   body += '<soapenv:Body>';
			      body += '<tem:Usuario_Registro>';
			         body += '<tem:int_IdPais>'+pais+'</tem:int_IdPais>';
			         body += '<tem:int_IdTipoCliente>'+tipocliente+'</tem:int_IdTipoCliente>';
			         body += '<tem:var_Nombres>'+this.state.nombres+'</tem:var_Nombres>';
			         body += '<tem:var_Apellidos>'+this.state.apellidos+'</tem:var_Apellidos>';
			         body += '<tem:var_Email>'+this.state.email+'</tem:var_Email>';
			      body += '</tem:Usuario_Registro>';
			   body += '</soapenv:Body>';
			body += '</soapenv:Envelope>';

			const response = await fetch(global.servicios,{method: "POST",headers:headers,body:body});
            parseString(response._bodyText, function (err, result) {
	            var rpta = result['s:Envelope']['s:Body'][0].Usuario_RegistroResponse[0].Usuario_RegistroResult;
	            var resp = JSON.parse(rpta);
			    Reactotron.log(resp)
			});

        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
  componentWillMount(){
							fetch( global.api+'/Pais_Listado')
						        .then(response => response.text())
						        .then((response) => {
						            parseString(response, function (err, result) {
						                var countries = JSON.parse(result.string._);
						                for(var i=0;i<countries.length;i++){
							                paises.push({id:countries[i].pais.id,value:countries[i].pais.id,name:countries[i].pais.pais});
						                }
						                scope.setState({paises:paises});
						            });
						        }).catch((err) => {
						            Reactotron.log("message : "+err.message)
						        })
  }
  onSelect(item) {
   	pais = item.value;
   	Reactotron.log("value : "+item.value)
  }
  onSelect2(item) {
	  
	 tipocliente = item.value;
	 Reactotron.log("tipocliente : "+item.value)
  }
	render() {
		const {
			containerStyle
		} = this.props
		var optionStyle = {fontFamily:'HP Simplified',color:'#575757'};
		var optBorder = {borderBottomWidth:0.3, borderBottomColor:'#e0e1e1'};
		

		return (<View style={[ style.container, containerStyle ]}>
					<Select 	onSelect = {this.onSelect.bind(this)}
					            defaultText  = {this.state.value}
					            style = {{backgroundColor:'#F3F3F3',borderWidth:0,width: "100%",marginBottom: 8,padding:0,paddingLeft:10}}
					            
					            textStyle = {{fontFamily:'HP Simplified',color:'#575757'}}
					            transparent = {true}
					            backdropStyle  = {{backgroundColor : 'rgba(0,0,0,0.6)'}}
					            indicatorIcon = {(<View style={{justifyContent:'center',width:40,height:40,
								padding:0,backgroundColor:'#a53894'}}>
								
            	<EvilIcons name={'chevron-down'} size={40} color="#FFF"/>
            </View>)}
            optionListStyle = {{backgroundColor : "#f6f7f7",borderWidth:1,borderColor:'#F3F3F3'}}
          >
		  {this.state.paises.map(r => <Option key={r.id} value = {{value:r.id}} styleText={optionStyle} style={optBorder}>{r.name}</Option>)}
	    		
        </Select>

        <Select
            onSelect = {this.onSelect2.bind(this)}
            defaultText  = {this.state.value2}
            style = {{backgroundColor:'#F3F3F3',borderWidth:0,width: "100%",marginBottom: 8,padding:0,paddingLeft:10}}
            textStyle = {{fontFamily:'HP Simplified',color:'#575757'}}
            transparent = {true}
            backdropStyle  = {{backgroundColor : 'rgba(0,0,0,0.6)'}}
            indicatorIcon = {(<View style={{justifyContent:'center',width:40,height:40,
	            padding:0,backgroundColor:'#a53894'}}>
            	<EvilIcons name={'chevron-down'} size={40} color="#FFF"/>
            </View>)}
            optionListStyle = {{backgroundColor : "#f6f7f7",borderWidth:1,borderColor:'#F3F3F3'}}
          >
          <Option value = {{value : 1}} styleText={optionStyle} style={optBorder}>{'Cliente'}</Option>
          <Option value = {{value : 2}} styleText={optionStyle} style={optBorder}>{'Distribuidor'}</Option>
        </Select>


          <TextInput placeholder={'Nombres'}
          style={style.inputContainerInput}   />

          <TextInput placeholder={'Apellidos'}
          style={style.inputContainerInput}   />

          <TextInput placeholder={'Email'}
          style={style.inputContainerInput}   />


					<Text style={{fontFamily:'HP Simplified',color:'#575757'}}>(*) Completar los datos obligatorios</Text>
		        </View>)
	}
}
