
import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';


var {height, width} = Dimensions.get('window');
var pBottom = 0;
var hHeight = 55;
var currentPregunta = 0;
export default class BottomBar extends Component{
	constructor(props) {
    	super(props);
    	if(this.isIphoneX()){
	    	hHeight = 80
	    	pBottom = 25;
    	}
  	}
  	isIphoneX() {
	    const dimen = Dimensions.get('window');
	    return (
	        Platform.OS === 'ios' &&
	        !Platform.isPad &&
	        !Platform.isTVOS &&
	        (dimen.height === 812 || dimen.width === 812)
	    );
	}
	render() {

		var center = (<View/>);
		var title = this.props.title;
		var btnCancelar  = (<View/>);
		var btnComputo = (<View/>);
		var btnImpresion = (<View/>);
		var btnSearch = (<View/>);
		var btnComparar = (<View/>);
		var btnCancelar = (<View/>);
		var btnEnviarCotizacion = (<View/>);
		var btnSiguiente = (<View/>);
		
		if(this.props.onSiguiente!=null){
			btnSiguiente = (<TouchableOpacity style={{backgroundColor:'#0096d6',alignItems:'center',flex:1
			,justifyContent:'center'}} onPress={()=>{ this.props.onSiguiente(); }}>
				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'SIGUIENTE'}</Text>
			</TouchableOpacity>);
		}
		
		
		if(this.props.onCancelar!=null){
			btnCancelar = (<TouchableOpacity style={{backgroundColor:'#999',alignItems:'center',flex:1
			,justifyContent:'center'}} onPress={()=>{ this.props.onCancelar(); }}>
				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'CANCELAR'}</Text>
			</TouchableOpacity>);
		}
		if(this.props.onComputo!=null){
			btnComputo = (<TouchableOpacity style={{backgroundColor:'#0096d6',alignItems:'center',flex:1
			,justifyContent:'center'}} onPress={()=>{ this.props.onComputo(); }}>
				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'IR A CÓMPUTO'}</Text>
			</TouchableOpacity>);
		}
		if(this.props.onImpresion!=null){
			btnImpresion = (<TouchableOpacity style={{backgroundColor:'#0096d6',alignItems:'center',flex:1
			,justifyContent:'center'}} onPress={()=>{ this.props.onImpresion(); }}>
				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'IR A IMPRESORAS'}</Text>
			</TouchableOpacity>);
		}
		
		if(this.props.onCancelar!=null){
			btnCancelar = (<TouchableOpacity style={{backgroundColor:'#999999',alignItems:'center',flex:1
			,justifyContent:'center'}} onPress={()=>{ this.props.onCancelar(); }}>
				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'CANCELAR'}</Text>
			</TouchableOpacity>);
		}
		if(this.props.onEnviarCotizacion!=null){
			btnEnviarCotizacion = (<TouchableOpacity style={{backgroundColor:'#0197d6',alignItems:'center',flex:1
			,justifyContent:'center'}} onPress={()=>{ this.props.onEnviarCotizacion(); }}>
				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'COTIZAR'}</Text>
			</TouchableOpacity>);
		}
		
		if(this.props.onSearch!=null){
		btnSearch = (<TouchableOpacity style={{width:60, backgroundColor:'#a53894',alignItems:'center'
		,justifyContent:'center',borderLeftWidth:2,borderLeftColor:'white'}} onPress={()=>{ this.props.onSearch(); }}>
			<Image style={{width:25,height:25,resizeMode:'contain',tintColor:'white'}} source={ img.lupa } />
		</TouchableOpacity>);
		}
		if(this.props.onComparar!=null){
		btnComparar = (<TouchableOpacity style={{flex:1,flexDirection:'row',backgroundColor:'#0096d6',alignItems:'center'
		,justifyContent:'center'}} onPress={()=>{ this.props.onComparar(); }}>
			<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'white',marginRight:15}} source={ img.ico_comparar } />
			<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:20}}>{'COMPARAR EQUIPOS'}</Text>
		</TouchableOpacity>);
		}
		if(this.props.onAnalisisCompetencia!=null){
		btnComparar = (<TouchableOpacity style={{flex:1,flexDirection:'row',backgroundColor:'#0096d6',alignItems:'center'
		,justifyContent:'center'}} onPress={()=>{ this.props.onAnalisisCompetencia(); }}>
			<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'white',marginRight:15}} source={ img.ico_comparar } />
			<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:20}}>{'ANÁLISIS COMPETENCIA'}</Text>
		</TouchableOpacity>);
		}
		if(this.props.onImpresoraIdeal!=null){
		btnComparar = (<TouchableOpacity style={{flex:1,flexDirection:'row',backgroundColor:'#0096d6',alignItems:'center'
		,justifyContent:'center'}} onPress={()=>{ this.props.onImpresoraIdeal(); }}>
			
			<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:20}}>{'ENCUENTRA TU IMPRESORA IDEAL'}</Text>
		</TouchableOpacity>);
		}
		
		return (
			<View style={ {width:width,height:hHeight,flexDirection:'row',
				paddingTop:0,backgroundColor:'#0096d6',justifyContent:'flex-start',paddingBottom:pBottom} }>

				{btnSiguiente}
				{btnComparar}
				{btnCancelar}
				{btnComputo}
				{btnImpresion}
				{btnSearch}
				{btnEnviarCotizacion}
			</View>
		);
	}
}
