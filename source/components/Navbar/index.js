import React, { Component } from "react"
import {
	View,
	Text,
	Image,
	Dimensions,
	Platform,
	TouchableOpacity
} from "react-native"
import { img } from "@media"
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons  from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
var {height, width} = Dimensions.get('window');

var hHeight = 50;
var hTopSpace = 20;




export default class Navbar extends Component{
	isIphoneX() {
	    const dimen = Dimensions.get('window');
	    return (
	        Platform.OS === 'ios' &&
	        !Platform.isPad &&
	        !Platform.isTVOS &&
	        (dimen.height >= 812 || dimen.width >= 812)
	    );
	}
	constructor(props) {
    	super(props);
    	
    	if(this.isIphoneX()){
	    	hHeight = 50;
			hTopSpace = 35;
    	} 
    	if(Platform.OS === 'android'){
	    	hTopSpace = 0;
    	}

	    //global.user.cotizador.length
	    this.state = {isFavorite:false,cotizaciones:0}
    	
    	
  	}
  	update(){
	  	this.setState({cotizaciones:global.user.cotizador.length});
  	}
	render() {

		var center = (<View/>);
		var title = this.props.title;
		var likeJSX = (<View/>);
		var sketchJSX = (<View/>);
		var stylesLogoRight = {};

		if(this.props.onLike!=null){
			likeJSX = (<TouchableOpacity onPress={()=>{ /*this.props.onLike();*/
			 	this.props.onLike(); }} 
			 style={{backgroundColor:'transparent',height:hHeight,width:hHeight-5,
					alignItems:'center',justifyContent:'center'}}>
					
					
					<Image style={{width:25,height:25,resizeMode:'contain',tintColor:'#FFF'}} source={img.heart} />
					
					
				</TouchableOpacity>);
		}
		var iconum = (<View/>)
		if(Number(this.state.cotizaciones)>0){
			iconum = (<View style={{width:14,height:14,borderWidth:1,borderColor:'white',borderRadius:7,backgroundColor:'#bb3155',justifyContent:'center',
								alignItems:'center',position:'absolute',top:10,left:15}}>
								<Text style={{fontFamily:'HP Simplified',fontSize:10,
									color:'white',fontWeight: 'bold'}}>{this.state.cotizaciones}</Text>
							</View>)
		}
		if(this.props.onSketch!=null){
			sketchJSX = (<TouchableOpacity onPress={()=>{
				this.props.onSketch();
			}} style={{backgroundColor:'transparent',marginRight:20,marginLeft:0,height:hHeight,width:hHeight-20,
							alignItems:'center',justifyContent:'center'}}>

							<Image style={{width:25,backgroundColor:'transparent',
									height:25,resizeMode:'contain', tintColor: 'white'}} source={ img.ico_cot } />
									
									
							{iconum}	
									
						</TouchableOpacity>);
		}
		
		if(this.props.onLike==null && this.props.onSketch==null){
			
			stylesLogoRight = {alignItems:'center',flex:1,marginRight:0,flexDirection:'row',justifyContent:'center',backgroundColor:'transparent',marginRight:40}
		}else{
			stylesLogoRight = {alignItems:'center',flex:1,marginRight:0,flexDirection:'row',justifyContent:'center',backgroundColor:'transparent',marginLeft:55}
		}
		if(this.props.onLike==null && this.props.onSketch!=null){
			stylesLogoRight = {alignItems:'center',flex:1,marginRight:0,flexDirection:'row',justifyContent:'center',backgroundColor:'transparent'}
		}
		
		
		var winpro = (<View/>);
		/*
		if(this.props.showWinPro){	
			winpro = (<View style={{width:110,height:30,flexDirection:'row',marginLeft:0,backgroundColor:'transparent'}}>
			
			<Image style={{marginLeft:0,marginTop:6,width:94,backgroundColor:'transparent',
									height:20,resizeMode:'contain', tintColor: 'white'}} source={ img.winPro } /></View>)
		}
		*/
		
		var logohp = (<View/>);
		if(this.props.onHome!=null){
			logohp = (<TouchableOpacity style={{backgroundColor:'transparent',width:hHeight-20,height:hHeight-20,marginBottom:3,
									marginLeft:10}} onPress={()=>{ this.props.onHome(); }}>
									
								<Image height style={{
									width:hHeight-20,backgroundColor:'transparent',
									height:hHeight-20,resizeMode:'contain', tintColor: 'white'}} 																				source={ img.logoAzul } />
									
									</TouchableOpacity>);
		}else{
			logohp = (<View style={{backgroundColor:'transparent',width:hHeight-20,height:hHeight-20,
									marginLeft:10}}>
									
								<Image height style={{
									width:hHeight-20,backgroundColor:'transparent',
									height:hHeight-20,resizeMode:'contain', tintColor: 'white'}} 																				source={ img.logoAzul } />
									
									</View>);
		}
		if(this.props.onBack!=null){
			center = (	<View style={{
				backgroundColor:'#0096d6',marginTop:hTopSpace,
				flexDirection:'row',justifyContent:'space-between',alignItems:'center',height:hHeight}}>

							<View style={{flex:1,flexDirection:'row',alignItems:'center',backgroundColor:'transparent',
								marginLeft:0,height:hHeight}}>

								<TouchableOpacity style={{backgroundColor:'transparent',alignItems:'center',
									width:hHeight-10,height:hHeight,justifyContent:'center'}} onPress={()=>{ this.props.onBack(); }}>
									<Image style={{width:25,backgroundColor:'transparent',
									height:25,resizeMode:'contain', tintColor: 'white'}} source={ img.chevron } />
								</TouchableOpacity>
								
								<View style={stylesLogoRight}>
								{logohp}
								</View>
							</View>
							
							<View style={{flexDirection:'row',backgroundColor:'transparent'}}>
								{likeJSX}
								{sketchJSX}
							</View>
						</View>);
		}else{
			center = (<View style={{flex:1,alignItems:'center',backgroundColor:'#0096d6'}}>
					<Image height
					style={{width:180,height:hHeight,
						resizeMode:'contain', tintColor: 'white'}}
						source={ img.hp_nav } />
						
			</View>);
		}

		return (
			<View style={ {width:width,height:hHeight+hTopSpace,
				paddingTop:0,backgroundColor:'#0096d6',justifyContent:'flex-start'} }>
					{center}
			</View>
		);
	}
}
