import React, { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: "center",
		width: 160,
	},
	title:{
		fontSize: 15,
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		color: "#575757",
		textAlign: "center",
		marginTop: 0,
		paddingLeft:10,
		paddingRight:10
	},
	category:{
		fontSize: 20,
		color: "#0096D6",
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		textAlign: "center",
		marginBottom: 0,
	},
	image:{
		resizeMode: "contain",
		marginBottom: 16,
		width: 140,
		height: 140
	}

})
