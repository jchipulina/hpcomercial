import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity
} from 'react-native';
import Dash from 'react-native-dash'
import { Img } from "@media"
import _style from "./style"
import Reactotron from 'reactotron-react-native'
import FastImage from 'react-native-fast-image'
/*<Image source={image} style={{width:70,height:70,resizeMode:'contain'}} />*/
export default class SearchItem extends Component {

	render() {

		const {
			style,
			title,
			category,
			image,
			producto,
			navigation,
			...props
		} = this.props

		return (<TouchableOpacity
					onPress={()=>{
						var type = "computo";
						if(Number(producto.idTipo)==2){ type = "impresion"; }
						Reactotron.log(producto);
						navigation.navigate("ItemProfileProduct",
						{type:type,producto:producto});
						
					}}
		        	{...props}
					style={[ _style.container, style ]}>
					<Text style={ _style.category }>{category}</Text>
					
					
					<FastImage
                	  style={{width:'100%',flex:1,backgroundColor:'white',minHeight:110 }}
					  source={{
					    uri: image,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
					
					<Text style={ _style.title }>{title}</Text>
				</TouchableOpacity>)
	}
}
