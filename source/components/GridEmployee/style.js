import React, { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
	},
	titulo:{
		color:"#1259A1",
		textAlign: "center",
		fontSize: 24,
		margin: 18,
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',
	},

	listItem:{
		flex: 1,
		flexGrow: 0,
		padding: 8,
		width:'38%',
	},
	listItemContainer:{
		flex:1

	},
	listItemText:{
		color: "white",
		textAlign: "center",
		fontSize: 22,
		margin: 8,
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',
		fontWeight:Platform.OS === 'ios' ? 'bold' : 'normal'
	},
})
