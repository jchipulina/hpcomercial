import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  Platform,
  Dimensions,
  Image,
  TouchableOpacity
} from 'react-native'
import Reactotron from 'reactotron-react-native'
import { Img } from "@media"
import _style from "./style"
import { img } from "@media"

const {height, width} = Dimensions.get('window'); 
const aspectRatio = height/width;

var sizeFont = 15;
var sizeBigFont = 18 
if(aspectRatio>1.6) {

   // Code for Iphone

}
else {
	sizeFont = 22;
	sizeBigFont = 26;
   // Code for Ipad

}

export default class GridEmployee extends Component {

	content(){
		const{
			producto,
			ventajas1,
			ventajas2,
			ventajas3,
			ventajas4,
			list,
			cols,
			gap,
      navigation
		} = this.props

		if (list && list.length) {

	var boxtitle = (<View/>);
	
      return list.map( item => {
				return(<View  key={ item.key } style={[ _style.listItem, { flexBasis:(cols==3)?"33.3%":"50%", padding: (cols==3)?2:6 } ]}>
            <TouchableOpacity disabled={(item.enabled)?false:true} style={ _style.listItemContainer } onPress={()=>{

              if(item.key=='datasheet'){
	              global.trackScreenView("Assets - Datasheet",{ 1: global.user.tipocliente });
	              this.props.navigation.navigate("PDFVista",{pdf:item.datasheet})
              }
              if(item.key=='ficha_tecnica'){
	             global.trackScreenView("Assets - Ficha Tecnica",{ 1: global.user.tipocliente });
	              this.props.navigation.navigate("FichaTecnica",{producto:producto})
              }
              
              if(item.key=='productoventaja'){
	              global.trackScreenView("Assets - Ventajas HP",{ 1: global.user.tipocliente });
	              this.props.navigation.navigate("ListadoCaracteristicas",{ventajas:ventajas1,titulo:"Ventajas HP",producto_nombre:producto.name})
              }
              if(item.key=='ventajas_competencia'){
	              global.trackScreenView("Assets - Ventajas Competencia",{ 1: global.user.tipocliente });
	              this.props.navigation.navigate("ListadoCaracteristicas",{ventajas:ventajas2,titulo:"Ventajas competencia",producto_nombre:producto.name})
              }
              if(item.key=='igualaigual'){
	              global.trackScreenView("Assets - Igual a Igual",{ 1: global.user.tipocliente });
	              this.props.navigation.navigate("ListadoCaracteristicas",{ventajas:ventajas3,titulo:"Igual a igual",producto_nombre:producto.name})
              }
              if(item.key=='seguridad'){
	              global.trackScreenView("Assets - Seguridad",{ 1: global.user.tipocliente });
	              this.props.navigation.navigate("ListadoCaracteristicas",{ventajas:ventajas4,titulo:"Seguridad",producto_nombre:producto.name})
              }
              
     
              
            }}>
            <View
              style={ {flex:1,backgroundColor:'#f4f7f7' ,borderColor:'#87898b',
								borderWidth:0.5} } >
              
           
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              
             
              		{item.enabled &&
        <Image style={{width:70,height:55,resizeMode:'contain',tintColor:'#blue'}} source={ item.image } />
      }
      
      {!item.enabled &&
        <Image style={{width:70,height:55,resizeMode:'contain',tintColor:'#8b8b8b'}} source={ item.image } />
      }
      
      
              		
              		<View style={{backgroundColor:'rgba(0,0,0,0)',paddingTop:0,paddingLeft:0,paddingRight:0}}>
							<Text style={{color:(item.enabled)?'#292929':'#8b8b8b',fontWeight:'200',fontSize:(cols==3)?sizeFont:15,lineHeight:(cols==3)?sizeFont:sizeBigFont,textAlign:'center',
								fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{item.title}
							</Text>
					</View>
              </View>
            </View>
            </TouchableOpacity>
				</View>);

			})


		}
	}


	render() {
		const{
			list,
			cols,
			gap,
			style
		} = this.props

		return (<View style={_style.container}>
					<View style={{ flex:1,marginBottom: 0,alignContent: "stretch",
						flexWrap: "wrap",flexDirection: "row"}}>
						{this.content()}
					</View>

				</View>)

	}
}
/*
{this.content()}
*/
