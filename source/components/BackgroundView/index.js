import React, { Component } from "react"
import {
	View,
	Image
} from "react-native"
import style from "./style"


export default class BackgroundView extends Component{


	render() {
		return (
			<View style={[ this.props.containerStyle, style.background ]}>
				<View  style={ style.background }>
					<Image style={ style.image } source={ this.props.image }/>
				</View>
				<View  style={ style.view }>
					<View  style={ this.props.style }>{this.props.children}</View>
				</View>
			</View>
		);
	}
}
