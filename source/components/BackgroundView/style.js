import React, { StyleSheet } from "react-native";

export default StyleSheet.create({
	"container": {
		flex: 1,
		position: "relative",
		overflow: "hidden",
		backgroundColor: "aqua"

	},
	"background":{
		position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
	},
	"image":{
		flex: 1,
		width:'100%',
		height:'100%',
		resizeMode: "cover",

	},
	"view":{
		position: 'absolute',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: "center"
	}
})
