import Reactotron, { trackGlobalErrors, openInEditor, overlay } from 'reactotron-react-native'
import { reactotronRedux as PluginRedux } from 'reactotron-redux'

if ( __DEV__ ) {
	Reactotron.configure({name: 'HP'})

	Reactotron.useReactNative({
	    asyncStorage: { ignore: [] }
	})

	Reactotron.use( 
		PluginRedux(),
		trackGlobalErrors(), 
		openInEditor(), 
		overlay(), 
	)

    Reactotron.connect()
}
