import React, { StyleSheet } from "react-native";

export default StyleSheet.create({
	text: {
		color: "#0096d6",
		fontSize: 20,

		textAlign: "center",
		fontFamily:'HP Simplified',
		marginBottom: 32,
	},
	image:{
		width: 144,
		height: 144,
		resizeMode: "contain",
		marginBottom: 16,
	},
	container:{
		paddingLeft: 24,
		paddingRight: 24,
		
	},
	button:{
		marginBottom:60,
		paddingLeft: 15,
		paddingTop:5,
		paddingBottom:5,
		paddingRight: 15,
		borderRadius:0,
	}

})
