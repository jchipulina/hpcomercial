import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  Platform,
} from 'react-native';

import {
	BackgroundView,
	Logo
} from "@components"
import { Button } from 'react-native-elements'
import { Img } from "@media"
import { img } from "@media"
import style from "./style"

export default class Cover extends Component {

	render() {

    var {navigation} = this.props;
    
		return (<BackgroundView image={ img.home_bg }>
		
					<View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
					
						<View style={{alignItems: 'center'}}>
							<Image
								style={{width:70,height:70,marginTop:75 }}
								source={ img.hp_nav }/>
							<Text
								style={{
									alignSelf: "stretch",
									color: "#000",
									marginTop:40,
									fontSize: 25,
									fontWeight:'200',
									textAlign: "center",
									fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
									lineHeight:25,
								}}>
								{"SOLUCIONES DE CÓMPUTO\nE IMPRESIÓN EMPRESARIAL"}
							</Text>
				        </View>

						<Button
						containerViewStyle={ style.button }
						backgroundColor={'#0096d6'}
						onPress={()=>{ this.props.onNext()}}
            			textStyle={{fontWeight:'normal',color:'#FFF',fontSize:20}}
						fontFamily = {Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}
						title="EMPEZAR AHORA" />
					</View>

		        </BackgroundView>)
	}
}
