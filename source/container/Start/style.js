import React, { StyleSheet } from "react-native";
/*
			flexDirection: "column",
		justifyContent: "center",
		alignItems: "stretch"
		
		*/
export default StyleSheet.create({
	container: {
		flex: 1,

	},
	header_ios: {
		justifyContent: "center",
		alignItems: "stretch",
		flex: 1,
		flexShrink: 0,
		padding: 16,
		zIndex:10,
		backgroundColor:'#2e95d5'
	},
	header: {
		justifyContent: "center",
		alignItems: "stretch",
		flex: 1,
		flexShrink: 0,
		padding: 16,
		backgroundColor:'#0096d6'
	},
	searchBar:{
		marginLeft: 12,
		marginRight: 12,
		marginTop:0,
		marginBottom:12, 
		justifyContent: "center",
		flexDirection:'row',
	},
	search:{
		backgroundColor: "red",
		borderWidth:0,
		flex:1
	},
	searchText:{ 
			fontFamily:'HP Simplified',
			fontSize:15
	},
	canales:{
		flex: 1,
		flexDirection: "row",
		backgroundColor:'transparent',
		marginLeft: -4,
		marginRight: -4
	},
	canal:{
		flex: 0.5,
		flexBasis: "50%",
		flexShrink: 1,
		margin: 4
	},
	canalText:{
		textAlign: "center",
		fontFamily:'HP Simplified',
		fontSize: 20,
		color:'#575757',
		margin: 20
	},
	canalImage:{
		flex: 1,
		flexShrink: 1,
		//width: "100%"
	},
	ofertas: {
		marginTop: 4,
		flex: 1,
	},
	oferta: {
		resizeMode: "cover",
		flex: 1,
		width: "100%"
	}
})
