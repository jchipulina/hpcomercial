import React, { Component } from "react";
import {
  View,
  Image,
  ScrollView,
  Text,
  TextInput,
  ImageBackground,
  Platform,
  Dimensions,
  TouchableOpacity,
  Linking
} from 'react-native'
import {
	Button,
	SearchBar
} from 'react-native-elements'
import Swiper from 'react-native-swiper'
import FastImage from 'react-native-fast-image'
import analytics from '@react-native-firebase/analytics';
import {
	BackgroundView,
	Logo,
	Register
} from "@components"
import Reactotron from 'reactotron-react-native'
import { Img } from "@media"
import { img } from "@media"
import style from "./style"
var parseString = require('react-native-xml2js').parseString;
var {height, width} = Dimensions.get('window');
let cmp;

var navigation;
let itemStyle = { padding:0 }
let itemTextStyle = { padding:6, borderBottomWidth:1, borderColor: "#ccc" }
var scope = this;
var banners = new Array();

export default class Start extends Component {
	constructor(props) {
    	super(props);
    	scope = this;
    	
		navigation = this.props.navigation;
		
    	cmp = this;
		this.state = { buscar: "", lista1:[],text:'',banners:[],currentSlide:0};
	}
	componentWillMount(){
		Reactotron.log("------HOME--------")
		
		this.onSignIn();
		this.obtenerBanners();
		
	}
	async onSignIn() {
	  Reactotron.log("onSignIn "+global.user.id);
	  await Promise.all([
	    analytics().setUserId(String(global.user.id)),
	    analytics().setUserProperty('usuario', String(global.user.id)),
	    analytics().setUserProperty('pais', global.user.pais),
	    analytics().setUserProperty('tipocliente', global.user.tipocliente),
	  ]);
	}
	async obtenerBanners( ){
		let urlBase =  global.api+'/BannersListado';
		var url = urlBase;
		url += "?int_IdUsuario="+global.user.id;
		url += "&int_IdTipoProducto=0";

		let response1 = await fetch(url,{method: "GET"});
		let responseText1 = await response1.text();
		parseString(responseText1, function (err, result) {
			var resp1 = JSON.parse(result.string._);
			Reactotron.log( '*Banners*' );
			Reactotron.log( resp1 );
			
			for(var i=0;i<resp1.length;i++){
				var obj = {};
				obj.imagen 			= resp1[i].banner.imagen;
				obj.link 			= resp1[i].banner.link;
				obj.tipolink 		= resp1[i].banner.tipolink;
				obj.tipoproducto 	= resp1[i].banner.tipoproducto;
				obj.idcategoria 	= resp1[i].banner.idcategoria;
				obj.idproducto 		= resp1[i].banner.idproducto;
				banners.push(obj);
				
			}
			scope.setState({banners:banners})
			//cmp.setState({ lista1: resp1.map( cmp.mapeaResultado ).slice( 0, 3 ) });
			//Reactotron.log( cmp.state );
		});
	}
	
	async getProductAndGo( id ){
		Reactotron.log( "getProductAndGo "+ id);
		var url =  global.api+'/ProductoDetalle?int_IdProducto='+id;
		let response = await fetch(url,{method: "GET"});
		let responseText = await response.text();
		parseString(responseText, function (err, result) {
			var resp = JSON.parse(result.string._);
			Reactotron.log( resp[0].producto );
			navigation.navigate("ItemProfileProduct",{type:'',producto:resp[0].producto});
		});
	}
	async obtenerResultados( term ){
		//BannersListado
		let urlBase =  global.api+'/Producto_Busqueda';
		var url = urlBase;
		url += "?busqueda=" + encodeURIComponent(term);
		url += "&int_IdTipoProducto=0&int_IdUsuario="+global.user.id;

		//Reactotron.log( "busca " + term );

		let response1 = await fetch(url,{method: "GET"});
		let responseText1 = await response1.text();
		parseString(responseText1, function (err, result) {
			var resp1 = JSON.parse(result.string._);
			//Reactotron.log( resp1 );
			cmp.setState({ lista1: resp1.map( cmp.mapeaResultado ).slice( 0, 3 ) });
			Reactotron.log( cmp.state );
		});
	}
	async fetchDataPartnersAndGo(){
		Reactotron.log("fetchDataPartnersAndGo");
		if(global.user.pais!='Colombia'){
			try { 
				
				//242 costa
				//243 Puerto rico
				var url = global.api+"/PlanetPartner_Detalle?int_IdUsuario="+global.user.id;
				let response = await fetch(url);
				let responseText = await response.text();
				laptops = new Array();
				
				Reactotron.log(url);
				
	            parseString(responseText, function (err, result) {
		             //Reactotron.log(result);
		            if(result.string._!=undefined){
			            var resp = JSON.parse(result.string._);
			            partnersData = resp[0].programa;
			            navigation.navigate('PlanetPartners',{data:partnersData});
		            }
				});
	        } catch (err) {
	            Reactotron.log("ERROR "+err);
	        }			
		}

	}
	busqueda( buscar ){
		Reactotron.log( "buscar " + buscar );
		
		
		this.setState({text:buscar})						
		this.setState({buscar})
		if (buscar.length >= 3) {
			this.obtenerResultados( buscar );
		}else{
			this.setState({lista1:[]});
		}
	}
	findByCategory(cat,tipo){
		const list_computo = [
			{
				key: "notebooks",
				title: "Notebooks",
				image: img.cat1,
				top: img.cat1,
				idcategoria: 1
			},{
				key: "desktop",
				title: "Desktops",
				image: img.cat2,
				top: img.cat2,
				idcategoria: 2
			},{
				key: "workstations",
				title: "Workstations",
				image: img.cat3,
				top: img.cat3,
				idcategoria: 4
			},{
				key: "monitores",
				title: "Monitores",
				image: img.cat4,
				top: img.cat4,
				idcategoria: 3
			},{
				key: "pos",
				title: "Retail POS",
				image: img.cat5,
				top: img.cat5,
				idcategoria: 5
			},{
				key: "care-packs",
				title: "HP Care Pack",
				image: img.cat6,
				top: img.cat6,
				idcategoria: 6
			}
		]
		const list_impresion = [
			{
				key: "OfficeJet",
				title: "HP OfficeJet",
				image: img.boton_officejet,
				top: img.boton_officejet_interna,
				idcategoria: 1
			},
			{
				key: "PageWide",
				title: "HP PageWide",
				image: img.boton_pagewide,
				top: img.boton_pagewide_interna,
				idcategoria: 2
			},
			{
				key: "LaserJet",
				title: "HP LaserJet",
				image: img.boton_laserjet,
				top: img.boton_laserjet_interna,
				idcategoria: 3
			},{
				key: "ScanJet",
				title: "HP ScanJet",
				image: img.impq4,
				top: img.top7,
				idcategoria: 4
			},{
				key: "Suministros",
				title: "Suministros",
				image: img.impq5,
				top: img.top9,
				idcategoria: 15
			}
		]
		if(Number(tipo)==1){
			for(var i=0;i<list_computo.length;i++){
				if(Number(list_computo[i].idcategoria) == Number(cat)){
					navigation.navigate('GridList',{item:list_computo[i],type:'computo'});
					break;
				}
			}
		}else{
			for(var i=0;i<list_impresion.length;i++){
				if(Number(list_impresion[i].idcategoria) == Number(cat)){
					navigation.navigate('GridList',{item:list_impresion[i],type:'impresion'});
					break;
				}
			}
		}
		
		
	}

	processLink(r){
		
		if(Number(r.tipolink)==0){ //LINK EXTERNO
			if(r.link!=''){
				Linking.canOpenURL(r.link).then(supported => {
					if (supported) {
						var action = "Click "+global.user.tipocliente
						global.trackScreenView("Banners",{ action:action,link:r.link});
						Linking.openURL(r.link);
					} else {
						Reactotron.log('Don\'t know how to open URI: ' + r.link);
					}
				});
			}
		}
		if(Number(r.tipolink)==1){
			console.warn("r.tipoproducto "+r.tipoproducto); 
			console.warn("r.idproducto "+r.idproducto); 
			console.warn("r.idcategoria "+r.idcategoria); 
			
			if(Number(r.tipoproducto)!=0){
				if(Number(r.idproducto)!=0){ //SOLO PRODUCTO
					this.getProductAndGo(r.idproducto);
					return;
				}	
				if(Number(r.tipoproducto)==1 && Number(r.idcategoria)==0){ //SOLO CATEGORIA COMPUTO
					navigation.navigate('ComputoCategories')
					return;
				}
				if(Number(r.tipoproducto)==2 && Number(r.idcategoria)==0){ //SOLO CATEGORIA IMPRESION
					navigation.navigate('ImpresionCategories')
					return;
				}
				if(Number(r.tipoproducto)==3 && Number(r.idcategoria)==0){ //SOLO CATEGORIA PLANET
					this.fetchDataPartnersAndGo();
					return;
				}
				if(Number(r.idcategoria)!=0 && Number(r.idproducto)==0){ //SOLO CATEGORIA
					this.findByCategory(r.idcategoria,r.tipoproducto);
					return;
				}
				
			}								
		}
	}
	mapeaResultado( obj, i, arreglo ){
		//const { navigation } = this.props
		return <TouchableOpacity onPress={()=>{
			navigation.navigate("ItemProfileProduct",{type:'',producto:obj.productob});
						
						setTimeout(()=>{
							//Reactotron.log(scope.txtField1)
							scope.txtField1.clear();
							//scope.txtField1.focus();
							scope.setState({lista1:[],buscar:''});
							scope.setState({text:''});
						}, 100)
						
		} }
			key={i}
			style={ itemStyle }
			id={ obj.productob.id }>
			<Text style={itemTextStyle} >{obj.productob.name} </Text>
			</TouchableOpacity>
	}

	render() {
		var swiper = (<View style={{flex:1, backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
				<Image style={{width:40,height:40,resizeMode:'cover'}} source={ img.loading } />
		</View>);
		var imagenes = new Array();
		if(this.state.banners.length>0){
			for(var i=0;i<this.state.banners.length;i++){
				imagenes.push(this.state.banners[i]);
			}
			swiper = (<Swiper  pagingEnabled={true} dotColor={'#ccc'}>			
					{imagenes.map(r => <TouchableOpacity style={{flex:1}} onPress={()=>{ this.processLink(r);
											//disabled={(r.link=='')?true:false}
							}}>
							<FastImage
		                	  style={{width:'100%',flex:1}}
							  source={{
							    uri: r.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.cover}
							/>
							</TouchableOpacity>
							)}
					</Swiper>);
		}
	
		
		var placeholder = "¿Qué estás buscando hoy "+global.user.nombres+"?";
		
		if(Platform.OS === 'ios'){
			return ( 
				<View style={{flex: 1,backgroundColor:'#0096d6'}}>   

						<View style={{position:"absolute",width:'100%',height:'35%',top:0,zIndex:2}}>
					
						<View style={{flex:1,justifyContent:'flex-end',alignItems: 'center',backgroundColor:'transparent',paddingTop:0}}> 
							<Image
								style={{width:70,height:70,tintColor:'#FFF'}}
								source={ img.hp_nav }/>
							<Text
								style={{
									color: "#FFF",
									marginTop:12,
									marginBottom:12,
									fontSize: 20,
									fontWeight:'300',
									textAlign: "center",
									fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
								}}>
								{"SOLUCIONES DE CÓMPUTO\nE IMPRESIÓN EMPRESARIAL"}
							</Text>
				        </View>
						<View style={ style.searchBar }>
						<View style={{ flex:1 }}>
							<TextInput 
								ref={ (c) => scope.txtField1 = c }
								underlineColorAndroid='transparent'
								style={{backgroundColor: "white",
								borderWidth:0,
								fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
								paddingLeft:10,
								fontSize:15,
								flex:1}}
								keyboardAppearance="light"
								returnKeyType="search"
								value={this.state.text}
								onChangeText={(buscar) => this.busqueda(buscar)}
								placeholder={placeholder} />

							<View style={{ zIndex:1, elevation:1, position:"absolute", left:0, top:"100%", width:"100%",backgroundColor: "#F6F6F7" }}>
								{this.state.lista1}
							</View>

						</View>
				              <TouchableOpacity onPress={()=>{ this.props.onBuscar(this.state.buscar) }}>
				                <View style={{backgroundColor:'#a53894',width:40,height:40,justifyContent:'center',alignItems:'center'}}>
				                  <Image style={{width:20,height:20,resizeMode:'contain',tintColor:'white'}} source={ img.ico_search } />
				                </View>
				              </TouchableOpacity>
						</View>
						
						</View>
						<View style={{backgroundColor:'white',position:"absolute",width:'100%',height:'65%',top:'35%',zIndex:1}}>
								<View style={{backgroundColor:'white',flex:1}}>
								
									<View style={ style.canales }>
							            <TouchableOpacity
							            style={[style.canal,{marginRight:2}]} onPress={()=>{this.props.onComputo()}}>
													<ImageBackground
														source={ img.computo_btn }
														style={ {flex:1,justifyContent:'flex-end',alignItems:'center'}}
														resizeMode="cover">
														
														<View style={{marginBottom:10, backgroundColor:'rgba(0,0,0,0.5)',borderColor:'white',
															borderWidth:0.5,paddingTop:2,paddingLeft:10,paddingRight:10,paddingBottom:2}}>
														<Text style={{color:'white',fontSize:20,
															fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'CÓMPUTO'}</Text>
														</View>
														
														
													</ImageBackground>
							            </TouchableOpacity>
							            <TouchableOpacity
							            style={[style.canal,{marginLeft:2}]} onPress={()=>{this.props.onImpresion()}}>
													<ImageBackground
														source={ img.boton_impresion_hp }
														style={ {flex:1,justifyContent:'flex-end',alignItems:'center'}}
														resizeMode="cover">
														
														
														<View style={{marginBottom:10,backgroundColor:'rgba(0,0,0,0.5)',borderColor:'white',
															borderWidth:0.5,paddingTop:2,paddingLeft:10,paddingRight:10,paddingBottom:2}}>
														<Text style={{color:'white',fontSize:20,
															fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'IMPRESIÓN'}</Text>
														</View>
													
													</ImageBackground>
							            </TouchableOpacity>
										</View>
			 
								</View>
								<View style={{backgroundColor:'transparent',flex:1}}>  
								{swiper}
								</View>
						</View>

				</View>);
		}else{
			return ( 
				<View style={{flex: 1,backgroundColor:'#0096d6'}}>   
 
					
						<View style={{backgroundColor:'transparent',flex:1,zIndex:30}} > 
						
						
						<View style={{alignItems: 'center',backgroundColor:'transparent',paddingTop:30}}> 
							<Image
								style={{width:70,height:70,tintColor:'#FFF'}}
								source={ img.hp_nav }/>
							<Text
								style={{
									color: "#FFF",
									marginTop:10,
									fontSize: 20,
									fontWeight:'300',
									textAlign: "center",
									fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
								}}>
								{"SOLUCIONES DE CÓMPUTO\nE IMPRESIÓN EMPRESARIAL"}
							</Text>
				        </View>




						<View style={ { margin:12,  justifyContent: "center", flexDirection:'row'} }>
						<View style={{ flex:1 }}>
							<TextInput 
								ref={ (c) => scope.txtField1 = c }
								underlineColorAndroid='transparent'
								style={{backgroundColor: "white",
								borderWidth:0,
								fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
								paddingLeft:10,
								fontSize:15,
								flex:1}}
								keyboardAppearance="light"
								returnKeyType="search"
								value={this.state.text}
								onChangeText={(buscar) => this.busqueda(buscar)}
								placeholder={placeholder} />
							
							
							
							
							
							<View style={{ zIndex:1, elevation:1, position:"absolute", left:0, top:"100%", width:"100%",backgroundColor: "#F6F6F7" }}>
								{this.state.lista1}
							</View>

						</View>
              <TouchableOpacity onPress={()=>{ this.props.onBuscar(this.state.buscar) }}>
                <View style={{backgroundColor:'#a53894',width:40,height:40,justifyContent:'center',alignItems:'center'}}>
                  <Image style={{width:20,height:20,resizeMode:'contain',tintColor:'white'}} source={ img.ico_search } />
                </View>
              </TouchableOpacity>
						</View>
						
						
							<View style={{backgroundColor:'white',flex:1}}>
								<View style={{backgroundColor:'transparent',flex:1}}>
								
									<View style={ style.canales }>
            <TouchableOpacity
            style={[style.canal,{marginRight:2}]} onPress={()=>{this.props.onComputo()}}>
						<ImageBackground
							source={ img.computo_btn }
							style={ {flex:1,justifyContent:'flex-end',alignItems:'center'}}
							resizeMode="cover">
							
							<View style={{marginBottom:10, backgroundColor:'rgba(0,0,0,0.5)',borderColor:'white',
								borderWidth:0.5,paddingTop:2,paddingLeft:10,paddingRight:10,paddingBottom:2}}>
							<Text style={{color:'white',fontSize:20,
								fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'CÓMPUTO'}</Text>
							</View>
							
							
						</ImageBackground>
            </TouchableOpacity>
            <TouchableOpacity
            style={[style.canal,{marginLeft:2}]} onPress={()=>{this.props.onImpresion()}}>
						<ImageBackground
							source={ img.boton_impresion_hp }
							style={ {flex:1,justifyContent:'flex-end',alignItems:'center'}}
							resizeMode="cover">
							
							
							<View style={{marginBottom:10,backgroundColor:'rgba(0,0,0,0.5)',borderColor:'white',
								borderWidth:0.5,paddingTop:2,paddingLeft:10,paddingRight:10,paddingBottom:2}}>
							<Text style={{color:'white',fontSize:20,
								fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'IMPRESIÓN'}</Text>
							</View>
						
						</ImageBackground>
            </TouchableOpacity>
			</View>
			
			 
								</View>
								<View style={{backgroundColor:'white',flex:1}}>  
								{swiper}
								</View>
							</View>
						</View>
						
						
				</View>)
		}

		

	}
}