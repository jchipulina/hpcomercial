import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
} from 'react-native'
import {
} from 'react-native-elements'
import Swiper from 'react-native-swiper'

import {
	Grid
} from "@components"
import { Img } from "@media"
import style from "./style"

export default class Categories extends Component {

	render() {
		const{
			title,
			list,
			novedosos,
      navigation
		} = this.props

		var grid = (<View/>);

		if(title=="Impresión"){
			grid = (<Grid title={title} list={list} novedosos={novedosos} cols={2} gap={8} navigation={navigation} onEncuentra={()=>{
				
				navigation.navigate('Question');
				
			}}  onPlanetPartner={()=>{
				
				navigation.navigate('PlanetPartners');
				
			}} onNext={(item)=>{
				navigation.navigate('GridList',{item:item,type:'impresion'});
				
			}}/>);
		}else{
			grid = (<Grid title={title} list={list} novedosos={novedosos} cols={2} gap={8} navigation={navigation} onNext={(item)=>{
				//if(item.key=="care-packs"){
					//navigation.navigate('CarePacks',{item:item});
				//}else{
					navigation.navigate('GridList',{item:item,type:'computo'});
				//}
				
				
			}}/>);
		}

		return (
				<View style={ style.container }>
					<Text style={ style.titulo }>{title.toUpperCase()}</Text>
					{grid}
				</View>)

	}
}
