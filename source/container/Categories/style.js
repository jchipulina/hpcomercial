import React, { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "stretch",
		paddingTop: 0,
		paddingBottom: 0,
		marginLeft: 0,
		marginRight: 0
	},
	titulo:{
		color:"#707070",
		textAlign: "center",
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		fontSize: 25,
		fontWeight:'200',
		margin: 15
	},
})
