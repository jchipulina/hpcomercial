import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  Platform,
  TextInput
} from 'react-native'
import {
	Input,
	Button
} from 'react-native-elements'
import Swiper from 'react-native-swiper'

import {
	SearchList,
	SearchItem
} from "@components"
import { Img } from "@media"
import { img } from "@media"
import Reactotron from 'reactotron-react-native'
var parseString = require('react-native-xml2js').parseString;

import style from "./style"

let cmp;
var navigation
export default class Start extends Component {

	constructor(props){
		super(props);
		cmp = this;
		navigation = this.props.navigation
		this.state={
			term:this.props.term,
			type:this.props.type,
			startSearch:false,
			lista1:[],
			lista2:[]
		}
	}

	async obtenerResultados( term ,type ){
		
		
		Reactotron.log("*type "+type);
		let urlBase =  global.api+'/Producto_Busqueda';
		var url = urlBase;
		url += "?busqueda=" + encodeURIComponent(term);
		if(type==undefined || type=='computo'){
			url += "&int_IdTipoProducto=1&int_IdUsuario="+global.user.id;
		}else{
			url += "&int_IdTipoProducto=2&int_IdUsuario="+global.user.id;
		}
		//Reactotron.log(url);
		Reactotron.log('type ____ '+type);
		let response1 = await fetch(url,{method: "GET"});
		let responseText1 = await response1.text();
		var items = new Array();
		parseString(responseText1, function (err, result) {
			if( result.string._ !=undefined ){
				var resp1 = JSON.parse(result.string._);
				items = resp1;
			}
			if(type==undefined){
				Reactotron.log('impresionLoad');
				cmp.impresionLoad(items,term);		
			}else{
				Reactotron.log('startSearch');
				cmp.setState({ lista1:items,startSearch:true});
			}
		});
	}
	async impresionLoad(items,term){
		Reactotron.log('impresionLoad');
			var url =  global.api+'/Producto_Busqueda';
			url += "?busqueda=" + encodeURIComponent(term);
			url += "&int_IdTipoProducto=2&int_IdUsuario="+global.user.id;
			let response2 = await fetch(url,{method: "GET"});
			let responseText2 = await response2.text();
			parseString(responseText2, function (err, result) {
				if( result.string._ !=undefined ){
					Reactotron.log(result.string._)
					var resp2 = JSON.parse(result.string._);
					for(var p=0;p<resp2.length;p++){
						items.push(resp2[p]);
					}
				}
				cmp.setState({ lista1:items,startSearch:true});
			});
	}

	componentWillMount(){
		if(this.state.term!=''){
			this.obtenerResultados( this.state.term ,this.state.type);
		}
	}

	mapeaResultado( obj, i, arreglo ){

		return <SearchItem
			key={i}
			navigation={navigation}
			producto={obj.productob}
			category={ obj.productob.categoria }
			image={ obj.productob.imagenes[0].imagen }
			id={ obj.productob.id }
			title={obj.productob.name} />
	}


	render() {
		var {navigation} = this.props;
		var {type} = this.props;
		var labelResult = "";
		if(this.state.lista1.length>0){
			labelResult = (this.state.lista1.length)+' Resultados';
		}else{
			if(this.state.startSearch){
				labelResult = '0 Resultados';
			}else{
				labelResult = '';
			}
			
		}
		return (
				<View style={ style.container }>
					
					<View style={ { justifyContent: "center", marginBottom: 5,flexDirection:'row' } }>
					
						
						
						
						<View style={{ marginLeft:5,marginRight:5,justifyContent: "center" }}>
							<Image style={{width:20,height:20,resizeMode:'contain',tintColor:'#a22f94'}} source={ img.lupa } />
						</View>
						<View style={{flex:1, backgroundColor:'white'}}>
						
						<TextInput style={{backgroundColor: "white", padding:10, maxWidth: 480, width: "100%",fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}
							ref={ (c) => this.txtField1 = c }
							underlineColorAndroid='transparent'
							keyboardAppearance="light"
							returnKeyType="search"
							value={this.state.term}
							onChangeText={(term) => this.setState({term})}
							onSubmitEditing={ () =>{
								Reactotron.log( "busca" );
								this.obtenerResultados( this.state.term ,this.state.type);
							}}
							
							placeholder={(this.state.term.length > 0)?"¿Bucas algo mas específico?":"Buscar"} />
							</View>
							<View style={{ marginLeft:5,marginRight:5,justifyContent: "center" }}>
								<Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
								{labelResult}
								</Text>
							</View>
					</View>
					<View style={{width:'100%',height:1,backgroundColor:'#999',height:0.5}}></View>
					<SearchList navigation={navigation} style={ style.list } title="Cómputo" list={this.state.lista1}/>
					
				</View>)

	}
}
