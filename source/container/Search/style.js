import React, { StyleSheet,Platform } from "react-native";

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "flex-start",
		alignItems: "stretch",
		padding: 16,
	},
	titulo:{
		color:"#1259A1",
		textAlign: "center",
		fontSize: 24,
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		margin: 18
	},
	searchBar:{
		justifyContent: "center",
		marginBottom: 10
	},
	search:{
		backgroundColor: "#F6F5F5",
		padding:10,
		maxWidth: 480,
		//marginLeft: 24,
		//marginRight: 24
		width: "100%"
	},
	list:{
		marginBottom: 10,
	},
})
