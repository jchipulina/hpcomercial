import React, { Platform,StyleSheet } from "react-native";

export default StyleSheet.create({
	container:{
		flex: 1,
		justifyContent: "center",
		alignItems: "stretch",
	},
	textInfo:{
		justifyContent: "center",
		marginTop: 16,
		color:'#575757',
		fontFamily:'HP Simplified'
	},
	textInfoText:{
		textAlign: "center",
				color:'#575757',
		fontFamily:'HP Simplified',
		padding: 0
	},
	textInfoLink:{
		textAlign: "center",
		padding: 0,
		color: "#009EDA",
		fontFamily:'HP Simplified',
		marginBottom: 16,
		fontWeight:'bold',
	},
	credits:{
		padding: 0,
		marginBottom: 8,
		fontSize: 9,
		color:'#575757',
		fontFamily:'HP Simplified'
	},
	button:{
		padding: 8,
		borderRadius: 0
	},
	buttonTitle:{
		fontSize: 20,
		fontWeight:'bold',
		fontFamily:'HP Simplified'
	},
	buttonContainer:{
		marginLeft: -24,
		marginRight: -24,
		borderRadius: 0
	},
	containerR: {
		flex: 1,
		flexDirection: "column",
		paddingLeft:20,
		paddingRight:20,
		justifyContent: "flex-start",
		alignItems: "stretch",
		backgroundColor:'transparent'
	},
	inputContainerInput: {
		color:'#000',
		width: "100%",fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		height: 34, marginBottom: 18,borderColor: '#000',fontWeight:'200',
		borderWidth: 0,backgroundColor:'#eaeaea',padding:10
	},
	input: {
		padding: 4,
		fontSize: 18,
	}

})
