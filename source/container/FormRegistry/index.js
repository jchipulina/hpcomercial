import React, { Component } from "react";
import { BackgroundView, Logo,} from "@components"
import {Input,Button} from 'react-native-elements';
import { Img } from "@media"
import { img } from "@media"
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  Text,
  Alert,
  Image,
  ActivityIndicator,
  StyleSheet,
  TextInput,
  ScrollView,
  Modal,
  TouchableOpacity,
  Platform,
  Linking
} from 'react-native';
import style from "./style"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {Select, Option} from "react-native-chooser";
import EvilIcons  from 'react-native-vector-icons/EvilIcons';
import Reactotron from 'reactotron-react-native';
var parseString = require('react-native-xml2js').parseString;
var paises = new Array();
var codigoValidacion = "";
var tempuser = {};
var paises2 = new Array();
paises2.push({id:1});
paises2.push({id:2});
paises2.push({id:3});
paises2.push({id:4});
paises2.push({id:5});
paises2.push({id:6});

var scope;
var pais = "";
var tipocliente = "";
var politicas = "";


export default class FormRegistry extends Component {
	
	constructor(props) {
    super(props);
    scope = this;
    this.state = {Alert_Visibility:false,errorCodigo:'',checkTerminos:false,value:"País",value2:"Tipo de cliente",
	    tipoclientes:[	{cliente: {id: 1,nombre: "Cliente"}},
	    				{cliente: {id: 2,nombre: "Distribuidor"}},
						{cliente: {id: 3,nombre: "HP Employee"}}],
	    paises:[],
	    nombres:'',apellidos:'',email:'',empresa:'',cod_verificacion:'',logger:'',loading:false}
    
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  
  	goFetchRegister(body,headers){
	  	console.warn("goFetchRegister");
	  	console.warn("global.servicios "+global.servicios);
		console.warn("*1");
		console.warn("body "+body);	
		console.warn(body);
		console.warn(headers);	
			
		fetch(global.servicios,{method: "POST",headers:headers,body:body})
		.then(response => response.text())
		.then((responseText)=> {

		  console.warn("*2");
		  console.warn(responseText);
		  parseString(responseText, function (err, result) {
	            var rpta = result['s:Envelope']['s:Body'][0].Usuario_RegistroResponse[0].Usuario_RegistroResult;
	            console.warn("*0"+rpta);
	            var resp = JSON.parse(rpta);
	            
	            //console.warn("*loading "+loading);
			    //scope.setState({loading:false});
			    if(tipocliente==3){
				    tempuser = resp.usuario;
				    tempuser.paisid = pais;
					tempuser.tipoclienteid = tipocliente;
					tempuser.codigo = resp.usuario.codigo;
					tempuser.cotizador = new Array(); 
					tempuser.favoritos = new Array();
				    codigoValidacion = resp.usuario.codigo;
				    console.warn(codigoValidacion);
				    Reactotron.log("CODIGO "+codigoValidacion);
				    scope.setState({Alert_Visibility:true});
				    
			    }else{
				    global.user = resp.usuario;
					global.user.paisid = pais;
					global.user.codigo = "";
					global.user.tipoclienteid = tipocliente;
					global.user.cotizador = new Array(); 
					global.user.favoritos = new Array(); 
					Reactotron.log(global.user)
					
				    global.saveUser()
				    scope.props.onNext();
			    }
			    
			});
			
			
		})
		.catch(error=>this.onError(error)) //to catch the errors if any
	}
	onError(error){
		console.warn(" -- -- onError ---- - ");
		console.warn(error)
	}

  msg(msg){
	  Alert.alert( 'Mensaje', msg, [ {text: 'OK', onPress: () => console.log('OK Pressed')}], { cancelable: false } );
  }
  async registrarDatos () {
	  
	  	
	  	if(this.state.nombres==""){
		  	this.msg("Ingrese su nombre");
		  	return;
	  	}
	  	if(this.state.apellidos==""){
		  	this.msg("Ingrese su apellido");
		  	return;
	  	}
	  	if(this.state.empresa==""){
		  	this.msg("Ingrese su empresa");
		  	return;
	  	}
	  	if(this.state.email==""){
		  	this.msg("Email es requerido");
		  	return;
	  	}
	  	if(!this.validateEmail(this.state.email)){
		  	this.msg("Ingrese un email válido");
		  	return;
	  	}
	  	if(tipocliente==""){
		  	this.msg("Tipo de cliente es requerido");
		  	return;
	  	}
	  	var email = this.state.email;
	  	if(tipocliente==3){
		  	if(email.indexOf("@hp.com")!=-1 || email.indexOf("@atomikal.com")!=-1 || email.indexOf("@merino.com.pe")!=-1){
			  	
		  	}else{
			  	this.msg("Debe registrarse con su email de HP para continuar.");
			  	return;
		  	}
		  	
	  	}

	  	if(email.indexOf("@gmail.com")!=-1 || email.indexOf("@outlook.com")!=-1 || email.indexOf("@hotmail.com")!=-1 || email.indexOf("@yahoo.com")!=-1){
			  	this.msg("Recuerde registrarse con su email corporativo para continuar.");
			  	return;
		 }
		  	 	
	  	if(pais==""){
		  	this.msg("Seleccione su país");
		  	return;
	  	}
	  	if(this.state.checkTerminos==false){
		  	this.msg("Debe aceptar los términos y condiciones.");
		  	return;
	  	}
    
        try {
	        //this.setState({loading:true});
	        var headers = {'Content-Type': 'text/xml;charset=UTF-8','SOAPAction':'"http://tempuri.org/IHPServicios/Usuario_Registro"'};
	        
	        var body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
			   body += '<soapenv:Header/>';
			   body += '<soapenv:Body>';
			      body += '<tem:Usuario_Registro>';
			         body += '<tem:int_IdPais>'+pais+'</tem:int_IdPais>';
			         body += '<tem:int_IdTipoCliente>'+tipocliente+'</tem:int_IdTipoCliente>';
			         body += '<tem:var_Nombres>'+this.state.nombres+'</tem:var_Nombres>';
			         body += '<tem:var_Apellidos>'+this.state.apellidos+'</tem:var_Apellidos>';
			         body += '<tem:var_Email>'+this.state.email+'</tem:var_Email>';
			         body += '<tem:var_Empresa>'+this.state.empresa+'</tem:var_Empresa>';
			      body += '</tem:Usuario_Registro>';
			   body += '</soapenv:Body>';
			body += '</soapenv:Envelope>';
			
			
			
			this.goFetchRegister(body,headers);
			/*
			let response = await fetch(global.servicios,{method: "POST",headers:headers,body:body});
			console.warn("*2");
			let responseText = await response.text();
			console.warn("*3");
			console.warn("*responseText "+responseText);
            parseString(responseText, function (err, result) {
	            var rpta = result['s:Envelope']['s:Body'][0].Usuario_RegistroResponse[0].Usuario_RegistroResult;
	            console.warn("*0"+rpta);
	            var resp = JSON.parse(rpta);
	            
	            console.warn("*loading "+loading);
			    scope.setState({loading:false});
			    if(tipocliente==3){
				    tempuser = resp.usuario;
				    tempuser.paisid = pais;
					tempuser.tipoclienteid = tipocliente;
					tempuser.codigo = resp.usuario.codigo;
					tempuser.cotizador = new Array(); 
					tempuser.favoritos = new Array();
				    codigoValidacion = resp.usuario.codigo;
				    console.warn(codigoValidacion);
				    Reactotron.log("CODIGO "+codigoValidacion);
				    scope.setState({Alert_Visibility:true});
				    
			    }else{
				    global.user = resp.usuario;
					global.user.paisid = pais;
					global.user.codigo = "";
					global.user.tipoclienteid = tipocliente;
					global.user.cotizador = new Array(); 
					global.user.favoritos = new Array(); 
					Reactotron.log(global.user)
					
				    global.saveUser()
				    scope.props.onNext();
			    }
			    
			   });
			 */
			    
			

        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
	componentWillMount(){
		Reactotron.log("Pais_Listado")
		console.warn(" global.api "+global.api);
		console.warn(" Pais_Listado ");
							fetch( global.api+'/Pais_Listado')
						        .then(response => response.text())
						        .then((response) => {
							        console.warn(" Pais_Listado RESPONSE !");

							        
							        console.warn(response);
							        
						            parseString(response, function (err, result) {
							            
							            var countries = JSON.parse(result.string._);
							            Reactotron.log(countries)
							            console.warn(countries);
							            if(countries.length>0){
								            Reactotron.log("SET COUNTRIES");
								            console.warn("SET COUNTRIES");
								            scope.setState({paises:countries});
							            }
						                
						            });
						        }).catch((err) => {
							        console.warn(err);
						            //Reactotron.log("message : "+err.message)
						        })
  	}
  	onSelect(item,t) {
   		pais = item.value;
   		for(var i=0;i<paises.length;i++){
	   		if(pais==paises[i].id){
		   		politicas=paises[i].politicas;
		   		Reactotron.log(paises[i].politicas);
		   		break;
		   		
	   		}
   		}
   		this.setState({value:t});
  	}
  	onSelect2(item,t) {
	  
	 	tipocliente = item.value;
	 	this.setState({value2:t});
  	}
	register(){
		//this.props.onNext();			
	}

  	doneNombre(){
	  	scope.txtField2.focus();
  	}
  	doneApellido(){
	  	scope.txtField3.focus();
  	}
	render() {
		
		var checkBoxcontent = {width:'100%',backgroundColor:'transparent',flexDirection:'row',paddingLeft:0,marginTop:20}
		var checkBox ={width:25,height:25,backgroundColor:'#0096d6',borderWidth:0,borderColor:'#999', flex:0, alignItems:"center", justifyContent:"center"}
		var checkBoxLabel = {marginTop:0,color:'#000',fontSize:14,fontWeight:'200',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',backgroundColor:'transparent'}
		
		
		var terminos = (<View>
						<View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { 
										 if(this.state.checkTerminos){
											  this.setState({checkTerminos:false})
										 }else{
											  this.setState({checkTerminos:true})
										 }
										 
									 }}>
									 	 
									 	{this.state.checkTerminos ? (
									        <Icon name={"check"} color="#fff" size={15}></Icon>
									      ) : (
									        <View></View>
									      )}
									 </TouchableOpacity>
									 
									 <TouchableOpacity onPress={ () => { 

											 if(politicas!=''){
												 Linking.canOpenURL(politicas).then(supported => {
											      if (supported) {
											        Linking.openURL(politicas);
											      } else {
											        Reactotron.log('Don\'t know how to open URI: ' + politicas);
											      }
											    });												 
											 }
										 
									 }} style={{backgroundColor:'transparent',marginLeft:10,flex:1}}>
									 	<Text style={[checkBoxLabel,{marginLeft:0}]}>  
									 	<Text>Acepto los</Text>
									 	<Text style={{color:'#0096d6'}}> Términos y Condiciones y Políticas de Privacidad</Text>
									 	 <Text > del Servicio</Text>
									 	 </Text>  
									 </TouchableOpacity>
								 </View>
								 <View style={{backgroundColor:'transparent'}}>
								 <Text style={[checkBoxLabel,{marginLeft:0,marginTop:6}]}>  
									 	De no registrar un correo corporativo, HP no podrá responder las cotizaciones solicitadas
									 	 </Text>
								</View>
						</View>)
		
		const {
			containerStyle
		} = this.props
		var optionStyle = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',color:'#575757'};
		var optBorder = {borderBottomWidth:0.3, borderBottomColor:'#e0e1e1'};
		var loading = (<View/>);
		if(this.state.loading){
				loading = (<View style={{position:'absolute',left:0,
					right:0,top:0,bottom:0,justifyContent:'center',
				alignItems:'center',backgroundColor:'rgba(0,0,0,0.5)'}}>
				<Text>{this.state.logger}</Text>
				<ActivityIndicator size="large" color="#FFF" />
				</View>);
		}
		var errorJSX = (<View></View>);
		if(this.state.errorCodigo!=""){
			errorJSX = (<Text style={[styles.Alert_Message,{color:'red',fontSize:12,padding:0,margin:0}]}>{this.state.errorCodigo}</Text>)
		}
		
		/*
			
			<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{codigoValidacion}</Text>
					
					*/
					
		return (<View style={{flex:1,backgroundColor:'white',justifyContent:'center'}}>
				<Modal visible={this.state.Alert_Visibility} transparent={true} animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Alert_Visibility)} } >

            <View style={{ flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor:'rgba(0,0,0,0.5)' }}>
 
 
                <View style={styles.Alert_Main_View}>
 
					<View style={{width:'100%',height:50,backgroundColor:'#0096d6',justifyContent:'center' }}>
                    <Text style={styles.Alert_Title}>HP Employee</Text>
					</View>

					
					
					<View style={{ width: '100%', flex:1,backgroundColor:'transparent',justifyContent:'center',alignItems:'center'}}>
                    <Text style={styles.Alert_Message}>Para finalizar tu registro, ingresa el código de verificación que hemos enviado a tu email*.</Text>
                    
                    <TextInput
          placeholderTextColor='#000'
          underlineColorAndroid='transparent'
          placeholder={'Código de verificación'} onChangeText={(cod_verificacion) => this.setState({cod_verificacion:cod_verificacion})} value={this.state.text}
          style={{width: "88%",fontFamily:'HP Simplified',textAlign:'center',
		height: 34, margin: 10,borderColor: '#000',fontWeight:'200',
		borderWidth: 0,backgroundColor:'#eaeaea',padding:10}}   />
		
	
					
		{errorJSX}
		
          
          
		<TouchableOpacity onPress={()=>{
						if(codigoValidacion==this.state.cod_verificacion){
							scope.setState({Alert_Visibility:false});
						    global.user = tempuser;
						    global.saveUser()
							scope.props.onNext();
					    }else{
						    scope.setState({errorCodigo:"Codigo incorrecto."});
					    }
						}} style={{marginTop:15,width:120,height:35,borderWidth:0.7,borderColor:'black',justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'FINALIZAR'}</Text>
					
					</TouchableOpacity>
					
					<Text style={styles.Alert_Message}>* No olvides revisar tu bandeja de correos no deseado.</Text>
					
					</View>


                

						
                    
					</View>
 
            </View>
 
 
        </Modal>
				<KeyboardAwareScrollView>
				<ScrollView style={ {flex: 1,backgroundColor:'white'} }>
					
					<View style={{flex: 1, justifyContent: "center", alignItems: "stretch" }}>
					<View style={{flex: 1, padding:20 }}> 
					<View style={{ justifyContent:'center',alignItems:'center',marginTop:10 }}>
					<Image
						style={[ {width:55,height:55,marginBottom:5,marginTop:10} ]}
						source={ img.hp_nav }/>
						 
						<Text
						style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',color:'#000',
							fontSize:25,textAlign:'center',fontWeight:'300' }}>
						{"BIENVENIDO"}
					</Text>
					
					
					<Text
						style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',lineHeight:16,color:'#000',
							fontSize:16,textAlign:'center',padding:10,paddingBottom:30,fontWeight:'200' }}>
						{"Llena los datos a continuación y descubre\ntodo lo que HP tiene para tu empresa"}
					</Text>
					</View>
					
					
					
					<View style={[ style.containerR, containerStyle ]}>
					<TextInput
          placeholderTextColor='#000'
          underlineColorAndroid='transparent'
          ref={ (c) => this.txtField1 = c }
          onSubmitEditing={this.doneNombre}
          keyboardType="default"
          placeholder={'Nombre'} 
          onChangeText={(nombres) => this.setState({nombres:nombres})}
          value={this.state.nombres}
          style={style.inputContainerInput}   />

          <TextInput
          placeholderTextColor='#000'
          underlineColorAndroid='transparent'
          ref={ (c) => this.txtField2 = c }
          onSubmitEditing={this.doneApellido}
          placeholder={'Apellido'} onChangeText={(apellidos) => this.setState({apellidos:apellidos})} value={this.state.text}
          value={this.state.apellidos}
          style={style.inputContainerInput}   />
          
           <TextInput
          placeholderTextColor='#000'
          underlineColorAndroid='transparent'
          ref={ (c) => this.txtField4 = c }
          placeholder={'Empresa'} onChangeText={(empresa) => this.setState({empresa:empresa})} value={this.state.text}
          value={this.state.empresa}
          style={style.inputContainerInput}   />

          <TextInput
          placeholderTextColor='#000'
          underlineColorAndroid='transparent'
          ref={ (c) => this.txtField3 = c }
          placeholder={'Email corporativo'} onChangeText={(email) => this.setState({email:email})} value={this.state.text}
          value={this.state.email}
          style={style.inputContainerInput}   />
          
          <Select
            onSelect = {this.onSelect2.bind(this)}
            defaultText  = {this.state.value2}
            style = {{backgroundColor:'#eaeaea',borderWidth:0,width: "100%",marginBottom: 15,padding:0,paddingLeft:10}}
            textStyle = {{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontWeight:'200',color:'#000'}}
            transparent = {true}
            backdropStyle  = {{backgroundColor : 'rgba(0,0,0,0.6)'}}
            indicatorIcon = {(<View style={{justifyContent:'center',width:34,height:34,
	            padding:0,backgroundColor:'transparent'}}>
            	<EvilIcons name={'chevron-down'} size={34} color="#999"/>
            </View>)}
            optionListStyle = {{backgroundColor : "#f6f7f7",borderWidth:1,borderColor:'#F3F3F3',height:110}}
          >

          {this.state.tipoclientes.map(r => <Option key={r.id} value = {{value:r.cliente.id}} styleText={optionStyle} style={optBorder}>{r.cliente.nombre}</Option>)}
        </Select>
        
        <Select onSelect = {this.onSelect.bind(this)}
					            defaultText  = {this.state.value}
					            style = {{backgroundColor:'#eaeaea',borderWidth:0,
						            width: "100%",marginBottom: 15,padding:0,paddingLeft:10}}
					            textStyle = {{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontWeight:'200',color:'#000'}}
					            transparent = {true}
					            backdropStyle  = {{backgroundColor : 'rgba(0,0,0,0.6)'}}
					            indicatorIcon = {(<View style={{justifyContent:'center',width:34,height:34,
								padding:0,backgroundColor:'transparent'}}
								optionListStyle={{backgroundColor:'red',height: 600}}
								>
            	<EvilIcons name={'chevron-down'} size={34} color="#999"/>
            </View>)}
            optionListStyle = {{backgroundColor : "#f6f7f7",borderWidth:1,borderColor:'#F3F3F3',height: '80%',maxHeight:370}}
          >
		  {this.state.paises.map(r => <Option key={r.pais.id} value = {{value:r.pais.id}} styleText={optionStyle} style={optBorder}>{r.pais.pais}</Option>)}
	    		
        </Select>
        
        
        						

			<TouchableOpacity style={{backgroundColor:'#0096d6',alignItems:'center',width:'100%',height:50,justifyContent:'center'}} onPress={()=>{ this.registrarDatos(); }}>
				<Text style={{fontWeight:'400',color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:18}}>{'INGRESAR'}</Text>
			</TouchableOpacity>
			
			
			{terminos}
					
		        </View>
					
					
						</View> 
						</View>
						
						
						
			
			
						
						
		        </ScrollView>
		        </KeyboardAwareScrollView>
		        {loading}
		        </View>)
	}
}

const styles = StyleSheet.create({

Alert_Main_View:{
 
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor : "#fff", 
  height: 300 ,
  width: '90%',
  borderRadius:0,
 
},
 
Alert_Title:{
 
  fontSize: 24, 
  color: "#fff",
  textAlign: 'center',
  fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
  padding: 0, 
},

Alert_Message:{
 
    fontSize: 16, 
    color: "#444444",
    textAlign: 'center',
    fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
    padding: 10,   
  },

buttonStyle: {
    
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'

},
   
TextStyle:{
    color:'#fff',
    textAlign:'center',
    fontSize: 22,
    marginTop: -5
}
 
});
