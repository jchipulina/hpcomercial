
import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import { View, Text ,ActivityIndicator,StatusBar} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Image,Platform,AsyncStorage } from "react-native"
import { img } from "@media"
import Home from "./Home"
import Cover from "./Cover"
import Suministros from "./Suministros"
import SuministroScreen from "./SuministroScreen"
import PlanetPartners from "./PlanetPartners"
import Question from "./Question"
import Employee from "./Employee"
import PDFVista from "./PDFVista"
import GridList from "./GridList"
import CarePacks from "./CarePacks"
import FichaTecnica from "./FichaTecnica"
import Comparar from "./Comparar"
import EncuentraLaTinta from "./EncuentraLaTinta"
import ListadoCaracteristicas from "./ListadoCaracteristicas"
import VideoVista from "./VideoVista"
import CompararBusqueda from "./CompararBusqueda"
import Registry from "./Registry"
import AnalisisCompetencia from "./AnalisisCompetencia"
import ComputoCategories from "./ComputoCategories"
import ImpresionCategories from "./ImpresionCategories"
import ImpresionList from "./ImpresionList"
import ItemProfile from "./ItemProfile"
import ItemProfileProduct from "./ItemProfileProduct"
import Resultados from "./Resultados"
import Cotizador from "./Cotizador"
import Caracteristicas from "./Caracteristicas"
import Buscar from "./Buscar"
import Herramientas from "./Herramientas"
import Impresion from "./Impresion"
import React, { Component } from "react"
import { DrawerContent } from "@components";
import Favoritos from "./Favoritos"
import Reactotron from 'reactotron-react-native'
import firebase from '@react-native-firebase/app';
import analytics from '@react-native-firebase/analytics';

global.featurePrint = new Array();
global.featureComp = new Array();

async function trackScreenView(screen) {
	await analytics().setCurrentScreen(screen, screen);
}
async function logEvent(screen,customDimensions) {
	await analytics().logEvent(screen,customDimensions);
}  

function removeSpaces(text){
	const newText = text.split(/\s/).join('');
	return newText; 
}	  
global.trackScreenView = function(screen,customDimensions=null){
	
	Reactotron.log("--GA "+screen)
	Reactotron.log(customDimensions)
	if(customDimensions!=null){
		console.warn("--GA Event "+removeSpaces(screen))
		console.warn("--dimension-- "+customDimensions+" --dimension--")
		logEvent(removeSpaces(screen),customDimensions);
	}else{
		console.warn("--GA Screen "+screen)
		
		trackScreenView(screen);
	}
}
function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}    
const rutas = {
    	Cover: {   screen: Cover,   },
    	VideoVista: {   screen: VideoVista,   },
    	PlanetPartners: {   screen: PlanetPartners,   },
    	FichaTecnica: {   screen: FichaTecnica,   },
    	EncuentraLaTinta: {   screen: EncuentraLaTinta,   },
    	AnalisisCompetencia: {   screen: AnalisisCompetencia,   },
    	Question: {   screen: Question,   },
    	CarePacks: {   screen: CarePacks,   },
    	Cotizador: {   screen: Cotizador,   },
    	Resultados: {   screen: Resultados,   },
    	PDFVista: {   screen: PDFVista,   },
    	SuministroScreen: {   screen: SuministroScreen,   },
    	Favoritos: {   screen: Favoritos,   },
		Registry: {   screen: Registry,   },
		Home: {   screen: Home,   },
		Buscar: {   screen: Buscar,   },
		ComputoCategories: {   screen: ComputoCategories,   },
		ImpresionCategories: {   screen: ImpresionCategories,   },
		ListadoCaracteristicas: {   screen: ListadoCaracteristicas,   },
		GridList: {   screen: GridList,   },
		ImpresionList: {   screen: GridList,   },
		ItemProfile: {   screen: ItemProfile, },
		ItemProfileProduct: {   screen: ItemProfileProduct, },
		Employee: {   screen: Employee, },
		Comparar: {   screen: Comparar,   },
		CompararBusqueda: {   screen: CompararBusqueda,   },
};


const MainNavigator = createStackNavigator(rutas,{  lazy: true, headerMode: 'none' });
const App = createAppContainer(MainNavigator);
export default () => (
  <App
    onNavigationStateChange={(prevState, currentState) => {
      const currentScreen = getActiveRouteName(currentState);
      const prevScreen = getActiveRouteName(prevState);
      //console.warn(currentScreen);
	  if(prevScreen=="Cotizador" && currentScreen=="ItemProfile"){
		  global.updateLikes();
	  }
	  if(prevScreen=="Favoritos" && currentScreen=="ItemProfile"){
			global.updateLikes();	  
	  }
	  if(prevScreen=="ItemProfileProduct" && currentScreen=="Favoritos"){
			global.updateFavorites();	  
	  }
	  if(prevScreen=="Favoritos" && currentScreen=="ItemProfileProduct"){
			//global.updateLikesProducto();	
	  }
	  if(prevScreen=="CompararBusqueda" && currentScreen=="Comparar"){
			 global.screenComparar.setProducto2();
	  }
	   if(prevScreen=="Cotizador" && currentScreen=="EncuentraLaTinta"){
			 global.screenEncuentraLaTinta.updateNav();
	  }
	 
    }}
  />
);
