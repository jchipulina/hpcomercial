import React, {Component} from "react";
import { PDFVista as Pantalla } from "@screens";

export default class sPDFVista extends Component {
	static navigationOptions = ({ navigation }) => ({ title: '', tabBarLabel: '', gesturesEnabled: false});
	render() {

		return <Pantalla navigation={this.props.navigation}/>
	}
}
