import React, { Component } from "react";

import {
  AppRegistry,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  Linking,
  View,
} from 'react-native';

import { connect } from "react-redux";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navbar } from "@components";
import Pdf from 'react-native-pdf';
import YouTube from 'react-native-youtube'

class VideoVista extends Component {
	
	constructor(props){
		super(props);
	}
	render(){
		var {navigation} = this.props;
        return (
            <View style={{flex:1,backgroundColor:'black'}}>
            	<YouTube
				 		  apiKey="AIzaSyDZeEQAaS87IEmfC-12YsWiOt5wTZsd0OY"
						  videoId="Wi2OaO1HEi8"   // The YouTube video ID
						  play={false}             // control playback of video with true/false
						  fullscreen={true}       // control whether the video should play in fullscreen or inline
						  showFullscreenButton={false}
						  loop={true}             // control whether the video should loop when ended
						  controls={1}
						  resumePlayAndroid={false}
						  onReady={e => this.setState({ isReady: true })}
						  onChangeFullscreen={(e)=>{
							  if(!e.isFullscreen){
								  setTimeout(()=>{
									  navigation.pop();
									  global.PlanetPartnersRef.update();
								  }, 500)
								  
							  }
						  }}
						  onChangeState={(e)=>{}}
						  onChangeQuality={e => this.setState({ quality: e.quality })}
						  onError={e => this.setState({ error: e.error })}
						
						  style={{ height:150}}
						/>
            </View>
        )
	}
}

export default VideoVista
