import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import { Categories } from "@containers";
import { Navbar,BottomBar } from "@components";
import Icon from 'react-native-vector-icons/FontAwesome';
import Reactotron from 'reactotron-react-native'
var parseString = require('react-native-xml2js').parseString;

export default class ImpresionCategories extends Component {
	
	constructor(props){
		super(props);
		global.trackScreenView("ImpresionCategories",{ tipocliente: global.user.tipocliente });
		this.state = { novedosos:global.featurePrint};
		Reactotron.log( "ImpresionCategories");
		
	}
	componentWillMount(){
		this.getProductosNovedosos(); 
	}
	async getProductosNovedosos(){
		var url =  global.api+'/ProductosNovedososListado?int_IdTipoProducto=2&int_IdUsuario='+global.user.id;
		let response = await fetch(url,{method: "GET"});
		let responseText = await response.text();
		global.featurePrint = new Array();
		var scope = this;
		parseString(responseText, function (err, result) {
			var resp = JSON.parse(result.string._);
			for(var i=0;i<resp.length;i++){
				global.featurePrint.push(resp[i].productonovedoso);
			}
			Reactotron.log( global.featurePrint );
			scope.setState({ novedosos:global.featurePrint})
		});
	}
	render() {
    var {navigation} = this.props;


	
		const list = [
			{
				key: "OfficeJet",
				title: "HP OfficeJet",
				image: img.boton_officejet,
				top: img.boton_officejet_interna,
				idcategoria: 1
			},
			{
				key: "PageWide",
				title: "HP PageWide",
				image: img.boton_pagewide,
				top: img.boton_pagewide_interna,
				idcategoria: 2
			},
			{
				key: "LaserJet",
				title: "HP LaserJet",
				image: img.boton_laserjet,
				top: img.boton_laserjet_interna,
				idcategoria: 3
			},{
				key: "ScanJet",
				title: "HP ScanJet",
				image: img.impq4,
				top: img.top7,
				idcategoria: 4
			},{
				key: "Suministros",
				title: "Suministros",
				image: img.impq5,
				top: img.top9,
				idcategoria: 15
			},{
				key: "encuentra",
				title: "encuentra",
				image: img.encuentra,
				top: img.encuentra,
				idcategoria: 15
			}
		]
		/*
			<TouchableOpacity onPress={()=>{
						 navigation.navigate('Question');
					}} style={{marginTop:2,marginLeft:2,marginRight:2,paddingTop:15,paddingBottom:15,backgroundColor:'#6b3a97',
						flexDirection:'row', justifyContent:'center', alignItems:'center'
					}}>
					<Image source={img.impresora} style={{width:45, height:45,resizeMode:'contain',tintColor:'white'}}/>
					<Text style={{marginLeft:10,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',
						fontWeight:Platform.OS === 'ios' ?'bold':'normal',color:'white',fontSize:20,lineHeight:18}}>
					{'Encuentra \ntu impresora ideal'}</Text>
					</TouchableOpacity>
					*/
		return(<View style={{flex:1}}>
            	<Navbar onBack={()=>{navigation.pop()}} onHome={()=>{navigation.navigate('Home')}}/>
				
				
				<View style={{flex:1,marginLeft:10,marginRight:10,marginBottom:8}}>
					<Categories title="Impresión" list={list} navigation={this.props.navigation} novedosos={this.state.novedosos}/>
					
				</View>
				
				{ global.user.pais!='Colombia' ? (
						<BottomBar onComputo={()=>{
							 navigation.navigate('ComputoCategories'); }} onSearch={()=>{
							navigation.navigate('Buscar',{term:'',type:'impresion'});
						}} />
					):(
						<BottomBar onImpresoraIdeal={()=>{
							navigation.navigate('Question');
							}} />
						
					)
					
				}
				
		    </View>)
	}
}
