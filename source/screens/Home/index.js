import React, { Component } from "react";
import {
  View,
  Text
} from 'react-native';
import { Start } from "@containers";
export default class Home extends Component {
	constructor(props){
		super(props);
		global.trackScreenView("Home",{ tipocliente: global.user.tipocliente }); 
	}
	render() {
    var {navigation} = this.props;
		return ( <Start navigation={navigation} style={{flex:1}} onComputo={()=>{
		
          navigation.navigate('ComputoCategories')
    }} onImpresion={()=>{
          navigation.navigate('ImpresionCategories')
    }} onBuscar={(term)=>{
          navigation.navigate('Buscar',{term:term});
    }}> </Start>);
	}
}
