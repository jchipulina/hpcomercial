import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  TextInput,
  Image,
  Platform,
  FlatList,
  Dimensions,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { img } from "@media"
import { Navbar } from "@components";
import FastImage from 'react-native-fast-image'
var {height, width} = Dimensions.get('window');
import Reactotron from 'reactotron-react-native'
import Dash from 'react-native-dash';
var parseString = require('react-native-xml2js').parseString;
	
var scope = null;	
class CompararBusqueda extends Component {
	
	constructor(props){
		super(props);
		scope = this;
		this.state = {busqueda:'',loading:false,text:''}
		var {type} = this.props.navigation.state.params;
		global.trackScreenView("CompararBusqueda",{ tipocliente: global.user.tipocliente}); 
	}
	componentWillMount(){
		//this.fetchData();
	}
	async fetchData (texto) {
		var {type} = this.props.navigation.state.params;
		//var type="computo"
		var value_type = '1';
		if(type=="impresion"){
			value_type = '2';
		}
	    try { 
			var url =  global.api+'/Producto_Busqueda';
			url += '?int_IdTipoProducto='+value_type+'&busqueda='+texto+'&int_IdUsuario='+global.user.id;;

			Reactotron.log('url '+url); 
			let response = await fetch(url);
			let responseText = await response.text();

            parseString(responseText, function (err, result) {
	            var resp = JSON.parse(result.string._);
	            
				Reactotron.log(resp); 
				
				scope.setState({ data: resp, loading: false });

	            
			});
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
    onChangeText(texto){
	   	//Reactotron.log("TEST");
	    this.setState({text:texto})
	    this.fetchData(texto);
	     
    }
    componentDidMount() {
    	
	}
	_keyExtractor = (item, index) => item.id;
    renderItem(item){
	   return (<TouchableOpacity onPress={()=>{
		   global.productoSelected = item.productob.id;
		  this.props.navigation.navigate("Comparar");
	   }} style={{backgroundColor:'white',
	   padding:15,paddingTop:8,paddingBottom:0,flexDirection:'column',justifyContent:'space-around'}}>
	
	       <Text style={{ flex:1, paddingLeft:10,paddingBottom:5,color: '#666',fontSize:16,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
	         {item.productob.name}
	       </Text>
		   <View style={{width:'100%', height:0.5, backgroundColor:'grey'}}></View>
	   </TouchableOpacity>);
	 }
	render() {

	var type = this.props.navigation.state.params;
	type="computo";
	
	var inputContainerInput = ({
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',flex:1,
		height: 40, marginBottom: 2,borderColor: '#c3c3c3',
		borderWidth: 1,backgroundColor:'#FFF',padding:10
	});
	
    var {navigation} = this.props;
	var icoSize = 60;
    var boxContainerStyle = {flex:1,paddingLeft:10,paddingRight:10};
    var boxStyle = {flexBasis:'50%',backgroundColor:'white',flexDirection:'row'};
           
		return (<View style={{flex:1,justifyContent:'center',backgroundColor:'#fff'}} >
				 	<Navbar onBack={()=>{
					 	global.productoSelected = -1;
					 	navigation.pop()}} showWinPro={(type=="computo")?true:false}
					 			onHome={()=>{navigation.navigate('Home')}}/>
					 	
					 	<View style={{width:'100%',height:140}}>
        					<Image style={{width:'100%',height:140,resizeMode:'cover'}} source={ img.comparar_bg } />
        					<View style={{position:'absolute',left:0,right:0,top:0,bottom:0,
	        					backgroundColor:'rgba(0,0,0,0.5)',justifyContent:'center'}}>
        						
        						<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        							<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'#FFF'}} source={img.ico_comparar} />
        							<View style={{marginLeft:5,flexDirection:'column',backgroundColor:'transparent'}}>
        							<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
	        							fontSize:24,lineHeight:24}}>{'COMPARADOR'}</Text>
        							
        						<Text style={{textAlign:'center',color:'#fff',fontSize:17,lineHeight:16,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{(type=="computo")?'equipos de cómputo':'equipos de impresión'}</Text>
        							</View>
        						</View>
        					</View>
						</View>
						
						
		            <View style={{flex:1,backgroundColor:'white'}}>
		            
		            
		            	<View style={{width:'90%',height:40,flexDirection:'row',backgroundColor:'grey',margin:15,marginBottom:5}}>
		            	<TextInput 
		            		underlineColorAndroid='transparent'
				          ref={ (c) => this.txtField1 = c }
				          keyboardType="default"
				          placeholder={'Buscar por modelo específico'} 
				          onChangeText={(texto) => this.onChangeText(texto)}
				          value={this.state.text}
				          style={inputContainerInput}   />
				          
				          
				          <View style={{width:50,height:40}}>
                <View style={[{flex:1,backgroundColor:'#00a1dd',alignItems:'center',justifyContent:'center'},{backgroundColor:'#a53894'}]} onPress={()=>{  }}>
                <Image style={{width:20,height:20,resizeMode:'contain',tintColor:'white'}} source={ img.lupa } />
                </View>
                </View>
                		</View>
				         
				     
          
				          <View style={{flex:1, backgroundColor:'white',marginBottom:20}}>
				          
				          <FlatList keyExtractor={this._keyExtractor} data={this.state.data}
          renderItem={({item}) => this.renderItem(item)} />
          
				          </View>
		              
		            </View>
		    		
					</View>)
	}
}

export default CompararBusqueda
