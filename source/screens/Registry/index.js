import React, { Component } from "react";
import {
  View,
  Text
} from 'react-native';
import { FormRegistry } from "@containers";

export default class Registry extends Component {
	constructor(props){
		super(props);
		global.trackScreenView("Cuestionario"); 
	}
	render() {
    var {navigation} = this.props
		return <FormRegistry onNext={()=>{
          navigation.navigate('Home')
    }}/>
	}
}
