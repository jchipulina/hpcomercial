import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  Linking,
  View,
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import Reactotron from 'reactotron-react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { Navbar,BottomBar } from "@components";
import { GridEmployee } from "@components";
var parseString = require('react-native-xml2js').parseString;
var scope;
var list = [
			{
				key: "datasheet",
				title: "Datasheet",
				enabled:true,
				pdf:'',
				image: img.emp_ico1,
				top: img.emp_ico1,
				idcategoria: 1
			},{
				key: "ficha_tecnica",
				title: "Ficha Técnica",
				enabled:true,
				image: img.emp_ico2,
				top: img.emp_ico2,
				idcategoria: 2
			},{
				key: "productoventaja",
				title: "Ventajas HP",
				enabled:false,
				image: img.emp_ico3,
				top: img.emp_ico3,
				idcategoria: 3
			},{
				key: "ventajas_competencia",
				title: "Ventajas\ncompetencia",
				enabled:false,
				image: img.emp_ico4,
				top: img.emp_ico4,
				idcategoria: 4
			},{
				key: "igualaigual",
				title: "Igual a igual",
				enabled:false,
				image: img.emp_ico5,
				top: img.emp_ico5,
				idcategoria: 5
			},{
				key: "seguridad",
				title: "Seguridad",
				enabled:false,
				image: img.emp_ico6,
				top: img.emp_ico6,
				idcategoria: 6
			},
		];
class Employee extends Component {
	
	constructor(props){
		super(props);
		scope = this;
		this.state = {analisisCompetencia:false,ventajas1:[],ventajas2:[],ventajas3:[],ventajas4:[],list:list};
		global.trackScreenView("Employee",{ tipocliente: global.user.tipocliente}); 
	}
		
	async fetchData () {
		var {producto} = this.props.navigation.state.params;

		try { 

			var url = global.api+'/ProductoPreventaVentajasListado?int_IdProducto='+producto.id+'&int_IdUsuario='+global.user.id
			let response = await fetch(url);
			let responseText = await response.text();
			laptops = new Array();
			scope.setState({ventajas1:[],ventajas2:[],ventajas3:[],ventajas4:[]});
			list[1].enabled=false;
			list[2].enabled=false;
			list[3].enabled=false;
			list[4].enabled=false;
			list[5].enabled=false;
			scope.setState({list:list});
            parseString(responseText, function (err, result) {
	            if(result.string._!=""){
		            var resp = JSON.parse(result.string._);
					Reactotron.log(resp);
					var ventajas1 = new Array();
					var ventajas2 = new Array();
					var ventajas3 = new Array();
					var ventajas4 = new Array();
		            for(var i=0;i<resp.length;i++){
			            
			            if(Number(resp[i].productoventaja.idcategoria)==2){
				            list[1].enabled=true;
			            }
			            
			            if(Number(resp[i].productoventaja.idcategoria)==3){
				            ventajas1.push(resp[i].productoventaja);
			            }
			            if(Number(resp[i].productoventaja.idcategoria)==4){
				            ventajas2.push(resp[i].productoventaja);
			            }
			            if(Number(resp[i].productoventaja.idcategoria)==5){
				            ventajas3.push(resp[i].productoventaja);
			            }
			            if(Number(resp[i].productoventaja.idcategoria)==6){
				            ventajas4.push(resp[i].productoventaja);
			            }
			            
			            if(Number(resp[i].productoventaja.idcategoria)==7){
				            //producto ventaja
				            scope.setState({analisisCompetencia:true});
			            }
			        }
				  	scope.setState({ventajas1:ventajas1,ventajas2:ventajas2,ventajas3:ventajas3,ventajas4:ventajas4});
				  	if(ventajas1.length>0){
					  	list[2].enabled=true;
				  	}
				  	if(ventajas2.length>0){
					  	list[3].enabled=true;
				  	}
				  	if(ventajas3.length>0){
					  	list[4].enabled=true;
				  	}
				  	if(ventajas4.length>0){
					  	list[5].enabled=true;
				  	}
				  	scope.setState({list:list});
	            }
	            
	            
			});
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
        
    }
    
    componentWillMount(){
		this.fetchData();
	}
	render() {
    	var {navigation} = this.props;
    	var {producto} = this.props.navigation.state.params;
    	var {datasheet} = this.props.navigation.state.params;
    	//var producto = {name:'TEST',id:'4'};
		//var datasheet = "";
		
		list[0].datasheet=datasheet;
		//scope.setState({list:list});
		
		var icoSize = 60;
		
		var bottombar = (<View></View>);
		
		if(this.state.analisisCompetencia){
			bottombar = (<BottomBar onAnalisisCompetencia={()=>{
						global.trackScreenView("Assets - Análisis Competencia",{ tipocliente: global.user.tipocliente });	
						 navigation.navigate('AnalisisCompetencia',{producto:producto});
					}} onSearch={()=>{
						navigation.navigate('Buscar',{term:'',type:'computo'});
					}} />);
		}else{
			bottombar = (<BottomBar onSearch={()=>{
						navigation.navigate('Buscar',{term:'',type:'computo'});
					}} />);
		}

		return (
				<View style={ {flex:1,backgroundColor:'white'}}>
				
					<Navbar onBack={()=>{this.props.navigation.pop()}} showWinPro={false}/>	
					<View style={{flex:1,marginLeft:8,marginRight:8,marginBottom:8}}>
					
					<View style={{margin:15,marginBottom:8,marginTop:20}}>
					<Text style={ {fontSize:20,color:'#0096d6',textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }>{producto.name.toUpperCase() }</Text>
					<Text style={ {color:'#0096d6',textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }>{'Assets'}</Text>
					</View>
					
					<GridEmployee ventajas1={this.state.ventajas1} 
					ventajas2={this.state.ventajas2}
					ventajas3={this.state.ventajas3}
					ventajas4={this.state.ventajas4}
					producto={producto}
					list={this.state.list} cols={2} gap={8} navigation={navigation} />	
					
					
					</View>
					
					{bottombar}
				</View>)




	}
}

export default Employee
