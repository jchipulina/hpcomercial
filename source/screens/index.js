import _Home from "./Home"
export const Home = _Home

import _Cover from "./Cover"
export const Cover = _Cover

import _Registry from "./Registry"
export const Registry = _Registry

import _PlanetPartners from "./PlanetPartners"
export const PlanetPartners = _PlanetPartners

import _PDFVista from "./PDFVista"
export const PDFVista = _PDFVista

import _AnalisisCompetencia from "./AnalisisCompetencia"
export const AnalisisCompetencia = _AnalisisCompetencia

import _ComputoCategories from "./ComputoCategories"
export const ComputoCategories = _ComputoCategories

import _FichaTecnica from "./FichaTecnica"
export const FichaTecnica = _FichaTecnica

import _Employee from "./Employee"
export const Employee = _Employee

import _ListadoCaracteristicas from "./ListadoCaracteristicas"
export const ListadoCaracteristicas = _ListadoCaracteristicas

import _ImpresionCategories from "./ImpresionCategories"
export const ImpresionCategories = _ImpresionCategories

import _Question from "./Question"
export const Question = _Question

import _Resultados from "./Resultados"
export const Resultados = _Resultados

import _Suministros from "./Suministros"
export const Suministros = _Suministros

import _Impresion from "./Impresion"
export const Impresion = _Impresion

import _Cotizador from "./Cotizador"
export const Cotizador = _Cotizador


import _ImpresionList from "./ImpresionList"
export const ImpresionList = _ImpresionList

import _Favoritos from "./Favoritos"
export const Favoritos = _Favoritos

import _Caracteristicas from "./Caracteristicas"
export const Caracteristicas = Caracteristicas

import _Buscar from "./Buscar"
export const Buscar = _Buscar

import _GridList from "./GridList"
export const GridList = _GridList

import _SuministroScreen from "./SuministroScreen"
export const SuministroScreen = _SuministroScreen

import _CarePacks from "./CarePacks"
export const CarePacks = _CarePacks

import _Comparar from "./Comparar"
export const Comparar = _Comparar

import _CompararBusqueda from "./CompararBusqueda"
export const CompararBusqueda = _CompararBusqueda

import _ItemProfile from "./ItemProfile"
export const ItemProfile = _ItemProfile

import _ItemProfileProduct from "./ItemProfileProduct"
export const ItemProfileProduct = _ItemProfileProduct

import _EncuentraLaTinta from "./EncuentraLaTinta"
export const EncuentraLaTinta = _EncuentraLaTinta

import _Herramientas from "./Herramientas"
export const Herramientas = _Herramientas

import _VideoVista from "./VideoVista"
export const VideoVista = _VideoVista
