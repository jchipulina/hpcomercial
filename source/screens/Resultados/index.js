import React, { Component } from "react";
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Dimensions,
  Platform,
  ScrollView,
  View,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { FeaturesBar } from "@components";
import { Navbar } from "@components";
import { BottomBar } from "@components";
import { img } from "@media"
import Dash from 'react-native-dash';
import FastImage from 'react-native-fast-image'
import Reactotron from 'reactotron-react-native'
var parseString = require('react-native-xml2js').parseString;

var {height, width} = Dimensions.get('window');

var scope;

class Resultados extends Component {
	constructor(props){
		super(props);
		scope = this;
		this.state = { lista:[],loaded:false,showButtonUp:false}
		global.trackScreenView("Resultados",{ tipocliente: global.user.tipocliente }); 
	
	}
	onPressCotizador(r){
		//Reactotron.log(r);
		//return;
	  var {navigation} = this.props;
	  this.addCotizador(r.producto);
	  navigation.navigate('Cotizador');
  }
  addCotizador(producto){
	  var exist = false;
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id == producto.id){
			  exist = true;
		  }
	  }
	  if(!exist){
		  global.user.cotizador.push(producto);
	  }
	  global.saveUser();
  }


	async obtenerDatos () {
		var {navigation} = this.props;
	  	var {respuestas} = this.props.navigation.state.params;
	  	
	  	//Reactotron.log("RESPUESTAS");
	  	//Reactotron.log(respuestas);
	  	
	  	/*
	  	var respuestas = [
			  true,
			  false,
			  false,
			  false,
			  false,
			  false,
			  false,
			  true,
			  false,
			  true,
			  false,
			  true,
			  false,
			  false,
			  false,
			  true,
			  false,
			  false,
			  false,
			  true,
			  false,
			  false,
			  false,
			  false
			]
			*/
			
	    var p1 = this.obtenerOpciones( respuestas,  0,  6, true );
		   var p2 = this.obtenerOpciones( respuestas,  7,  8, false );
		   var p3 = this.obtenerOpciones( respuestas,  9, 10, false );
		   var p4 = this.obtenerOpciones( respuestas, 11, 14, false );
		   var p5 = this.obtenerOpciones( respuestas, 15, 18, false );
		   var p6 = this.obtenerOpciones( respuestas, 19, 23, false );


        try {
	        this.setState({loading:true});

	        var headers = {'Content-Type': 'text/xml;charset=UTF-8','SOAPAction':'"http://tempuri.org/IHPServicios/Productos_Recomendados"'};
	        
			var url =  global.api+'/Productos_Recomendados';
			url += `?var_P1=`+p1;
			url += `&var_P2=`+p2;
			url += `&var_P3=`+p3;
			url += `&var_P4=`+p4;
			url += `&var_P5=`+p5;
			url += `&var_P6=`+p6;
			url += `&int_IdUsuario=`+global.user.id;

			Reactotron.log(url);
			let response = await fetch(url,{method: "GET",headers:headers});
			let responseText = await response.text();
			
			
			parseString(responseText, function (err, result) {
	           	
	           	
	           	Reactotron.log('result impresoras');
			   
			   	scope.setState({loaded:true});
			   	
			   	if(result.string._!=undefined){
				   	Reactotron.log('resultados');
				   	var resp = JSON.parse(result.string._);
				   		Reactotron.log(resp);
			   		scope.setState({lista:resp});
			   	}else{
				   	Reactotron.log('sin resultados');
				   	scope.setState({lista:[]});
			   	}
			   	
			   	
	           	/*
	           	
	           	if(Number(resp[0].exito)!=0){
		           	scope.setState({loading:false});
		           	Alert.alert( 'Gracias', 'Cotización enviada, en breve nos estaremos comunicando contigo.', [ {text: 'Aceptar', onPress: () => scope.dissmissAlert()}], { cancelable: false } );
	           	}
	            */
			});

        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
    

  componentWillMount(){
    this.setState({loading:true});

    this.obtenerDatos();

  }

  obtenerOpciones( arreglo, inicio, fin, multiple ){
   if (multiple) {
     let r = new Array();
     for (let i = inicio; i <= fin; i++) {
       if ( arreglo[ i ] ) {
         r.push(( "0" + ( i + 1 ).toString()).substr( -2, 2 ));
       }
     }
     return r.join( "," );
   }
   else{
     for (let i = inicio; i <= fin; i++) {
       if ( arreglo[ i ] ) {
         return ( "0" + ( i + 1 ).toString()).substr( -2, 2 );
       }
     }
   }

 }
 	onRemoveItem(r){
	 	var newlista = new Array();
					for(var i=0;i<this.state.lista.length;i++){
						if(r.producto.id != this.state.lista[i].producto.id){
							newlista.push(this.state.lista[i]);
						}
					}
					scope.setState({lista:newlista}); 
					
 	}
	render() {

		var {navigation} = this.props;
    //var {respuestas} = navigation.state.params;
    //Reactotron.log( "resultados" );
    // Reactotron.log( respuestas );

		var icoSize = 60;
    //var boxContainerStyle = {flexWrap:'wrap',width:'100%',backgroundColor:'white', flexDirection:'row'};
    //var boxStyle = {flexBasis:'33.3%',backgroundColor:'white',flexDirection:'row'};
    //var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:'HP Simplified'};
    
    
    var boxContainerStyle = {width:'100%',backgroundColor:'white', flexDirection:'column'};
    var boxStyle = {flex:1,backgroundColor:'transparent',flexDirection:'row'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};

	  //var laptops = new Array();
		/*laptops.push({id:1,label:"LJ MFP M477fnw",item_img:img.imp1});
    laptops.push({id:2,label:"LJ MFP M477fnw",item_img:img.imp2});
    laptops.push({id:3,label:"LJ MFP M477fnw",item_img:img.imp3});

    laptops.push({id:4,label:"LJ MFP M477fnw",item_img:img.imp1});
    laptops.push({id:5,label:"LJ MFP M477fnw",item_img:img.imp2});
    laptops.push({id:6,label:"LJ MFP M477fnw",item_img:img.imp3});

    laptops.push({id:7,label:"LJ MFP M477fnw",item_img:img.imp1});
    laptops.push({id:8,label:"LJ MFP M477fnw",item_img:img.imp1});
    laptops.push({id:9,label:"LJ MFP M477fnw",item_img:img.imp1});

    laptops.push({id:10,label:"LJ MFP M477fnw",item_img:img.imp1});
    laptops.push({id:11,label:"LJ MFP M477fnw",item_img:img.imp1});
    laptops.push({id:12,label:"LJ MFP M477fnw",item_img:img.imp1});
    */
		
		var topheader = (<View/>)
		var scrollview = (<View/>)
		
		
		
		 var btnUp = (<View/>);
			if(this.state.showButtonUp){
				btnUp = (<TouchableOpacity onPress={()=>{ 
					this.scrollView.scrollTo({y: 0})
				}} 
				style={{width:30, height:30,borderRadius:15,justifyContent:'center',alignItems:'center',
					backgroundColor:'rgba(0,0,0,0.5)',position:'absolute',bottom:100,right:15}}>
					<Icon name={'angle-up'} size={15} color="#fff"/>
				</TouchableOpacity>);
			}
			
		if(this.state.lista.length>0){
			
			topheader = (<View style={{width:'100%',paddingLeft:30,paddingRight:30,backgroundColor:'white' }}>
						 	<Text style={{textAlign:'left',color:'#0096d6',fontSize:18,
							 	fontFamily:'HP Simplified',fontWeight: '200'}}>
							 	<Text>ENCONTRAMOS </Text>
							 	<Text>{(this.state.lista.length>0)?this.state.lista.length+' IMPRESORAS IDEALES PARA UD':''} </Text>
							 	</Text>
						 </View>);
						 
						 
						 
						 
						 scrollview = (<ScrollView style={{flex:1}}
											ref={ref => this.scrollView = ref}
											onScroll={(event)=>{
												if(event.nativeEvent.contentOffset.y>200){
													this.setState({showButtonUp:true});
												}else{
													this.setState({showButtonUp:false});
												}
											}}
										>
    				  <View style={boxContainerStyle}>
    				  
					  	{topheader}
    					{this.state.lista.map(r => <View  key={r.id} style={boxStyle}>
                <TouchableOpacity onPress={()=>{
	                
                  navigation.navigate('ItemProfileProduct',{type:"impresion",producto:r.producto});
                  
                }} style={{flex:1, alignItems:'center',backgroundColor:'transparent',flexDirection:'column'}}>
               
    				<View style={{flex:1,justifyContent:'center', alignItems:'center',backgroundColor:'transparent',flexDirection:'row'}}>
    				
    						
    				<View style={{width:100,height:100,marginRight:5}}>
	    				<FastImage
		                	  style={{flex:1,width:100,height:100,backgroundColor:'white'}}
							  source={{
							    uri: r.producto.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
    				</View>		
    				<View style={{width:160,height:100,flexDirection:'column',backgroundColor:'transparent',justifyContent:'center'}}>
    				
    				<Text style={{marginTop:0,fontSize:14, fontWeight:'400',
	                color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.producto.name}</Text>
	                
					<Text style={{marginBottom:0,fontSize:11,
					color:'#868686',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Impresión'}</Text>
					
					<TouchableOpacity onPress={()=>{
						this.onPressCotizador(r);
						}} style={{width:140,height:30,borderWidth:0.7,borderColor:'black',marginTop:5,justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'COTIZAR AHORA'}</Text>
					
					</TouchableOpacity>
					
    				</View>			
    						
                
				
				<TouchableOpacity 
				onPress={()=>{ this.onRemoveItem(r); }}
				style={{ justifyContent:'center',alignItems:'center',width:22,height:22, backgroundColor:'transparent',
					position:'absolute',top:10,right:-20}}>
					<Material name={'close'} size={15} color="#999"/>
				</TouchableOpacity>
				</View>
				<View style={{width:'80%',backgroundColor:'#999',height:0.5}}></View>
				
                </TouchableOpacity>
						
    					</View>)}
    					
    					
    					


    					</View>
    				</ScrollView>);
		}else{
			scrollview = (<View/>);
			if(this.state.loaded){
						 scrollview = (<View style={{width:'100%',paddingLeft:30,paddingRight:30,backgroundColor:'white' }}>
						 	<Text style={{textAlign:'left',color:'#0096d6',fontSize:18,
							 	fontFamily:'HP Simplified',fontWeight: '200'}}>
							 	{'LOS SENTIMOS TU BUSQUEDA NO PRODUJO RESULTADOS.'}
							 	</Text>
						 </View>);
			}
			
		}
		return (<View style={{flex:1}} >
        			<Navbar onBack={()=>{ navigation.pop(); }} onHome={()=>{navigation.navigate('Home')}}/>
        			<View style={{width:'100%',height:150}}>
        				<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.resultados_imp } />
					</View>
        			<View style={{flex:1,backgroundColor:'white',padding:0 }}>
					 	<View style={{flex:1,padding:20,paddingBottom:0 }}>

					 		
					 		{scrollview}
					 		{btnUp}


						</View>


				 	</View>
			</View>)
	}
}

export default Resultados
