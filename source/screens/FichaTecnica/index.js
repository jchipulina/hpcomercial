import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  Linking,
  View,
  ScrollView
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import Reactotron from 'reactotron-react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { Navbar,BottomBar } from "@components";
import { GridEmployee } from "@components";
import FastImage from 'react-native-fast-image'
var parseString = require('react-native-xml2js').parseString;
var scope;
var fichas = new Array();
class FichaTecnica extends Component {
	
	constructor(props){
		super(props);
		scope = this;
		this.state = {fichas:[]};
		global.trackScreenView("Ficha Tecnica",{ tipocliente: global.user.tipocliente}); 
	}
		
	async fetchData () {
		var {producto} = this.props.navigation.state.params;

		try { 
			//producto.id = 1;
			var url = global.api+'/ProductoPreventaFichaTecnicaListado?int_IdProducto='+producto.id+'&int_IdUsuario='+global.user.id
			Reactotron.log("url "+url);
			fichas = new Array();
			let response = await fetch(url);
			let responseText = await response.text();
			laptops = new Array();
            parseString(responseText, function (err, result) {
	            if(result.string._!="" && result.string._!=undefined){
		            var resp = JSON.parse(result.string._);
					Reactotron.log(resp);
				  	for(var i=0;i<resp.length;i++){
					  	fichas.push(resp[i].productoficha);
				  	}
				  	scope.setState({fichas:fichas});
	            }

			});
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
        
    }
    
    componentWillMount(){
		this.fetchData();
	}
	render() {
    	var {navigation} = this.props;
    	var {producto} = this.props.navigation.state.params;
    	var {datasheet} = this.props.navigation.state.params;
		var icoSize = 60;
		
		//Reactotron.log('ficha_tecnica render');
	     //         Reactotron.log(producto.imagenes[0].imagen);

		
		return (
				<View style={ {flex:1,backgroundColor:'#fff'}}>
					<Navbar onBack={()=>{this.props.navigation.pop()}} showWinPro={false}/>	
					<ScrollView style={{flex:1}}>
					
					<View style={{margin:15,marginBottom:0,marginTop:20}}>
					<Text style={ {fontSize:20,color:'#0096d6',textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }>{producto.name.toUpperCase() }</Text>
					</View>
					
					<FastImage
                	  style={{width:'100%',height:200,flex:1}}
					  source={{
					    uri: producto.imagenes[0].imagen,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
					
					
					{this.state.fichas.map(ficha => <View style={{flex:1,alignItems:'center'}}>
		                
						
						
						
						<View style={{width:'90%',height:1,backgroundColor:'#edeced',marginTop:25}}></View>
						<View style={{width:'90%',height:3,backgroundColor:'#f4f7f7',marginTop:0}}></View>
						
						
		                <View style={{width:'90%',marginTop:0,justifyContent:'flex-start',alignItems:'flex-start',padding:10,flexDirection:'row'}}>
		                	<Text style={{color:'#0096d6',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'SKU - '+ficha.sku}</Text>	
		                </View>
		                <View style={{width:'90%',backgroundColor:'#f4f7f7',justifyContent:'center',padding:10,flexDirection:'row'}}>
			                <View style={{width:100,backgroundColor:'transparent',justifyContent:'center'}}>
				                <Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Procesador'} </Text>
			                </View>
			                 <View style={{flex:1}}>
				                <Text style={{color:'gray',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{ficha.procesador}
					            </Text>
			            	</View>
		                </View>
		                <View style={{width:'90%',backgroundColor:'#fff',justifyContent:'center',padding:10,flexDirection:'row'}}>
			                <View style={{width:100,backgroundColor:'transparent',justifyContent:'center'}}>
				                <Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{'Memoria'}
					            </Text>
			                </View>
			                 <View style={{flex:1}}>
				                <Text style={{color:'gray',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{ficha.memoria}
					            </Text>
			            	</View>
		                </View>
			            <View style={{width:'90%',backgroundColor:'#f4f7f7',justifyContent:'center',padding:10,flexDirection:'row'}}>
			                <View style={{width:100,backgroundColor:'transparent',justifyContent:'center'}}>
				                <Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{'Pantalla'}
					            </Text>
			                </View>
			                 <View style={{flex:1}}>
				                <Text style={{color:'gray',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{ficha.pantalla}
					            </Text>
			            	</View>
		                </View>
		                
						<View style={{width:'90%',backgroundColor:'#fff',justifyContent:'center',padding:10,flexDirection:'row'}}>
			                <View style={{width:100,backgroundColor:'transparent',justifyContent:'center'}}>
				                <Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{'Disco Duro'}
					            </Text>
			                </View>
			                 <View style={{flex:1}}>
				                <Text style={{color:'gray',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{ficha.discoduro}
					            </Text>
			            	</View>
		                </View>
		                
		                
		                {ficha.precio!="" ? (
					        <View style={{width:'90%',backgroundColor:'#f4f7f7',justifyContent:'center',padding:10,flexDirection:'row'}}>
			                <View style={{width:100,backgroundColor:'transparent',justifyContent:'center'}}>
				                <Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{'Precio'}
					            </Text>
			                </View>
			                 <View style={{flex:1}}>
				                <Text style={{color:'gray',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
									{ficha.precio}
					            </Text>
			            	</View>
		                </View>
					      ) : (
					        <View></View>
					      )}
					      
					      
					      {ficha.pdf!="" ? (<View style={{width:'90%',height:50,alignItems:'flex-start',justifyContent:'flex-start'}}><TouchableOpacity onPress={()=>{
						navigation.navigate("PDFVista",{pdf:ficha.pdf});
						}} style={{width:100,height:30,borderWidth:0.7,borderColor:'black',marginTop:10,justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'VER PDF'}</Text>
					
					</TouchableOpacity></View>) : (
					        <View></View>
					      )}
					      
					      
      
      
		               
		                </View>)}
		                
		                
		                
		                
		                
					</ScrollView>
					<BottomBar onAnalisisCompetencia={()=>{
						 //navigation.navigate('ImpresionCategories');
					}} onSearch={()=>{
						navigation.navigate('Buscar',{term:'',type:'computo'});
					}} />
				</View>)
	}
}

export default FichaTecnica
