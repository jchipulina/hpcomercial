import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import { Categories } from "@containers";
import { Navbar,BottomBar } from "@components";
import Icon from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import FastImage from 'react-native-fast-image'
import Reactotron from 'reactotron-react-native'
var parseString = require('react-native-xml2js').parseString;

export default class ListadoCaracteristicas extends Component {
	
	constructor(props){
		super(props);
		
		
		var {ventajas} = this.props.navigation.state.params;

				
		var laptops = new Array();
		
		for(var i=0;i<ventajas.length;i++){
			ventajas[i].id = i+1;
			laptops.push(ventajas[i]);
		}

			  	this.state = {laptops:laptops,tabSelected:0}

		global.trackScreenView("ListadoCaracteristicas",{ tipocliente: global.user.tipocliente}); 
	}

    
	render() {	  	
			  	
		var boxContainerStyle = {width:'100%',backgroundColor:'white', flexDirection:'column'};	  	
    	var {navigation} = this.props;
    	var {titulo} = this.props.navigation.state.params;
    	var {producto_nombre} = this.props.navigation.state.params;
    	
		return(<View style={{flex:1,backgroundColor:'white'}}>
            	<Navbar onBack={()=>{navigation.pop()}} onHome={()=>{navigation.navigate('Home')}}/>
            	
				<View style={{flex:1,marginLeft:10,marginRight:10,marginBottom:8}}>
					<View style={{margin:15,marginBottom:8,marginTop:20}}>
					<Text style={ {fontSize:20,color:'#0096d6',textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }>{producto_nombre.toUpperCase()}</Text>
					<Text style={ {color:'#0096d6',textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }><Text style={ {color:'gray'}}>Assets/</Text>{titulo}</Text>
					</View>
					
	             
	             <ScrollView style={{flex:1}} contentContainerStyle={{alignItems:'center'}} horizontal={false}
				ref={ref => this.scrollView = ref}
				onScroll={(event)=>{
					if(event.nativeEvent.contentOffset.y>200){
						this.setState({showButtonUp:true});
					}else{
						this.setState({showButtonUp:false});
					}
				}}
			>

            
    				  <View style={boxContainerStyle}>
					  
					 
					  	<Listado items={this.state.laptops} navigation={this.props.navigation} onSelected={(index)=>{ this.setState({tabSelected:index}); }} />
    					
    					</View>
    				</ScrollView>
					
					
					
				</View>
		    </View>)
	}
}



class Listado extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selected : 0
		}
	}
	
	render() {
		var boxStyle = {backgroundColor:'red'};
		var label1 = {marginTop:6,marginBottom:0,fontSize:16,color:'#000',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		 var label2 = {color:'black', fontWeight:'200',fontSize:14,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};               
		   var label3 = {marginTop:6,marginBottom:0,fontSize:16,color:'#FFF',fontWeight:'200',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
         
         

                        
		return (this.props.items.map(r => <View  key={r.id} style={{backgroundColor:(r.id%2==0)?'#FFF':'#f4f7f7'}}>
                <View onPress={()=>{
		                // navigation.navigate('ItemProfile',{type:type,item:r});
                }} style={{flex:1,alignItems:'center'}}>
                
                <View style={{width:'100%',height:1,backgroundColor:'#eaebeb'}}></View>
                <View style={{flex:1, flexDirection:'row'}}>
                
                <TouchableOpacity onPress={()=>{
					if(r.id!=this.state.selected){ this.setState({ selected:r.id })
					}else{ this.setState({ selected:0 }) }
                }} style={{flex:1,backgroundColor:'transparent',paddingLeft:20,
	                paddingBottom:18,paddingTop:10}}>
	                	 <Text style={label1}><Text style={[label1,{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',fontWeight:Platform.OS === 'ios' ? '600' : ''}]}>{(Number(r.id)<=9)?"0"+r.id+". ":""+r.id+". "}</Text>{r.name}</Text>
                </TouchableOpacity>
				<View style={{width:45,paddingRight:10, backgroundColor:'transparent',justifyContent:'center',alignItems:'flex-end'}}>
				
				
				
		
			        <TouchableOpacity onPress={()=>{
					if(r.id!=this.state.selected){ this.setState({ selected:r.id })
					}else{ this.setState({ selected:0 }) }
                }} style={{width:33, height:33, backgroundColor:'transparent'}}>

					<EvilIcons name={(this.state.selected!=r.id)?'plus':'minus'} size={33} color="gray"/>
			
				</TouchableOpacity>
			      
      
				
				</View>
                </View>
                </View>

				{this.state.selected==r.id &&
					<View>
				        <View style={{flex:1,alignItems:'center',backgroundColor:'black'}}>
		                <View style={{width:'100%',height:1,backgroundColor:'#eaebeb'}}></View>
		                <View style={{flex:1, flexDirection:'row'}}>
		                <View style={{flex:1,backgroundColor:'transparent',padding:12 }}>
			                
			                {r.imagen!="" && 
				                <FastImage
		                	  style={{width:'100%',height:180,flex:1}}
							  source={{
							    uri: r.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/> 
			                }
			               
			                <Text style={label3}>{r.descripcion}</Text>
			                
			                <Text style={[label3,{color:'#01aedc',marginTop:20,fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontWeight:Platform.OS === 'ios' ? '200' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>Análisis:</Text>
			                <Text style={[label3,{color:'#01aedc',marginBottom:5,fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>{r.analisis}</Text>
		                </View>

		                </View>
		                </View>
	                </View>}
      
                	
                	
    					</View>));

	}
}