import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Platform,
  View,
  Modal,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';

import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { img } from "@media"
import { Navbar } from "@components";
import FastImage from 'react-native-fast-image'
var {height, width} = Dimensions.get('window');
import Reactotron from 'reactotron-react-native'
import Dash from 'react-native-dash';
import HTML from 'react-native-render-html';

import Octicons from 'react-native-vector-icons/Octicons';
var parseString = require('react-native-xml2js').parseString;	
var scope = null;
var productoID1=0;
var productoID2=0;
var productoData = null;
var productoData2 = null;
var productoData3 = null;
var emptyArray = new Array({value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''});
var all_caracteristicas = new Array([],['','','','','','','','','','','','','','','','','','','','','','','','','','','',''],['','','','','','','','','','','','','','','','','','','','','','','','','','','','']);
all_caracteristicas[1] = new Array({value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''});
all_caracteristicas[2] = emptyArray;


var currentSector=1;


var comparableProducts = new Array();
global.productoSelected=-1;	
class AnalisisCompetencia extends Component {
	
	constructor(props){
		super(props);
		scope = this;
		this.state = {comparable:[],Alert_Visibility:false,caracteristicas:new Array([],['','','','','','','','','','','','','','','','','','','','','','','','','','','',''],['','','','','','','','','','','','','','','','','','','','','','','','','','','','']),item1:null,item2:null,item3:null}
		
		global.trackScreenView("AnalisisCompetencia",{ tipocliente: global.user.tipocliente});
	}

  selectedComparable(index){
	  this.setState({Alert_Visibility:false});
	  if(currentSector==2){
		  var producto = comparableProducts[index];
		  					for(var t=0;t<all_caracteristicas[0].length;t++){
					            var id = all_caracteristicas[0][t].id;
					            for(var h=0;h<producto.caracteristicas.length;h++){
						            var c = producto.caracteristicas[h];
					            	if(Number(c.codigo)==id){
						            	all_caracteristicas[1][t] = {value:c.valor};
						            	break;
					            	}
								}
				            }
				            
							var p_modelo = producto.marca+" "+producto.modelo;
							global.trackScreenView("Assets - Análisis Competencia",{ tipoCliente: global.user.tipocliente,modelo:p_modelo});
	            
							this.setState({item2:producto,caracteristicas:all_caracteristicas});
	  }
	  if(currentSector==3){
		  var producto = comparableProducts[index];

				            for(var t=0;t<all_caracteristicas[0].length;t++){
					            var id = all_caracteristicas[0][t].id;
					            for(var h=0;h<producto.caracteristicas.length;h++){
						            var c = producto.caracteristicas[h];
					            	if(Number(c.codigo)==id){
						            	all_caracteristicas[2][t] = {value:c.valor};
						            	break;
					            	}
								}
				            }
							var p_modelo = producto.marca+" "+producto.modelo;
							global.trackScreenView("Assets - Análisis Competencia",{ tipoCliente: global.user.tipocliente ,modelo:p_modelo});
				            
							this.setState({item3:producto,caracteristicas:all_caracteristicas});
							
	  }
	 
  }


	closeItem2(){
		all_caracteristicas[1] = new Array({value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''});
		this.setState({item2:null,caracteristicas:all_caracteristicas});
	}
	closeItem3(){
		all_caracteristicas[2] = new Array({value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''});
		this.setState({item3:null,caracteristicas:all_caracteristicas});
	}
	componentWillMount(){
		var {producto} = this.props.navigation.state.params;
		//Reactotron.log(producto);
		//var producto = {id:88,name:'XXX'};
		
		Reactotron.log("componentWillMount")
		this.setState({caracteristicas:new Array(emptyArray,emptyArray,emptyArray)});
		
		
		this.fetchMainProduct(producto);
		
	}
	
	async fetchMainProduct (producto) {
        try { 
			var url =  global.api+'/ProductoHPPreventaAnalisis_Listado';
			url += '?int_IdProducto='+producto.id+'&int_IdUsuario='+global.user.id;
			
			Reactotron.log(url);
			
			let response = await fetch(url);
			let responseText = await response.text();
			var respuesta = new Array();
			
			all_caracteristicas = new Array([],['','','','','','','','','','','','','','','','','','','','','','','','','','','',''],['','','','','','','','','','','','','','','','','','','','','','','','','','','','']);
all_caracteristicas[1] = new Array({value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''},{value:''});
all_caracteristicas[2] = emptyArray;


            parseString(responseText, function (err, result) {
	            
	             Reactotron.log(result.string._);
	            var resp = JSON.parse(result.string._);
	            Reactotron.log(resp);
				productoData = resp[0].producto;
				
				for(var i=0;i<productoData.caracteristicas.length;i++){
					all_caracteristicas[0].push({id:Number(productoData.caracteristicas[i].codigo),value:productoData.caracteristicas[i].valor});
				}
				scope.setState({item1:productoData});
				scope.setState({caracteristicas:all_caracteristicas});
				scope.fetchComparableProducts(producto);
			});
				           
     
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
    
    
    
    
	async fetchComparableProducts (producto) {
		//Reactotron.log('fetchComparableProducts');
        try { 
	        comparableProducts = new Array();
	        
			var url =  global.api+'/ProductoCompetenciaPreventaAnalisis_Listado';
			url += '?int_IdProducto='+producto.id+'&int_IdUsuario='+global.user.id;
			let response = await fetch(url);
			let responseText = await response.text();
			var respuesta = new Array();
			var position = 2;
            parseString(responseText, function (err, result) {
	            var resp = JSON.parse(result.string._);
	            for(var i=0;i<resp[0].productos.length;i++){
		            var producto = resp[0].productos[i].producto;
					
		            if(Number(producto.directo)==1){
			            if(position==2){
				            position++;
				            productoData2 = producto;
				            for(var t=0;t<all_caracteristicas[0].length;t++){
					            var id = all_caracteristicas[0][t].id;
					            for(var h=0;h<productoData2.caracteristicas.length;h++){
						            var c = productoData2.caracteristicas[h];
					            	if(Number(c.codigo)==id){
						            	all_caracteristicas[1][t] = {value:c.valor};
						            	break;
					            	}
								}
				            }
							scope.setState({item2:productoData2,caracteristicas:all_caracteristicas});
							comparableProducts.push(producto);
							continue;
			            }
			            if(position==3){
				            position++;
				            productoData3 = producto;
				            for(var t=0;t<all_caracteristicas[0].length;t++){
					            var id = all_caracteristicas[0][t].id;
					            for(var h=0;h<productoData3.caracteristicas.length;h++){
						            var c = productoData3.caracteristicas[h];
					            	if(Number(c.codigo)==id){
						            	all_caracteristicas[2][t] = {value:c.valor};
						            	break;
					            	}
								}
				            }
							scope.setState({item3:productoData3,caracteristicas:all_caracteristicas});
							comparableProducts.push(producto);
							continue;
			            }
		            }
		            comparableProducts.push(producto);
		            
		            scope.setState({comparable:comparableProducts});
	            }
			});
				           
     
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
	render() {

	var {producto} = this.props.navigation.state.params;
	
	//var producto = {id:88,name:'XXX'};

    var {navigation} = this.props;
	var icoSize = 60;
    	var caracteristicas_jsx = (<View/>);

			

		    	var caracteristicas = this.state.caracteristicas;
		    	
		    	Reactotron.log("caracteristicas");
		    	Reactotron.log(caracteristicas);
		    	
		    	Reactotron.log("*1 "+caracteristicas[0].length);
		    	Reactotron.log("*2 "+caracteristicas[1].length);
		    	Reactotron.log("*3 "+caracteristicas[2].length);
		    	
		    	Reactotron.log(caracteristicas[1]);
		    	Reactotron.log(caracteristicas[1][0]);
		    	Reactotron.log(caracteristicas[1][0].value);
		    	if(caracteristicas.length>0){	




				caracteristicas_jsx = (<View style={{flex:1,backgroundColor:'gray'}}>
                    {caracteristicas[0].map((r, index) => 
					        <View style={{flexDirection:'row',backgroundColor:(index%2==0)?'white':'#f6f6f6'}}>
					                  		<View style={{flex:1,borderStyle: 'solid',borderRightWidth:1,borderColor:'#d8d8d8'}}>
					                  		
					                  		<HTML tagsStyles = {{ p: { fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:13,fontWeight:'200',textAlign:'center' } }}containerStyle={{padding:5,width:'100%'}} 
					                  		html={'<p>'+((caracteristicas[0][index].value!=undefined)?caracteristicas[0][index].value:'-')+'</p>'} />
					                  		
					                  		</View>
					                  		<View style={{flex:1,justifyContent:'center',borderStyle: 'solid',borderRightWidth:1,borderColor:'#d8d8d8'}}>
					                  		
					                  		<HTML tagsStyles = {{ p: { fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:13,fontWeight:'200',textAlign:'center' } }}containerStyle={{padding:5,width:'100%'}} 
					                  		html={'<p>'+((caracteristicas[1][index].value!=undefined)?caracteristicas[1][index].value:'-')+'</p>'} />
					                  		
					                  		</View>
					                  		<View style={{flex:1,justifyContent:'center'}}>
					                  		
					                  		<HTML tagsStyles = {{ p: { fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:13,fontWeight:'200',textAlign:'center' } }}containerStyle={{padding:5,width:'100%'}} 
					                  		html={'<p>'+((caracteristicas[2][index].value!=undefined)?caracteristicas[2][index].value:'-')+'</p>'} />
					                  		
					                  		</View>
					                  	</View>)}
				</View>)
				
				}

    	var melements1 = (<View/>)
    	if(this.state.item1!=null){
	    	melements1 = (<View style={{paddingBottom:10}}>
	    				</View>)
    	}
    	var melements2 = (<View/>)
    	if(this.state.item2!=null){
	    	melements2 = (<View style={{paddingBottom:10}}>
	    				<View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                        <FastImage
	                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent' }}
						  source={{
						    uri: this.state.item2.icon1,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }}
						  resizeMode={FastImage.resizeMode.contain}
						/>
                        
                        
                      </View>
                      <View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <FastImage
                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent'}}
					  source={{
					    uri: this.state.item2.icon2,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
                      </View>
                      <View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <FastImage
                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent'}}
					  source={{
					    uri: this.state.item2.icon3,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
                     </View></View>)
    	}
		
		
		var cotizar1 = (<View/>)
		var cotizar2 = (<View/>)
		
		var product1 = (<View></View>)
		
		var product2 = (<TouchableOpacity  onPress={()=>{   currentSector=2;
									this.setState({Alert_Visibility:true});
			                      }} style={{flex:1,backgroundColor:'#FFF'}}>
			                      
			                      <View style={{justifyContent:'center',alignItems:'center',backgroundColor:'#f0f0f0',width:'100%', height:93 }} >
		<Image source={img.plus} style={{width:40,height:40,tintColor:'#0096d6'}}></Image>
								</View>
			<View style={{backgroundColor:'#0096d6',width:'100%', height:30,marginTop:7,justifyContent:'center',alignItems:'center'}} >
				<Text style={{marginBottom:0,fontSize:13,fontWeight:'400',
					color:'#fff',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Seleccione'}</Text>
			</View>
		</TouchableOpacity>)
		
		
		var product3 = (<TouchableOpacity  onPress={()=>{   currentSector=3;
									this.setState({Alert_Visibility:true});
			                      }} style={{flex:1,backgroundColor:'#FFF'}}> 
			                      
			                      <View style={{justifyContent:'center',alignItems:'center',backgroundColor:'#f0f0f0',width:'100%', height:93 }} >
		<Image source={img.plus} style={{width:40,height:40,tintColor:'#0096d6'}}></Image>
								</View>
			<View style={{backgroundColor:'#0096d6',width:'100%', height:30,marginTop:7,justifyContent:'center',alignItems:'center'}} >
				<Text style={{marginBottom:0,fontSize:13,fontWeight:'400',
					color:'#fff',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Seleccione'}</Text>
			</View>
		</TouchableOpacity>)
			
		
		var closeStyle = {alignItems:'center',justifyContent:'center',width:15, height:15,
	                    backgroundColor:'transparent',position:'absolute',right:0,top:0};
						
						
						/*
							
							
							*/
		if(this.state.item1!=null){
			product1 = (<View style={{flex:1,backgroundColor:'white'}}>
	                    <FastImage
		                	  style={{flex:1,width:'100%',height:75}}
							  source={{
							    uri: producto.imagenes[0].imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
	                    <Text style={{marginTop:5,marginBottom:5,textAlign:'center',color:'#000',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'HP'}</Text>
	                    
	                    <View style={{marginLeft:10,marginRight:10,minHeight:30,marginBottom:5,backgroundColor:'transparent',flexDirection:'row',
		                    borderTopWidth:1,borderTopColor:'#c3c3c3',borderBottomWidth:1,borderBottomColor:'#c3c3c3'}}>
	                      <View style={{flex:1,marginLeft:5,marginTop:0,justifyContent:'center'}}>
	                        <Text style={{textAlign:'center',color:'#0096d6',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item1.name}</Text>
	                      </View>
	                      
	                    </View></View>);
		}

		if(this.state.item2!=null){
			product2 = (<View style={{flex:1,backgroundColor:'white'}}>
	                    <FastImage
		                	  style={{flex:1,width:'100%',height:75}}
							  source={{
							    uri: this.state.item2.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
	                    <TouchableOpacity onPress={()=>{  this.closeItem2(); }} style={closeStyle}>
	                      <MaterialIcons name={'close'} size={17} color="#999"/>
	                    </TouchableOpacity>
	                    
	                    
	                    <Text style={{marginTop:5,marginBottom:5,textAlign:'center',color:'#000',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item2.marca}</Text>
	                    
	                    <View style={{marginLeft:10,marginRight:10,minHeight:30,marginBottom:5,backgroundColor:'transparent',flexDirection:'row',
		                    borderTopWidth:1,borderTopColor:'#c3c3c3',borderBottomWidth:1,borderBottomColor:'#c3c3c3'}}>
	                      <View style={{flex:1,marginLeft:5,marginTop:0,justifyContent:'center'}}>
	                        <Text style={{textAlign:'center',color:'#0096d6',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item2.modelo}</Text>
	                      </View>
	                      
	                    </View></View>);
		}
         if(this.state.item3!=null){
			product3 = (<View style={{flex:1,backgroundColor:'white'}}>
	                    <FastImage
		                	  style={{flex:1,width:'100%',height:75}}
							  source={{
							    uri: this.state.item3.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
	                    <TouchableOpacity onPress={()=>{  this.closeItem3(); }} style={closeStyle}>
	                      <MaterialIcons name={'close'} size={17} color="#999"/>
	                    </TouchableOpacity>
	                    
	                    
	                    <Text style={{marginTop:5,marginBottom:5,textAlign:'center',color:'#000',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item3.marca}</Text>
	                    
	                    <View style={{marginLeft:10,marginRight:10,minHeight:30,marginBottom:5,backgroundColor:'transparent',flexDirection:'row',
		                    borderTopWidth:1,borderTopColor:'#c3c3c3',borderBottomWidth:1,borderBottomColor:'#c3c3c3'}}>
	                      <View style={{flex:1,marginLeft:5,marginTop:0,justifyContent:'center'}}>
	                        <Text style={{textAlign:'center',color:'#0096d6',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item3.modelo}</Text>
	                      </View>
	                      
	                    </View></View>);
		}
		
			                    
		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}} >
		
		<Modal visible={this.state.Alert_Visibility} transparent={true} animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Alert_Visibility)} } >

            <View style={{ flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor:'rgba(0,0,0,0.5)' }}>
 
				
                <View style={{ alignItems: 'center',justifyContent: 'center',
						backgroundColor : "white",height: 200 , width: '90%', borderRadius:0}}>
					
					
					<View style={{width:'100%',height:15,marginTop:5,marginRight:20,alignItems:'flex-end'}}>
						<TouchableOpacity onPress={()=>{  this.setState({Alert_Visibility:false}); }} style={closeStyle}>
	                      <MaterialIcons name={'close'} size={20} color="#999"/>
	                    </TouchableOpacity>
					</View>
					
					
					<View style={{ width: '100%', flex:1,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>

					<View style={{paddingLeft:10,paddingRight:20,backgroundColor:'white',width:'100%',height:40}}>
	                    <Text style={{ fontSize: 16,  color: "#444444", textAlign: 'left',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg', padding: 10}}>
	                    	{'Selecciona el modelo a comparar'}
	                    </Text>
                    </View>
                    
                    <View style={{backgroundColor:'white',flex:1,width:'100%'}}>
                    	<ScrollView style={{flex:1,width:'100%',marginBottom:15}}>
		            
		    				<View style={{flex:1,paddingLeft:20,paddingRight:20,marginTop:0}}>
		    					
		    					{this.state.comparable.map((r, index) => 
									<View style={{flex:1}}>
					                  		<View style={{width:'100%',backgroundColor:'gray',height:0.5}}></View>
					            <TouchableOpacity disabled={(((scope.state.item2!=null) && r.id!=scope.state.item2.id) || ((scope.state.item3!=null) && r.id!=scope.state.item3.id))?false:true} style={{}} onPress={()=>{  this.selectedComparable(index); }} style={{width:'100%',height:40,justifyContent:'center',opacity:(((scope.state.item2!=null) && r.id!=scope.state.item2.id) || ((scope.state.item3!=null) && r.id!=scope.state.item3.id))?1:0.3, flexDirection:'row'}}>
					<Text style={{flex:1,marginTop:12,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.marca+" "+r.modelo}</Text>
					
					<View style={{width:40,paddingRight:10, backgroundColor:'white',justifyContent:'center',alignItems:'flex-end'}}>
				<Octicons name={'chevron-right'} size={16} color="#35b3e2"/>					
				</View>
					            </TouchableOpacity>
					                  	</View>)}
		    					
					            
		    				</View>
						</ScrollView>
                    </View>
					
					
					</View>
					</View>
 
            </View>
 
 
        </Modal>
        
        
        
				 	<Navbar onBack={()=>{navigation.pop()}} showWinPro={true} onHome={()=>{navigation.navigate('Home')}}/>
				 	
				 	<View style={{margin:15,marginBottom:12,marginTop:20}}>
					<Text style={ {fontSize:20,color:'#0096d6',textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }>{producto.name.toUpperCase() }</Text>
					
					<Text style={ {textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'} }>
					<Text style={ {color:'gray'}} >{'Assets/'}</Text>
					<Text style={ {color:'#0096d6'}}>{'Comparación ante la competencia'}</Text>
					</Text>
					
					</View>

		            <ScrollView style={{flex:1,width:'100%'}}>
		            
		    				<View style={{flex:1,paddingLeft:10,paddingRight:10}}>
					                  <View style={{flexDirection:'row'}}>
					                    <View style={{flex:1,backgroundColor:'white'}}>
						                    {product1}
										</View>
					                    <View style={{flex:1,marginLeft:0,backgroundColor:'white'}}>
							                {product2}
					                    </View>
					                    <View style={{flex:1,marginLeft:0,backgroundColor:'white'}}>
					                    	{product3}
					                    </View>
					                  </View>
					                  
					                    {caracteristicas_jsx}    
					                  
		
		    				</View>
						</ScrollView>
					</View>)
	}
}

export default AnalisisCompetencia
