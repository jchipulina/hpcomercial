import React, { Component } from "react";
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Dimensions,
  ScrollView,
  Platform,
  View,
  Alert,
  Modal,
  ActivityIndicator,
  TextInput
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { FeaturesBar } from "@components";
import { Navbar } from "@components";
import { BottomBar } from "@components";
import { img } from "@media"
import Dash from 'react-native-dash';
import {Select, Option} from "react-native-chooser";
import EvilIcons  from 'react-native-vector-icons/EvilIcons';
import Reactotron from 'reactotron-react-native';
import dismissKeyboard from 'react-native-dismiss-keyboard';

var parseString = require('react-native-xml2js').parseString;
var paises = new Array();
var {height, width} = Dimensions.get('window');
var scope = null;
var pais = '';
var tipocliente = '';

class Cotizador extends Component {
	constructor(props){
		super(props);
		scope = this;
		this.state = {Alert_Visibility:false,paises:[],loading:false,page:1,cotizador:global.user.cotizador,
			value:"País",value2:"Tipo de cliente"}
		global.trackScreenView("Cotizador",{ tipocliente: global.user.tipocliente}); 
	
	}
	doneTelefono(){
	  	scope.txtField2.focus();
  	}
	msg(msg){
	  Alert.alert( 'Mensaje', msg, [ {text: 'OK', onPress: () => console.log('OK Pressed')}], { cancelable: false } );
  }
	async registrarDatos () {
		var {navigation} = this.props;
	  	var params = {};
	  	params.id = global.user.id;
	  	params.pais = global.user.paisid;
	  	params.tipocliente = global.user.tipoclienteid;
	  	params.detalle = this.state.detalles;
	  	

	  	
	  	var productos = '';
	  	for(var i=0;i<global.user.cotizador.length;i++){
		  	productos+=global.user.cotizador[i].id+',';
	  	}
	  	if(global.user.cotizador.length>0){
		  	productos = productos.substring(0, productos.length - 1); 
	  	}else{
		  	this.msg("Necesita cotizar al menos 1 producto");
		  	return;
	  	}
	  	
	  	params.productos = productos;
	  	params.telefono = this.state.telefono;
	  	
	  	if(params.telefono==""){
		  	this.msg("Ingrese su teléfono");
		  	return;
	  	}
	  	if(params.detalle==""){
		  	this.msg("Detalle es requerido");
		  	return;
	  	}

        try {
	        this.setState({loading:true});
	       
	        var headers = {'Content-Type': 'text/xml;charset=UTF-8','SOAPAction':'"http://tempuri.org/IHPServicios/Cotizador_Registro"'};
	        var body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
			   body += '<soapenv:Header/>';
			   body += '<soapenv:Body>';
			      body += '<tem:Cotizador_Registro>';
			         body += '<tem:int_IdUsuario>'+params.id+'</tem:int_IdUsuario>';
			         body += '<tem:int_idPais>'+params.pais+'</tem:int_idPais>';
			         body += '<tem:int_TipoCliente>'+params.tipocliente+'</tem:int_TipoCliente>';
			         body += '<tem:var_Detalle>'+params.detalle+'</tem:var_Detalle>';
			         body += '<tem:var_Productos>'+params.productos+'</tem:var_Productos>';
			         body += '<tem:var_Telefono>'+params.telefono+'</tem:var_Telefono>';
			      body += '</tem:Cotizador_Registro>';
			   body += '</soapenv:Body>';
			body += '</soapenv:Envelope>';
			
			//return;
			
			body = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
			  body += '<soap:Body>';
			    body += '<Cotizador_Registro xmlns="http://tempuri.org/">';
			      body += '<int_IdUsuario>'+params.id+'</int_IdUsuario>';
			      body += '<int_idPais>'+params.pais+'</int_idPais>';
			      body += '<int_TipoCliente>'+params.tipocliente+'</int_TipoCliente>';
			      body += '<var_Detalle>'+params.detalle+'</var_Detalle>';
			      body += '<var_Productos>'+params.productos+'</var_Productos>';
			      body += '<var_Telefono>'+params.telefono+'</var_Telefono>';
			    body += '</Cotizador_Registro>';
			  body += '</soap:Body>';
			body += '</soap:Envelope>';
			
			/*
			var url = global.api+'/Cotizador_Registro?int_IdUsuario=${encodeURIComponent(params.id)}';
			url += `&int_idPais=${encodeURIComponent(params.pais)}`;
			url += `&int_TipoCliente=${encodeURIComponent(params.tipocliente)}`;
			url += `&var_Detalle=${encodeURIComponent(params.detalle)}`;
			url += `&var_Productos=${encodeURIComponent(params.productos)}`;
			url += `&var_Telefono=${encodeURIComponent(params.telefono)}`;
			*/
			
			var url = global.api+'/Cotizador_Registro?int_IdUsuario='+params.id;
			url += '&int_idPais='+encodeURIComponent(params.pais);
			url += '&int_TipoCliente='+encodeURIComponent(params.tipocliente);
			url += '&var_Detalle='+encodeURIComponent(params.detalle);
			url += '&var_Productos='+encodeURIComponent(params.productos);
			url += '&var_Telefono='+encodeURIComponent(params.telefono);
			
			
			//Reactotron.log(url);
			let response = await fetch(url,{method: "GET",headers:headers});
			let responseText = await response.text();
			
			
			Reactotron.log('-------------');
			Reactotron.log(url);
			Reactotron.log(responseText);
			Reactotron.log('-------------');
			
			parseString(responseText, function (err, result) {
	           	var resp = JSON.parse(result.string._);
	           	
	           	Reactotron.log(resp);
	           	
	           	if(Number(resp[0].exito)!=0){
		           	
		           	global.user.cotizador = new Array();
		           	scope.setState({loading:false,Alert_Visibility:true});
		           	/*Alert.alert( 'Gracias', 'Cotización enviada, en breve nos estaremos comunicando contigo.', [ {text: 'Aceptar', onPress: () => scope.dissmissAlert()}], { cancelable: false } );*/
	           	}
	            /*var rpta = result['s:Envelope']['s:Body'][0].Usuario_RegistroResponse[0].Usuario_RegistroResult;
	            var resp = JSON.parse(rpta);
	            global.user = resp.usuario;
				global.saveUser()
			    scope.setState({loading:false});
			    scope.props.onNext();
			    */
			});

        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
    dissmissAlert(){
	    var {navigation} = this.props;
	    navigation.pop();
    }
	componentWillMount(){
							/*fetch( global.api+'/Pais_Listado')
						        .then(response => response.text())
						        .then((response) => {
							         Reactotron.log(response)
						            parseString(response, function (err, result) {
						                var countries = JSON.parse(result.string._);
						                for(var i=0;i<countries.length;i++){
							                paises.push({id:countries[i].pais.id,value:countries[i].pais.id,name:countries[i].pais.pais});
						                }
						                scope.setState({paises:paises});
						            });
						        }).catch((err) => {
						            Reactotron.log("message : "+err.message)
						        })
						        */
  	}
	onEnviarCotizacion(){
		
		this.registrarDatos();


	}
	removeCotizador(producto){
	  var temp = new Array();
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id != producto.id){
		  	temp.push(global.user.cotizador[i]);
		  }
	  }
	  global.user.cotizador = temp;
	  global.saveUser();
	  this.setState({cotizador:global.user.cotizador});
	}
	onSelect(item,t) {
   		pais = item.value;
   		this.setState({value:t});
  	}
  	onSelect2(item,t) {
	  
	 	tipocliente = item.value;
	 	this.setState({value2:t});
  	}
  	
  	handleKeyDown(e){
	  	Reactotron.log('handleKeyDown');
	  	Reactotron.log(e);
	    if(e.nativeEvent.key == "Enter"){
	        dismissKeyboard();
	    }
	}
	onKeyPress = ({ nativeEvent }: Object) => {
    	Reactotron.log('onKeyPress');
    	if(nativeEvent.key == "Enter"){
	    	Reactotron.log('dismissKeyboard');
	        dismissKeyboard();
	    }
  	}
  	Show_Custom_Alert(visible) {
	 
	    this.setState({Alert_Visibility: visible});
	    
	}
	render() {
		var {navigation} = this.props;
		var optionStyle = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',color:'#575757'};
		var optBorder = {borderBottomWidth:0.3, borderBottomColor:'#e0e1e1'};
		 var {navigation} = this.props;

		var icoSize = 60;
    var boxContainerStyle = {flexWrap:'wrap',width:'100%',backgroundColor:'#ebf9ff', flexDirection:'row'};
    var boxStyle = {flexBasis:'50%',backgroundColor:'white',flexDirection:'row'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
    
    	var productoItemStyle = {marginBottom:5,flexDirection:'row',backgroundColor:'transparent'}; 
    	var labelStyle = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:14,color:'#1259a1',marginRight:10,marginLeft:15};
    	var labelContainer = {flexDirection:'row',marginBottom:15};	
    	var inputContainerInput = {
		width: "100%",fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
		height: 45, marginBottom: 8,borderColor: 'gray',
		borderWidth: 0,backgroundColor:'#F3F3F3',padding:10
	}			

         var loading = (<View/>)     
        if(this.state.loading){
				loading = (<View style={{position:'absolute',left:0,
					right:0,top:0,bottom:0,justifyContent:'center',
				alignItems:'center',backgroundColor:'rgba(0,0,0,0.5)'}}>
				<ActivityIndicator size="large" color="#FFF" />
				</View>);
		}      
		return (<View style={{flex:1,backgroundColor:'white'}} >
		
		
		
		<Modal visible={this.state.Alert_Visibility} transparent={true} animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Alert_Visibility)} } >

            <View style={{ flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor:'rgba(0,0,0,0.5)' }}>
 
 
                <View style={styles.Alert_Main_View}>
 
					<View style={{width:'100%',height:50,backgroundColor:'#0096d6',justifyContent:'center' }}>
                    <Text style={styles.Alert_Title}>¡Gracias!</Text>
					</View>

					
					
					<View style={{ width: '100%', flex:1,backgroundColor:'transparent',justifyContent:'center',alignItems:'center'}}>
                    <Text style={styles.Alert_Message}>
                    
                    Tu cotización ha sido enviada, en breve nos estaremos comunicando contigo. </Text>
                    
<TouchableOpacity onPress={()=>{
						this.setState({Alert_Visibility:false});
						navigation.pop();
						}} style={{marginTop:15,width:120,height:35,borderWidth:0.7,borderColor:'black',justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'ACEPTAR'}</Text>
					
					</TouchableOpacity>
					
					</View>


                

						
                    
					</View>
 
            </View>
 
 
        </Modal>
 

        
        			<Navbar onBack={()=>{navigation.pop();}} onHome={()=>{navigation.navigate('Home')}}/>
        			<KeyboardAwareScrollView>
					<ScrollView style={ {flex: 1,backgroundColor:'white'} }>
        			<View style={{flex:1,backgroundColor:'white',padding:0,height:'100%' }}>
					 	<View style={{flex:1,backgroundColor:'white',padding:20,paddingBottom:0,height:'100%'}}>
					 	
					 	 <View style={{width:'100%',paddingBottom:20,backgroundColor:'transparent' }}>
							 <Text style={{textAlign:'center',color:'#2ea2e5',fontSize:23,fontWeight:'200',
								 fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
							 COTIZADOR</Text>
						 </View>
						 
						 <View style={labelContainer}>
						 	<View><Text style={[labelStyle,{color:'#000',width:75,fontWeight:'bold'}]}>{'Contacto:'}</Text></View>
						 	<View><Text style={[labelStyle,{color:'#000'}]}>{global.user.nombres}</Text></View>
						 </View>
						 
						 <View style={labelContainer}>
						 	<View><Text style={[labelStyle,{color:'#000',width:75,fontWeight:'bold'}]}>{'email:'}</Text></View>
						 	<View><Text style={[labelStyle,{color:'#000'}]}>{global.user.email}</Text></View>
						 </View>
						 
						 <View style={[labelContainer,{marginBottom:10}]}>
						 	<View><Text style={[labelStyle,{color:'#000',width:75,fontWeight:'bold'}]}>{'Producto:'}</Text></View>
						 	
						 	<View style={{width:'80%',flexDirection:'column',
							 	backgroundColor:'white'}}>
						 	
						 	
						 	{this.state.cotizador.map(r => <View style={productoItemStyle}>
						 		<View style={{width:'70%',backgroundColor:'white',margin:0}}>
						 		<Text style={[labelStyle,{color:'#000'}]}>{r.name}</Text>
						 		</View>
						 		
						 		<View style={{width:'30%',backgroundColor:'white'}}>
						 		<TouchableOpacity
						 		onPress={()=>{
							 		this.removeCotizador(r);
						 		}}
						 		style={{width:22,height:22,
							 		justifyContent:'center',alignItems:'center'}}>
						 		
						 		<Material name={'close'} size={16} color="#0096d6"/>
						 		</TouchableOpacity>
						 		</View>
						 		
						 	</View>)}
              
						 	
						 	</View>
						 
						 </View>
						 
					

						
			          <TextInput
			          underlineColorAndroid='transparent'
			          onSubmitEditing={this.doneTelefono}
			          ref={ (c) => this.txtField1 = c }
			          onChangeText={(telefono) => this.setState({telefono:telefono})}
			          placeholder={'Teléfono / Celular'}
			          style={inputContainerInput}   />
			          
			          
			          
					  <TextInput 
					  ref={ (c) => this.txtField2 = c }
					  placeholder={'Detalles / Comentarios'}
					  underlineColorAndroid='transparent' 
					  style={[inputContainerInput,{height:200,flex:1}]} multiline={true}
					  onKeyPress={this.onKeyPress} onChangeText={(detalles) => this.setState({detalles:detalles})}/>
						 
						
						</View>
						 	
						
				 	</View>
				 	</ScrollView>
				 	</KeyboardAwareScrollView>
				 	<BottomBar onCancelar={()=>{ 
								navigation.pop();
							} } 
							onEnviarCotizacion={()=>{ 
								this.onEnviarCotizacion();
						}} />
						{loading}
			</View>)
	}
}
/*<Text style={[labelStyle,{color:'#858585',marginBottom:20}]}>
						 {'* Completa tus datos y te contactaremos para brindarte una rápida atención.'}</Text>*/
export default Cotizador




const styles = StyleSheet.create({

Alert_Main_View:{
 
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor : "#fff", 
  height: 200 ,
  width: '90%',
  borderRadius:0,
 
},
 
Alert_Title:{
 
  fontSize: 24, 
  color: "#fff",
  textAlign: 'center',
  fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
  padding: 0, 
},

Alert_Message:{
 
    fontSize: 16, 
    color: "#444444",
    textAlign: 'center',
    fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
    padding: 10,   
  },

buttonStyle: {
    
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'

},
   
TextStyle:{
    color:'#fff',
    textAlign:'center',
    fontSize: 22,
    marginTop: -5
}
 
});
