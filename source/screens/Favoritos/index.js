import React, { Component } from "react";
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Platform,
  Dimensions,
  ScrollView,
  View,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { FeaturesBar } from "@components";
import { Navbar } from "@components";
import { BottomBar } from "@components";
import { img } from "@media"
import Dash from 'react-native-dash';
import Reactotron from 'reactotron-react-native'
import FastImage from 'react-native-fast-image'

var {height, width} = Dimensions.get('window');
var scope = this;

class Favoritos extends Component {
	constructor(props){
		super(props);
		scope = this;
		this.state = {page:1,data:global.user.favoritos}
		global.updateFavorites = function(){
			scope.setState({data:global.user.favoritos});
		}
		global.trackScreenView("Favoritos",{ tipocliente: global.user.tipocliente}); 
	
	}
	onPressCotizador(r){
	  var {navigation} = this.props;
	  this.addCotizador(r);
	  navigation.navigate('Cotizador');
  }
  addCotizador(producto){
	  var exist = false;
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id == producto.id){
			  exist = true;
		  }
	  }
	  if(!exist){
		  global.user.cotizador.push(producto);
	  }
	  global.saveUser();
  }

	dissLike(item){
		Reactotron.log(" dissLike "+item.id);
		this.removeFavorite(item)
	}
	removeFavorite(producto){
	  var temp = new Array();
	  for(var i=0;i<global.user.favoritos.length;i++){
		  if(global.user.favoritos[i].id != producto.id){
		  	temp.push(global.user.favoritos[i]);
		  }
	  }
	  global.user.favoritos = temp;
	  global.saveUser();
	  this.setState({data:global.user.favoritos});
  	}		
	render() {

		 var {navigation} = this.props;

		var icoSize = 60;
    var boxContainerStyle = {width:'100%',backgroundColor:'white', flexDirection:'column'};
    var boxStyle = {flex:1,backgroundColor:'transparent',flexDirection:'row'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
    
				
    		for(var i=0;i<this.state.data.length;i++){
	    		Reactotron.log(this.state.data[i].imagenes[0]);
    		}			 
		return (<View style={{flex:1}} >
        			<Navbar onBack={()=>{navigation.pop();}}  onHome={()=>{navigation.navigate('Home')}}/>
        			<View style={{flex:1,backgroundColor:'white',padding:0 }}>
        			
        				<View style={{width:'100%',height:140}}>
        					<Image style={{width:'100%',height:140,resizeMode:'cover'}} source={ img.favoritos } />
        					<View style={{position:'absolute',left:0,right:0,top:0,bottom:0,
	        					backgroundColor:'rgba(0,0,0,0.6)',justifyContent:'center'}}>
        						
        						<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        							<Image style={{width:25,height:25,resizeMode:'contain',tintColor:'#FFF'}} source={img.heart} />
        							<Text style={{marginLeft:10,color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:22}}>{'MIS FAVORITOS'}</Text>
        						
        						</View>
        					</View>
						</View>
					 	
					 	<View style={{flex:1,padding:0,paddingBottom:0 }}>
					 	
					 							 
						 
						 <ScrollView style={{flex:1}}>
    				  <View style={boxContainerStyle}>


              {this.state.data.map(r => <View  key={r.id} style={boxStyle}>
                <TouchableOpacity onPress={()=>{
	                
                  navigation.navigate('ItemProfileProduct',{type:r.type,producto:r});
                  
                }} style={{flex:1, alignItems:'center',backgroundColor:'transparent',flexDirection:'column'}}>
               
    				<View style={{flex:1,justifyContent:'center', alignItems:'center',backgroundColor:'transparent',flexDirection:'row'}}>
    				
    						
    				<View style={{width:100,height:100,marginRight:5}}>
	    				<FastImage style={{width:100,height:100,flex:1,marginTop:0}} source={{
						    uri: r.imagenes[0].imagen,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }} resizeMode={FastImage.resizeMode.contain} />
    				</View>		
    				<View style={{width:160,height:100,flexDirection:'column',backgroundColor:'transparent',justifyContent:'center'}}>
    				
    				<Text style={{marginTop:0,fontSize:14, fontWeight:'400',
	                color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.name}</Text>
	                
					<Text style={{marginBottom:0,fontSize:11,
					color:'#868686',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{(r.type == 'impresion')?"Impresión":"Cómputo"}</Text>
					
					<TouchableOpacity onPress={()=>{
						this.onPressCotizador(r);
						}} style={{width:140,height:30,borderWidth:0.7,borderColor:'black',marginTop:5,justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'COTIZAR AHORA'}</Text>
					
					</TouchableOpacity>
					
    				</View>			
    						
                
				
				<TouchableOpacity 
				onPress={()=>this.dissLike(r)}
				style={{ justifyContent:'center',alignItems:'center',width:22,height:22, backgroundColor:'transparent',
					position:'absolute',top:10,right:-20}}>
					<Material name={'close'} size={15} color="#999"/>
				</TouchableOpacity>
				</View>
				<View style={{width:'80%',backgroundColor:'#999',height:0.5}}></View>
				
                </TouchableOpacity>
						
    					</View>)}


    					</View>
    				</ScrollView>
						 

						 
						
						</View>
						 	
						
				 	</View>
			</View>)
	}
}
/*
	<BottomBar onComputo={()=>{ 
								navigation.navigate('ComputoCategories'); }} 
							onImpresion={()=>{ 
								navigation.navigate('ImpresionCategories');
						}} />
						*/
export default Favoritos
