import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Platform,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { img } from "@media"
import { Navbar } from "@components";
import FastImage from 'react-native-fast-image'
var {height, width} = Dimensions.get('window');
import Reactotron from 'reactotron-react-native'
import Dash from 'react-native-dash';
var parseString = require('react-native-xml2js').parseString;

var item_laptop = {image:'https://images.eprice.it/nobrand/0/lightbox/785/204250785/4471531a-f658-487c-887b-881479cf64c8.jpg',
	name:'HP Elite x2',settings:['Lab 1','Lab 2','Lab 3'],mon:'1',so:'WIN',hd:'3.5GB',pr:'Inter 7',men:'4GB'};


var item_print = {image:'https://images.eprice.it/nobrand/0/lightbox/785/204250785/4471531a-f658-487c-887b-881479cf64c8.jpg',
	name:'HP Elite x2 ',settings:['Lab 1','Lab 2','Lab 3'],s1:'X1',s2:'X2',s3:'X3',s4:'X4'};
	
var scope = null;
var productoID1=0;
var productoID2=0;
var productoData = null;
var productoData2 = null;
var currentSector=1;
global.productoSelected=-1;	
class Comparar extends Component {
	
	constructor(props){
		super(props);
		var {type} = this.props.navigation.state.params;
		scope = this;
		global.screenComparar = this;
		//this.state = {item1:item_laptop,item2:item_laptop}
		this.state = {item1:null,item2:null}
		
		global.trackScreenView("Comparar",{ tipocliente: global.user.tipocliente}); 
	}
	onPressCotizador(r){
	  var {navigation} = this.props;
	  this.addCotizador(r);
	  navigation.navigate('Cotizador');
  }
  addCotizador(producto){
	  var exist = false;
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id == producto.id){
			  exist = true;
		  }
	  }
	  if(!exist){
		  global.user.cotizador.push(producto);
	  }
	  global.saveUser();
  }
	setProducto2(){
		//var {producto2} = this.props.navigation.state.params;
		if(global.productoSelected!=-1){
			
			if(currentSector==1){
				productoID1=global.productoSelected;
			}
			if(currentSector==2){
				productoID2=global.productoSelected;
			}
			Reactotron.log("P1 "+productoID1+" P2 "+productoID2+" currentSector"+currentSector);
			
			
			this.fetchData(productoID1,productoID2);
		}
		
	}
	closeBlock1(){
		
	}
	closeBlock2(){
		
	}
	closeItem1(){
		global.productoSelected=-1;	
		
		if(this.state.item2!=null){
			if(productoData2!=null){
				
			var item = {};
		            item.name = productoData2.producto.name;
		            item.imagen = productoData2.producto.imagen;
		            item.icon1 = productoData2.producto.icon1;
		            item.icon2 = productoData2.producto.icon2;
		            item.icon3 = productoData2.producto.icon3;
		            item.especificaciones = new Array();
		            var especificaciones = productoData2.producto.especificaciones;
		            for(var i=0;i<especificaciones.length;i++){
			            especificaciones[i].valor2=especificaciones[i].valor;
			            if(especificaciones[i].valor2!='-'){
				            especificaciones[i].valor = "";
				            item.especificaciones.push(especificaciones[i]);
			            }
		            }
			}
			this.setState({item1:null,item2:item});
		}else{
			productoID1=0;
			this.setState({item1:null});
		}
	}
	closeItem2(){
		global.productoSelected=-1;	
		
		if(this.state.item1!=null){
			if(productoData!=null){
			var item = {};
		            item.name = productoData.producto.name;
		            item.imagen = productoData.producto.imagen;
		            item.icon1 = productoData.producto.icon1;
		            item.icon2 = productoData.producto.icon2;
		            item.icon3 = productoData.producto.icon3;
		            item.especificaciones = new Array();
		            var especificaciones = productoData.producto.especificaciones;
		            for(var i=0;i<especificaciones.length;i++){
			            if(especificaciones[i].valor!='-'){
				            especificaciones[i].valor2 = "";
				            item.especificaciones.push(especificaciones[i]);
			            }
		            }
			}					
			this.setState({item1:item,item2:null});
		}else{
			productoID2=0;
			this.setState({item2:null});
		}
		
	}
	componentWillMount(){
		
		var {producto1} = this.props.navigation.state.params;
		//var producto1;
		Reactotron.log("producto1 "+producto1);
		if(producto1!=undefined){
			productoID1 = producto1.id;
			this.fetchData(productoID1,0);
		}else{
			productoID1 = 0;
			this.fetchData(productoID1,0);
		}
	}
	async fetchData (productoID1,productoID2) {
	   
       
        try { 

			var url =  global.api+'/Producto_Comparador';
			url += '?int_IdProducto1='+productoID1+'&int_IdProducto2='+productoID2;

			let response = await fetch(url);
			let responseText = await response.text();
			var respuesta = new Array();
			
            parseString(responseText, function (err, result) {
	            var resp = JSON.parse(result.string._);
	            
				Reactotron.log(resp[0].productos); 
				
	            if(resp[0].productos.length == 1){
		            
		            var pro = resp[0].productos[0];
		            productoData = pro;
		            var item = {};
		            item.name = pro.producto.name;
		            item.imagen = pro.producto.imagen;
		            item.icon1 = pro.producto.icon1;
		            item.icon2 = pro.producto.icon2;
		            item.icon3 = pro.producto.icon3;
		            item.especificaciones = new Array();
		            var especificaciones = pro.producto.especificaciones;
		            for(var i=0;i<especificaciones.length;i++){
			            if(especificaciones[i].valor!=''){
				            item.especificaciones.push(especificaciones[i]);
			            }
		            }
					if(Number(pro.producto.id)==Number(productoID1)){
						scope.setState({item1:item});
					}
		            if(Number(pro.producto.id)==Number(productoID2)){
						scope.setState({item2:item});
					}
	            }
	            if(resp[0].productos.length == 2){
		            
		             for(var i=0;i<resp[0].productos.length;i++){
			             if(Number(productoID1)==Number(resp[0].productos[i].producto.id)){
				             productoData = resp[0].productos[i];
				             respuesta.push(resp[0].productos[i]);
				             break;
			             }
		             }
		            for(var i=0;i<resp[0].productos.length;i++){
			             if(Number(productoID2)==Number(resp[0].productos[i].producto.id)){
				             productoData2 = resp[0].productos[i];
				             respuesta.push(resp[0].productos[i]);
				             break;
			             }
		             }		            
	            }
			});
			
			
			if(respuesta.length==2){
		           var item1 = {};
		            var pro = respuesta[0];
		            productoData = pro;
		            Reactotron.log(pro);
		            item1.name = pro.producto.name;
		            item1.imagen = pro.producto.imagen;
		            item1.icon1 = pro.producto.icon1;
		            item1.icon2 = pro.producto.icon2;
		            item1.icon3 = pro.producto.icon3;
		            item1.especificaciones = new Array();
		            var especificaciones = pro.producto.especificaciones;
		            for(var i=0;i<especificaciones.length;i++){
			            if(especificaciones[i].valor==''){
				           especificaciones[i].valor = '-';
			            }
				            especificaciones[i].valor2 = '-';
				            item1.especificaciones.push(especificaciones[i]);
			            //}
		            }
		            scope.setState({item1:item1});
		            
		            
		            
		            
		            var item2 = {};
		            var pro = respuesta[1];
		            productoData2 = pro;
		            
		            Reactotron.log("productoData2");
		            Reactotron.log(pro);
		            item2.id = pro.producto.id;
		            item2.name = pro.producto.name;
		            item2.imagen = pro.producto.imagen;
		            item2.icon1 = pro.producto.icon1;
		            item2.icon2 = pro.producto.icon2;
		            item2.icon3 = pro.producto.icon3;
					var especificaciones = pro.producto.especificaciones;
		            for(var i=0;i<especificaciones.length;i++){
			            if(especificaciones[i].valor==''){
				           especificaciones[i].valor = '-';
			            }
				            item1.especificaciones[i].valor2 = especificaciones[i].valor;
		            }
		           
		            var eliminar = new Array();
		            for(var i=0;i<item1.especificaciones.length;i++){
			            if(item1.especificaciones[i].valor=='-' && item1.especificaciones[i].valor2=='-'){
				            item1.especificaciones.splice(i, 1);
				            i--;
				        }
		            }

		            scope.setState({item2:item2});
	           }
	           
	           
	           
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
	render() {

	var {type} = this.props.navigation.state.params;
	//type="computo";
	
    var {navigation} = this.props;
	var icoSize = 60;
    var boxContainerStyle = {flex:1,paddingLeft:20,paddingRight:20};
    var boxStyle = {flexBasis:'50%',backgroundColor:'white',flexDirection:'row'};

    	var iconos = (<View/>);

	    	var especificaciones = new Array();
	    	if(this.state.item1!=null){	
		    	especificaciones = this.state.item1.especificaciones;
		    }else if(this.state.item2!=null){	
		    	especificaciones = this.state.item2.especificaciones;
		    }
		    	if(especificaciones.length>0){	

		    	iconos = (<View style={{flex:1,backgroundColor:'transparent',paddingLeft:15,paddingRight:30}}>
                    {especificaciones.map(r => 
					        <View style={{flexDirection:'row',padding:3, backgroundColor:'transparent',alignItems:'center'}}>
                        
                        
                        <Text style={{flex:1,textAlign:'center',color:'#000',fontSize:15,fontWeight:'200',
	                    fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.valor}</Text>
                        
                        
                        <FastImage
	                	  style={{width:30,height:30,flex:1,backgroundColor:'transparent' }}
						  source={{
						    uri: r.especificacion,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }}
						  resizeMode={FastImage.resizeMode.contain}
						/>
						
						<Text style={{flex:1,textAlign:'center',color:'#000',fontSize:14,fontWeight:'200',
	                    fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',backgroundColor:'transparent' }}>{r.valor2}</Text>
                        
                        
                    </View>)}
				</View>)
				
				}
	    	
	    	
    	
    	var melements1 = (<View/>)
    	if(this.state.item1!=null){
	    	melements1 = (<View style={{paddingBottom:10}}>
	    				<View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                        <FastImage
	                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent' }}
						  source={{
						    uri: this.state.item1.icon1,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }}
						  resizeMode={FastImage.resizeMode.contain}
						/>
                        
                        
                      </View>
                      <View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <FastImage
                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent'}}
					  source={{
					    uri: this.state.item1.icon2,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
                      </View>
                      <View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <FastImage
                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent'}}
					  source={{
					    uri: this.state.item1.icon3,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
                     </View></View>)
    	}
    	var melements2 = (<View/>)
    	if(this.state.item2!=null){
	    	melements2 = (<View style={{paddingBottom:10}}>
	    				<View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>

                        <FastImage
	                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent' }}
						  source={{
						    uri: this.state.item2.icon1,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }}
						  resizeMode={FastImage.resizeMode.contain}
						/>
                        
                        
                      </View>
                      <View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <FastImage
                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent'}}
					  source={{
					    uri: this.state.item2.icon2,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
                      </View>
                      <View style={{marginTop:5,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <FastImage
                	  style={{width:'100%',height:30,flex:1,backgroundColor:'transparent'}}
					  source={{
					    uri: this.state.item2.icon3,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
                     </View></View>)
    	}
		
		
		var cotizar1 = (<View/>)
		var cotizar2 = (<View/>)
		
		var product1 = (<TouchableOpacity  onPress={()=>{  
									currentSector=1;
			                      this.props.navigation.navigate("CompararBusqueda",{sector:1,type:type}); 
			                      }} style={{flex:1,backgroundColor:'#FFF'}}>
			                      
			                      <View style={{justifyContent:'center',alignItems:'center',backgroundColor:'#f0f0f0',width:'100%', height:80 }} >
		<Image source={img.plus} style={{width:40,height:40,tintColor:'#0096d6'}}></Image>
								</View>
			<View style={{backgroundColor:'#0096d6',width:'100%', height:30,marginTop:7,justifyContent:'center',alignItems:'center'}} >
				<Text style={{marginBottom:0,fontSize:13,fontWeight:'400',
					color:'#fff',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Seleccione equipo 01'}</Text>
			</View>
		</TouchableOpacity>)
		
		var product2 = (<TouchableOpacity  onPress={()=>{  
									currentSector=2;
			                      this.props.navigation.navigate("CompararBusqueda",{sector:2,type:type}); 
			                      }} style={{flex:1,backgroundColor:'#FFF'}}>
			                      
			                      <View style={{justifyContent:'center',alignItems:'center',backgroundColor:'#f0f0f0',width:'100%', height:80 }} >
		<Image source={img.plus} style={{width:40,height:40,tintColor:'#0096d6'}}></Image>
								</View>
			<View style={{backgroundColor:'#0096d6',width:'100%', height:30,marginTop:7,justifyContent:'center',alignItems:'center'}} >
				<Text style={{marginBottom:0,fontSize:13,fontWeight:'400',
					color:'#fff',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Seleccione equipo 02'}</Text>
			</View>
		</TouchableOpacity>)
		
			
		
		var closeStyle = {alignItems:'center',justifyContent:'center',width:15, height:15,
	                    backgroundColor:'transparent',position:'absolute',right:0,top:0};
		
		if(this.state.item1!=null){
			cotizar1 = (<TouchableOpacity onPress={()=>{
						this.onPressCotizador(this.state.item1);
						}} style={{width:120,height:35,borderWidth:0.7,borderColor:'black',marginTop:5,justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'COTIZAR AHORA'}</Text>
					
					</TouchableOpacity>);
					
			product1 = (<View style={{flex:1,backgroundColor:'white'}}>

	                      <FastImage
		                	  style={{flex:1,width:null,height:null}}
							  source={{
							    uri: this.state.item1.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
					
							<TouchableOpacity onPress={()=>{  this.closeItem1(); }} style={closeStyle}>
	                      <MaterialIcons name={'close'} size={17} color="#999"/>
	                    </TouchableOpacity>
	                    
	                    <View style={{height:40,backgroundColor:'transparent',flexDirection:'row',
		                    borderTopWidth:1,borderTopColor:'#c3c3c3',borderBottomWidth:1,borderBottomColor:'#c3c3c3'}}>
	                      <View style={{flex:1,marginLeft:5,marginTop:0,justifyContent:'center'}}>
	                        <Text style={{color:'#0096d6',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item1.name}</Text>
	                      </View>
	                      
	                    </View></View>);
		}
		
		
		if(this.state.item2!=null){
			cotizar2 = (<TouchableOpacity onPress={()=>{
				Reactotron.log(this.state.item2);
						this.onPressCotizador(this.state.item2);
						}} style={{width:120,height:35,borderWidth:0.7,borderColor:'black',marginTop:5,justifyContent:'center',alignItems:'center'}}>
						
						<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',
					color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'COTIZAR AHORA'}</Text>
					
					</TouchableOpacity>);
			product2 = (<View style={{flex:1,backgroundColor:'white'}}>
		                    <FastImage
		                	  style={{flex:1,width:null,height:null}}
							  source={{
							    uri: this.state.item2.imagen,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
		                    
		                    <TouchableOpacity onPress={()=>{  this.closeItem2(); }} style={closeStyle}>
		                      <MaterialIcons name={'close'} size={17} color="#999"/>
		                    </TouchableOpacity>
							<View style={{height:40,backgroundColor:'transparent',flexDirection:'row',
		                    borderTopWidth:1,borderTopColor:'#c3c3c3',borderBottomWidth:1,borderBottomColor:'#c3c3c3'}}>
	                      <View style={{flex:1,marginLeft:5,marginTop:0,justifyContent:'center'}}>
	                        <Text style={{color:'#0096d6',
		                        fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{this.state.item2.name}</Text>
	                      </View>
	                      
	                    </View></View>);
		}
		var mensaje_ayuda = (<View style={{backgroundColor:'white',width:width,paddingTop:0,paddingBottom:10,alignItems:'center'}}></View>);
		if(this.state.item1==null && this.state.item2==null){
			mensaje_ayuda = (<View style={{backgroundColor:'white',width:width,paddingTop:16,paddingBottom:10,alignItems:'center'}}>
		              
		              <Text style={{fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',color:'black', 
			              fontWeight:'200'}}>{'Elige el equipo a comparar haciendo clic en el ícono'}</Text>
		              
		            </View>);
		}
		
		
	                    
		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}} >
				 	<Navbar onBack={()=>{navigation.pop()}} showWinPro={(type=="computo")?true:false} onHome={()=>{navigation.navigate('Home')}}/>
				 	
				 	
				 	<View style={{width:'100%',height:140}}>
        					<Image style={{width:'100%',height:140,resizeMode:'cover'}} source={ img.comparar_bg } />
        					<View style={{position:'absolute',left:0,right:0,top:0,bottom:0,
	        					backgroundColor:'rgba(0,0,0,0.5)',justifyContent:'center'}}>
        						
        						<View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        							<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'#FFF'}} source={img.ico_comparar} />
        							<View style={{marginLeft:5,flexDirection:'column',backgroundColor:'transparent'}}>
        							<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
	        							fontSize:24,lineHeight:24}}>{'COMPARADOR'}</Text>
        							
        						<Text style={{textAlign:'center',color:'#fff',fontSize:17,lineHeight:16,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{(type=="computo")?'equipos de cómputo':'equipos de impresión'}</Text>
        							</View>
        						</View>
        					</View>
						</View>
						
						
						
		            <ScrollView style={{flex:1}}>
		            
		            
						
						
		              
		            
		            {mensaje_ayuda}
		            
		    		<View style={boxContainerStyle}>
		                  <View style={{height:170, flexDirection:'row'}}>
		                    <View style={{flex:1,backgroundColor:'white'}}>
			                    {product1}
							</View>
		                    <View style={{flex:1,marginLeft:10,backgroundColor:'white'}}>
				                {product2}
		                    </View>
		                  </View>
		                  <View style={{paddingTop:0, paddingBottom:10,flexDirection:'row',justifyContent:'center'}}>
								</View>
				                  
								  	{iconos}
								  	
								  	
								  	<View style={{paddingTop:0, paddingBottom:10,flexDirection:'row'}}>
								  	<View style={{flex:1,backgroundColor:'white',paddingTop:5,alignItems:'center',marginRight:60}}>
								  		{cotizar1}
								  	</View>
								  	
								  	<View style={{flex:1,backgroundColor:'white',paddingLeft:0,paddingTop:5,alignItems:'center',marginRight:10}}>
								  		{cotizar2}
								  	</View>
								  	</View>
				                  
		    				</View>
						</ScrollView>
					</View>)
	}
}

export default Comparar
