import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  Linking,
  View,
} from 'react-native';

import { connect } from "react-redux";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navbar } from "@components";
import Pdf from 'react-native-pdf';

class PDFVista extends Component {
	
	constructor(props){
		super(props);
		global.trackScreenView("PDFVista",{ tipocliente: global.user.tipocliente }); 
	}
	render(){
		var {pdf} = this.props.navigation.state.params;
		const source = {uri:pdf,cache:true};
        return (
            <View style={styles.container}>
            	<Navbar onBack={()=>{this.props.navigation.pop()}} showWinPro={false}/>	
                <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    style={styles.pdf}/>
            </View>
        )
	}
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 0,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});


export default PDFVista
