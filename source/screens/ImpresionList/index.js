import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { img } from "@media"
import { Navbar } from "@components";
var {height, width} = Dimensions.get('window');
import Dash from 'react-native-dash';
import { BottomBar } from "@components"

class ImpresionList extends Component {
	constructor(props){
		super(props);
		global.trackScreenView("ImpresionList",{ tipocliente: global.user.tipocliente}); 
	}
	render() {

    var {navigation} = this.props;

		var icoSize = 60;
    var boxContainerStyle = {flexWrap:'wrap',width:width,backgroundColor:'#ebf9ff', flexDirection:'row'};
    var boxStyle = {flexBasis:'50%',backgroundColor:'white',flexDirection:'row'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:'HP Simplified'};
    var laptops = new Array();
		laptops.push({id:1,label:"Serie 900",item_img:img.imp1});
    laptops.push({id:2,label:"Serie 900",item_img:img.imp2});
    laptops.push({id:3,label:"Serie 900",item_img:img.imp3});

    laptops.push({id:4,label:"Serie 800",item_img:img.imp1});
    laptops.push({id:5,label:"Serie 800",item_img:img.imp2});
    laptops.push({id:6,label:"Serie 800",item_img:img.imp3});

    laptops.push({id:4,label:"Serie 700",item_img:img.imp1});
    laptops.push({id:5,label:"Serie 700",item_img:img.imp2});
    laptops.push({id:6,label:"Serie 700",item_img:img.imp3});

    laptops.push({id:4,label:"Serie 700",item_img:img.imp1});
    laptops.push({id:5,label:"Serie 700",item_img:img.imp2});
    laptops.push({id:6,label:"Serie 700",item_img:img.imp3});


		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}} >

				 	<Navbar onBack={()=>{navigation.pop()}}/>
				 	<View style={{flex:1,alignItems:'center',justifyContent:'center'}} >

					 	<Image style={{width:width,height:150,resizeMode:'cover'}} source={ img.topImpresion } />

            <View style={{backgroundColor:'white',width:width,paddingTop:16,paddingBottom:16}}>
            <Text style={{textAlign:'center',color:'#1259a1',fontSize:22,fontFamily:'HP Simplified',fontWeight: 'bold'}}>ScanJet Enterprise</Text>
            <Text style={{textAlign:'center',color:'#1259a1',fontSize:16,fontFamily:'HP Simplified'}}>Impresión</Text>

            </View>

            <ScrollView style={{flex:1}}>
    				  <View style={boxContainerStyle}>


              {laptops.map(r => <View  key={r.id} style={boxStyle}>
                <TouchableOpacity onPress={()=>{
                  navigation.navigate('ItemProfile',{type:"impresion"});
                }} style={{flex:1,justifyContent:'center', alignItems:'center',}}>
                <Dash style={{width:width/2, height:1}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={4}/>
    						<Image style={{width:width/2,height:80,flex:1,resizeMode:'contain',marginTop:15}} source={ r.item_img } />
                <Text style={{marginTop:10,marginBottom:15,flex:1,height:15,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:'HP Simplified'}}>{r.label}</Text>

                </TouchableOpacity>
                <Dash style={{flexDirection:'column',width:1, height:130}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={4}/>

    					</View>)}


    					</View>
    				</ScrollView>
            <BottomBar onComparar={()=>{
                navigation.navigate('Comparar',{type:'impresion'});
            }} onSearch={()=>{
				navigation.navigate('Buscar',{type:'impresion'});
            }}/>
				 	</View>

			</View>)
	}
}

export default ImpresionList
