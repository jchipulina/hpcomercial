import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  ScrollView,
  Image,
  Linking,
  Modal,
  View,
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import Reactotron from 'reactotron-react-native'
import Communications from 'react-native-communications';
import { Navbar } from "@components";
import YouTube from 'react-native-youtube'
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/FontAwesome';
var parseString = require('react-native-xml2js').parseString;

var scope;
require('html2json').html2json;

/*		
	
	
	
	<View style={{backgroundColor:'black',width:'100%',height:200}}>
						 	{ (this.state.player==false) ? ( 
							 	
							 	<TouchableOpacity onPress={()=>{ scope.setState({player:true}); }} style={{backgroundColor:'black',height:200}}>
						 		<Image style={{width:'100%',height:200,resizeMode:'cover'}} source={ img.planetparnerts_video } />
						 	</TouchableOpacity>
							 	
							 	):(
							 	
							 	<Video source={{uri: "http://140.82.28.69/files/planta.mp4"}}   // Can be a URL or a local file.
						       ref={(ref) => {
						         this.player = ref
						       }}                                   // Store reference
						       paused={false}
						       onEnd={()=>{
							       scope.setState({player:false})
						       }} 
						       onBuffer={this.onBuffer}                // Callback when remote video is buffering
						       onError={this.videoError}               // Callback when video cannot be loaded
						       style={{width:'100%',height:200}} />
							 	
							 	
							 	)
							 	
							 	
						 	}
						 	</View>
	
	
 */

class PlanetPartners extends Component {
	constructor(props){
		super(props);
		this.state = {data:null}
		scope = this;
		global.PlanetPartnersRef = this;
		
		this.state = {currentTab:0,Alert_Visibility:false,centrosAcopio:[],key:0,player:false,checkBox1:true}
		
		global.trackScreenView("HP Planet Partner",{ tipocliente: global.user.tipocliente}); 
		
	}
	update(){
		this.setState({currentTab:0,key:Math.random()});
	}
	Show_Custom_Alert(visible) {
	 
	    this.setState({Alert_Visibility: visible});
	    
	}
	componentDidMount(){

		if(this.props.navigation.state.params==undefined){
			this.fetchDataPartners();
			return;
		}
		let {data} = this.props.navigation.state.params;
		//Reactotron.log("this.state.data 1");
		
		//Reactotron.log("-------------*");
		Reactotron.log(this.props.navigation.state.params.data);
		this.setState({data:this.props.navigation.state.params.data});
		setInterval(()=>{
			//Reactotron.log(this.state.data);
			//Reactotron.log("-------------**");
			this.checkCentros();
		}, 500)
		
			
	}
	checkCentros(){
		//Reactotron.log("this.state.data 2");
		//Reactotron.log(this.state.data);
		if(this.state.data!=null){
			if(this.state.data.centros!=undefined){
				var centros = html2json(this.state.data.centros[0].valor);
				var centros_a = new Array();
				for(var i=0;i<centros.child[0].child.length;i++){
					centros_a.push(centros.child[0].child[i].child[0].text);
				}
				this.setState({centrosAcopio:centros_a})
			}
		}
		
	}
	async fetchDataPartners(){
	Reactotron.log("fetchDataPartners");
			try { 
				//242 costa
				//243 Puerto rico
				var url = global.api+"/PlanetPartner_Detalle?int_IdUsuario="+global.user.id;
				let response = await fetch(url);
				let responseText = await response.text();
				laptops = new Array();
	            parseString(responseText, function (err, result) {
		            if(result.string._!=undefined){
			            var resp = JSON.parse(result.string._);
			            partnersData = resp[0].programa;
			            scope.setState({data:partnersData});
			            scope.checkCentros();	
		            }
		           
				});
	        } catch (err) {
	            Reactotron.log("ERROR "+err);
	        }


	}
	render() {
		
		
		var icoSize = 60;
		var {navigation} = this.props;
		var txtbase = {color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15}
		
		let data = this.state.data;
		
		var styleContainer = {flex:1,width:'100%',padding:30,paddingTop:20,backgroundColor:'white' };
		var mediumText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:17,lineHeight:18,fontWeight:Platform.OS === 'ios' ? '300' : ''}
		
		var smallText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15,fontWeight:Platform.OS === 'ios' ? '300' : ''}
		
		
		var disclaimerText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:11,lineHeight:14,color:'#666',fontWeight:Platform.OS === 'ios' ? '200' : ''};
		var disclaimer = {paddingTop:20};  
		var titleText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',marginTop:10,marginBottom:10,color:'black'}
		var fontText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		
		
		var strongText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
			fontSize:16,fontWeight:Platform.OS === 'ios' ? '400' : '',marginTop:10,marginBottom:0,color:'#000'}
		
		var checkBox ={width:25,height:25,backgroundColor:'#fff',borderWidth:1,borderColor:'#999', flex:0, alignItems:"center", justifyContent:"center"}
		var checkBoxLabel = {marginTop:0,color:'#000',fontSize:14,fontWeight:'200',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',backgroundColor:'transparent'}
			
		return (<View style={{flex:1,backgroundColor:'#fff'}} >
		
		{this.state.data!=null &&


        <Modal visible={this.state.Alert_Visibility} transparent={true} animationType={"fade"}
          onRequestClose={ () => { this.Show_Custom_Alert(!this.state.Alert_Visibility)} } >

            <View style={{ flex:1, alignItems: 'center', justifyContent: 'center', backgroundColor:'rgba(0,0,0,0.5)' }}>
 
 
                <View style={styles.Alert_Main_View}>
                
                	<TouchableOpacity onPress={()=>{
	                	this.setState({Alert_Visibility:false});
                	}} style={{width:'100%',height:30,flexDirection:'row-reverse'}}>
                		<View style={{width:30,height:30,backgroundColor:'transparent',marginRight:7,marginTop:7}}>
                			<EvilIcons name={'close'} size={30} color="#666"/>
                		</View>
					</TouchableOpacity>
					
					<View style={{width:'100%',backgroundColor:'transaparent',justifyContent:'center',marginTop:0}}>
                    	<Text style={styles.Alert_Title}>{'Centro de Acopio CI\nReciclables'}</Text>
					</View>
					<ScrollView style={{width:'100%',height:250,marginTop:10,marginBottom:20}}>
						<View style={{ margin:20,marginTop:10 }}>
	
						   { this.state.centrosAcopio.map((item, index) => {
						       return (
						         <Text key={index} style={styles.Alert_Message}> {item} </Text> 
						       )
						     })
						   }
	
						</View>
                    </ScrollView>
					</View>
 
            </View>
 
 
        </Modal>
				}
		

				 	<Navbar onBack={()=>{navigation.pop()}} onHome={()=>{navigation.navigate('Home')}}/>
				 	<View style={{flex:1}} >
				 	
				 	
				 	{	Platform.OS === 'ios' ? (
					 	
					 	<YouTube
				 		  apiKey="AIzaSyDZeEQAaS87IEmfC-12YsWiOt5wTZsd0OY"
						  videoId="Wi2OaO1HEi8"   // The YouTube video ID
						  play={false}             // control playback of video with true/false
						  fullscreen={true}       // control whether the video should play in fullscreen or inline
						  showFullscreenButton={true}
						  loop={true}             // control whether the video should loop when ended
						  controls={1}
						  resumePlayAndroid={false}
						  onReady={e => this.setState({ isReady: true })}
						  onChangeState={e => this.setState({ status: e.state })}
						  onChangeQuality={e => this.setState({ quality: e.quality })}
						  onError={e => this.setState({ error: e.error })}
						
						  style={{ alignSelf: 'stretch', height: 150 }}
						/>
						

					 	
					 	
					 	):(
						 							
						<TouchableOpacity onPress={()=>{  navigation.navigate('VideoVista'); }} style={{backgroundColor:'black',height:150}}>
						 		<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.planetparnerts_video } />
						 	</TouchableOpacity>
						 	
						 	
       
       
					 	)
					 	
				 	}
				 	
				 	
					 		
					 	
					 	

            <View style={{backgroundColor:'#bf951e',width:'100%',paddingTop:20,paddingBottom:15}}>
            
            <Text style={{textAlign:'center',color:'white',fontSize:22,lineHeight:20,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'HP PLANET PARTNERS'}</Text>
            
            <Text style={{textAlign:'center',color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15}}>{'Reciclando juntos de manera responsable'}</Text>
 
            </View>
            {this.state.data!=null &&
            <ScrollView style={{flex:1}}>
            
            <View style={{flex:1,alignItems:'center'}}>
	            <View style={{width:'80%',height:45,marginTop:20,backgroundColor:'white',flexDirection:'row'}}>
	            	<TouchableOpacity onPress={()=>{
		            	this.setState({currentTab:0});
	            	}} style={{flex:1,backgroundColor:(this.state.currentTab==0)?'#bf951e':'#e1e4e5',justifyContent:'center'}}>
	            		<Text style={[txtbase,{marginLeft:15,color:(this.state.currentTab==0)?'#fff':'#000'}]}>{'HP Planet\nPartners'}</Text>
	            	</TouchableOpacity>
	            	<TouchableOpacity onPress={()=>{
		            	this.setState({currentTab:1});
	            	}} style={{flex:1,backgroundColor:(this.state.currentTab==1)?'#bf951e':'#e1e4e5',justifyContent:'center'}}>
	            		<Text style={[txtbase,{marginLeft:15,color:(this.state.currentTab==1)?'#fff':'#000'}]}>{'Participa\ndel programa'}</Text>
	            	</TouchableOpacity>
	            </View>
            </View>
            
            {(this.state.currentTab==0) ? (
		        <View style={styleContainer} key={this.state.key}>
            
            	<Text style={[smallText,{color:'#000'}]}>
						{'Recopilamos productos usados para reventa y reciclaje en 74 países y territorios de todo el mundo.'}
						</Text>
						
						<Image style={{width:'100%',height:180,resizeMode:'contain',marginTop:5}} source={ img.mundo } />
						
						<Text style={[smallText,{color:'#000'}]}>
						{'Estamos comprometidos a ayudar a nuestros clientes a reciclar de manera responsable.'}
						</Text>
				
				<View style={{flexDirection:'row',marginTop:20,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0, backgroundColor:'white'}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c1_p } />
						</View>
						

						
					</View>
					
					<View style={{flex:1}}>
						<Text style={[smallText,{color:'#000'}]}>
						{'Nuestro objetivo de reciclaje de productos es reciclar 1.2 millones de toneladas de hardware y suministros para 2025 desde principios de 2016, y hemos reciclado 271,400 toneladas hasta 2017.'}
						</Text>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:20,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c2_p } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[smallText,{color:'#000'}]}>
						{'Más del 80% de nuestros cartuchos de tinta y el 100% de los cartuchos de tóner HP LaserJet son ahora fabricados con plástico reciclado de “circuito cerrado”, 1 y hemos utilizado más de medio millón de libras de plástico marino para hacer cartuchos de tinta HP originales.'}
						</Text>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:20,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c3_p } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[smallText,{color:'#000'}]}>
						{'Nuestros programas de remanufactura dan al hardware de TI, como impresoras, sistemas personales y monitores, una nueva vida, reduciendo los'}
						</Text>
					</View>
				</View>
            
	            <View style={disclaimer}>
					<Text style={disclaimerText}>
				{'1. 80% of Original HP ink cartridges contain between 45-70% recycled content. 100% of Original HP toner cartridges contain between 5-38% post-consumer or post-industrial recycled content. Does not include toner bottles. See www.hp.com/go/recycledcontent for list.'}
					</Text>
				 </View>
		
			</View>
		      ) : (
		        <View style={styleContainer}>
		        
		        		<View style={{flex:1,width:'100%',backgroundColor:'transparent'}}>
		        			
		        			  
		        			{ (global.user.pais=="Colombia") && 
			        			<View style={{width:'100%',flexDirection:'row',backgroundColor:'transparent',justifyContent:'center',alignItems:'center'}}>
									<View style={{flexDirection:'row',backgroundColor:'transparent',flex:1,alignItems:'center'}}>
					        			<TouchableOpacity style={[checkBox,{marginRight:10}]} onPress={ () => { 
													 if(this.state.checkBox1){
														  this.setState({checkBox1:false})
													 }else{
														  this.setState({checkBox1:true})
													 }
										}}>
												 	{this.state.checkBox1 ? (
												        <Icon name={"check"} color="#0096d6" size={15}></Icon>
												      ) : (
												        <View></View>
												      )}
										</TouchableOpacity>
										<Text style={[titleText,{color:'#000',marginBottom:0,marginTop:0,fontSize:14,flex:1}]}>Recicla tus tintas y tóner HP</Text>
									</View>
									<View style={{flexDirection:'row',backgroundColor:'transparent',flex:1,alignItems:'center'}}>
										<TouchableOpacity style={[checkBox,{marginRight:10}]} onPress={ () => { 
													 if(this.state.checkBox1){
														  this.setState({checkBox1:false})
													 }else{
														  this.setState({checkBox1:true})
													 }
										}}>
												 	{!this.state.checkBox1 ? (
												        <Icon name={"check"} color="#0096d6" size={15}></Icon>
												      ) : (
												        <View></View>
												      )}
										</TouchableOpacity>
										<Text style={[titleText,{color:'#000',marginBottom:0,marginTop:0,fontSize:14,flex:1}]}>Recicla tu PC, impresora y periféricos</Text>
									</View>
								</View>
							}
							
							
						</View>

				{ (this.state.checkBox1) ? (
					<View>
					
					<View style={{flexDirection:'row',marginTop:20,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c1_p2 } />
						</View>
						<View style={{flex:1,marginRight:20,marginLeft:0, backgroundColor:'white',alignItems:'center'}}>
							<View style={{width:2,flex:1,backgroundColor:'#bf951e'}}></View>
						</View>
					</View>
					
					<View style={{flex:1}}>
					<Text style={[strongText,{color:'#000',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Requisito:</Text>
						<Text style={[smallText,{color:'#000',marginTop:5}]}>
						{'Tener como mínimo 5 unidades de suministros Originales HP y/o Samsung'}
						</Text>
						<View style={{width:'100%',height:20,backgroundColor:'white'}}></View>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:0,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c2_p2 } />
						</View>
						<View style={{flex:1,marginRight:20,marginLeft:0, backgroundColor:'white',alignItems:'center'}}>
							<View style={{width:2,flex:1,backgroundColor:'#bf951e'}}></View>
						</View>
					</View>
					
					<View style={{flex:1}}>
					<Text style={[strongText,{color:'#000',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Beneficios:</Text>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'No genera costo adicional para el usuario'}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'HP realiza el aprovechamiento de materiales y garantiza una buena disposición final de los productos'}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Se realiza entrega de certificados de participación en el Programa HP Planet Partners a través de una empresa con licencia ambiental (CI Recyclables)'}
						</Text>
						<View style={{width:'100%',height:20,backgroundColor:'white'}}></View>
						
						
					</View>
				</View>
					</View>
					
					):(
						<View>
						
						
				
				

				<View style={{flexDirection:'row',marginTop:20,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c1_p2 } />
						</View>
						<View style={{flex:1,marginRight:20,marginLeft:0, backgroundColor:'white',alignItems:'center'}}>
							<View style={{width:2,flex:1,backgroundColor:'#bf951e'}}></View>
						</View>
					</View>
					
					<View style={{flex:1}}>
					<Text style={[strongText,{color:'#000',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Requisitos:</Text>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Cualquier computador o impresora HP desde una unidad'}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Cualquier computador, impresoras y periféricos (500 kg) '}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Periféricos : Mouse, teclados , audífonos y otros elementos.'}
						</Text>
						<View style={{width:'100%',height:20,backgroundColor:'white'}}></View>
						
					</View>
				</View>

				<View style={{flexDirection:'row',marginTop:0,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c2_p2 } />
						</View>
						<View style={{flex:1,marginRight:20,marginLeft:0, backgroundColor:'white',alignItems:'center'}}>
							<View style={{width:2,flex:1,backgroundColor:'#bf951e'}}></View>
						</View>
					</View>
					
					<View style={{flex:1}}>
					<Text style={[strongText,{color:'#000',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Beneficios:</Text>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Recogemos a domicilio y sin costo.'}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Entrega de Certificado de tratamiento y/o disposición final con una empresa que tiene licencia ambiental vigente.'}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'También se reciben en el centro de acopio de CI Recyclables. (En Bogotá y Cartagena)'}
						</Text>
						<View style={{width:'100%',height:0.5,backgroundColor:'#ccc',marginTop:10}}></View>
						<Text style={[smallText,{color:'#000',marginTop:12}]}>
						{'Sin costos adicionales a la empresa.'}
						</Text>
						<View style={{width:'100%',height:20,backgroundColor:'white'}}></View>
						
					</View>
				</View>
				
				</View>
					)
					
				}
				
				
				
				<View style={{flexDirection:'row',marginTop:0,backgroundColor:'white'}}>
					<View style={{width:75,justifyContent:'flex-start',paddingTop:0}}>
						<View style={{padding:0,marginRight:5,marginLeft:0, backgroundColor:'white'}}>
							<Image style={{width:55,height:55,resizeMode:'contain',marginTop:0}} source={ img.c3_p2 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#000',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Solicitar la recolección:</Text>
						<Text style={[smallText,{color:'#000',marginTop:5}]}>
						{'Elija la opción que sea de su agrado'}
						</Text>
						
						{data.telefono!=undefined &&
						<TouchableOpacity  onPress={()=>{
							Communications.phonecall(data.telefono[0].valor, true)
						}} style={{borderWidth:1,borderColor:'#0096d6',padding:12,paddingLeft:20,paddingRight:20,marginTop:16}}>
							<Text style={[smallText,{color:'#0096d6'}]}>
								{data.telefono[0].texto}
							</Text>
						</TouchableOpacity>
						}
						
						{data.formulario!=undefined &&
							<TouchableOpacity onPress={()=>{
							var url = data.formulario[0].valor;
							Reactotron.log(url);
							Linking.canOpenURL(url)
							  .then((supported) => {
							    if (!supported) {
							      console.log("Can't handle url: " + url);
							    } else {
							      return Linking.openURL(url);
							    }
							  })
							  .catch((err) => console.error('An error occurred', err));
							
						}} style={{borderWidth:1,borderColor:'#0096d6',padding:12,paddingLeft:20,paddingRight:20,marginTop:8}}>
							<Text style={[smallText,{color:'#0096d6'}]}>
								{data.formulario[0].texto}
							</Text>
						</TouchableOpacity>
						}
						
						
						{data.centros!=undefined &&
					        <TouchableOpacity   onPress={()=>{
							this.setState({Alert_Visibility:true});
						}} style={{borderWidth:1,borderColor:'#0096d6',padding:12,paddingLeft:20,paddingRight:20,marginTop:8}}>
							<Text style={[smallText,{color:'#0096d6'}]}>
								{data.centros[0].texto}
							</Text>
							</TouchableOpacity>
					      }
						
						{data.link!=undefined &&
					        <TouchableOpacity onPress={()=>{
							var url = data.link[0].valor;
							Reactotron.log(url);
							Linking.canOpenURL(url)
							  .then((supported) => {
							    if (!supported) {
							      console.log("Can't handle url: " + url);
							    } else {
							      return Linking.openURL(url);
							    }
							  })
							  .catch((err) => console.error('An error occurred', err));
							
						}} style={{borderWidth:1,borderColor:'#0096d6',padding:12,paddingLeft:20,paddingRight:20,marginTop:8}}>
							<Text style={[smallText,{color:'#0096d6'}]}>
								{data.link[0].texto}
							</Text>
							</TouchableOpacity>
					      }
						
					</View>
				</View>
		        
		 		        
		        
		        
		        </View>
		      )}

			</ScrollView>}
	
            </View></View>);
            
	}
}
const styles = StyleSheet.create({

Alert_Main_View:{
 
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor : "#fff", 
  width: '90%',
  borderRadius:0,
 
},
 
Alert_Title:{
 
  fontSize: 24, 
  color: "#444444",
  textAlign: 'center',
  fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
  padding: 0, 
},

Alert_Message:{
 
    fontSize: 14, 
    color: "#444444",
    textAlign: 'left',
    fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
    padding: 2,   
  },

buttonStyle: {
    
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'

},
   
TextStyle:{
    color:'#fff',
    textAlign:'center',
    fontSize: 22,
    marginTop: -5
}
 
});
export default PlanetPartners
