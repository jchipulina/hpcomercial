import React, { Component } from "react";

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import Reactotron from 'reactotron-react-native'

import { Search } from "@containers";

import { Navbar } from "@components";

class Buscar extends Component {
	constructor(props){
		super(props);
		var {type} = this.props.navigation.state.params;
		//Reactotron.log("Buscar ");
		global.trackScreenView("Buscar",{ tipocliente: global.user.tipocliente,tipo:type }); 
	}
	render() {
		
		
		
		var {term} = this.props.navigation.state.params;
		var {type} = this.props.navigation.state.params;
		var {navigation} = this.props;
		
		var search_content;
		if(type!=undefined){
			search_content = (<Search term={term} type={type} navigation={navigation}/>);
		}else{
			search_content = (<Search term={term} navigation={navigation}/>);
		}
		
    	
		var icoSize = 60;
		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'white'}} >

				 	<View style={{flex:1}} >
						<Navbar onBack={()=>{ navigation.pop() }}  onHome={()=>{navigation.navigate('Home')}}/>
						
					 	
					 	{search_content}


				 	</View>

			</View>)
	}
}

export default Buscar
