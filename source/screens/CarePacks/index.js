import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { img } from "@media"
import { Navbar } from "@components";
import FastImage from 'react-native-fast-image';
import Reactotron from 'reactotron-react-native';
var {height, width} = Dimensions.get('window');
import Dash from 'react-native-dash';
var parseString = require('react-native-xml2js').parseString;

class CarePacks extends Component {
	constructor(props){
		super(props);
		scope = this;
		this.state = {laptops:[]}
		
		global.trackScreenView("CarePacks",{ tipocliente: global.user.tipocliente});
		
	}
	async fetchData () {
	    var {item} = this.props.navigation.state.params;
        try { 
	        
			var url = global.api+'/ProductoSerieListado?int_IdProductoCategoria=6&int_IdUsuario='+global.user.id;

			let response = await fetch(url);
			let responseText = await response.text();
            parseString(responseText, function (err, result) {
	            
	            Reactotron.log(result);
	            
	           var resp = JSON.parse(result.string._);
	            
	            // Reactotron.log(resp);
	 
			    laptops = new Array();
		      	for(var i=0;i<resp.length;i++){
			      	var item = {};
			      	item.id = resp[i].productoserie.idserie;
			      	item.idserie = resp[i].productoserie.idserie;
			      	item.label = resp[i].productoserie.largename;
			      	item.img_url = resp[i].productoserie.image;
			      	item.pdf = resp[i].productoserie.pdf;
			      	laptops.push(item);
			      	
		      	}
			  	scope.setState({laptops:laptops});
			  
			  
			});
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
    }
	componentWillMount(){
		this.fetchData();
	}
	render() {
		var {navigation} = this.props;
		var icoSize = 60;
    var boxContainerStyle = {flexWrap:'wrap',width:width,backgroundColor:'white', flexDirection:'row'};
    var boxStyle = {flexBasis:'33.3%',backgroundColor:'white',flexDirection:'row'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:'HP Simplified'};
   
/*    var laptops = new Array();

		laptops.push({id:1,label:"Soporte al siguiente\ndía hábil en sitio",item_img:img.care_ico1});
    laptops.push({id:2,label:"Reemplazo único\nde batería",item_img:img.care_ico2});
    laptops.push({id:3,label:"Compromiso\nde reparación (CTR)",item_img:img.care_ico3});
    laptops.push({id:4,label:"Protección daños\naccidentales (DDP) (NBD)",item_img:img.care_ico4});
    laptops.push({id:5,label:"Retención de\nmedios defectuosos",item_img:img.care_ico5});
    laptops.push({id:6,label:"Soporte para\nviajeros",item_img:img.care_ico6});
    laptops.push({id:7,label:"Compromiso\nde reparación",item_img:img.care_ico7});

*/

		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'#fff'}} >

				 	<Navbar onBack={()=>{navigation.pop();}} onHome={()=>{navigation.navigate('Home')}}/>
				 	<View style={{flex:1,alignItems:'center',justifyContent:'center'}} >

					 	<Image style={{width:width,height:150,resizeMode:'cover'}} source={ img.top15 } />

            <View style={{backgroundColor:'white',width:width,paddingTop:16,paddingBottom:16}}>
            <Text style={{textAlign:'center',color:'#1259a1',fontSize:22,fontFamily:'HP Simplified',fontWeight: 'bold'}}>CarePacks</Text>
            <Text style={{textAlign:'center',color:'#1259a1',fontSize:16,fontFamily:'HP Simplified'}}>Cómputo</Text>

            </View>

            <ScrollView style={{flex:1}}>
    				  <View style={boxContainerStyle}>

					  <Dash style={{width:'100%', height:1}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={1}/>
					  	
              {this.state.laptops.map(r => <TouchableOpacity onPress={()=>{
	              //Reactotron.log(r.id);
	              
	              this.props.navigation.navigate('PDFVista',{pdf:r.pdf});
	              
              }}  key={r.id} style={boxStyle}>
                <View style={{flex:1,minHeight:100,justifyContent:'center', alignItems:'center',}}>
                
    						
    						
    						<FastImage
		                	  style={{width:60,height:60,marginTop:5}}
							  source={{
							    uri: r.img_url,
							    headers:{ Authorization: 'someToken' },
							    priority: FastImage.PriorityNormal
							  }}
							  resizeMode={FastImage.resizeMode.contain}
							/>
    						
							
							<Text style={{flex:1,marginTop:5,height:15,textAlign:'center',
								fontSize:10,color:'#87898b',paddingLeft:5,paddingRight:5,
								fontFamily:'HP Simplified'}}>{r.label}</Text>
							
							<Dash style={{width:'100%', height:1}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={1}/>	
                </View>
                <Dash style={{flexDirection:'column',width:1, height:'100%'}} dashColor={'#9fd8f0'} dashThickness={1} dashLength={1}/>

    					</TouchableOpacity>)}


    					</View>
    				</ScrollView>

				 	</View>

			</View>)
	}
}

export default CarePacks
