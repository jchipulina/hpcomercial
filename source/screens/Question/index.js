import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  Image,
  Linking,
  View,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { FeaturesBar } from "@components";
import { Navbar } from "@components";
import { BottomBar } from "@components";
import Reactotron from 'reactotron-react-native'
import { img } from "@media"
var currentPregunta = 1;
class Question extends Component {
	constructor(props){
		super(props);
		currentPregunta = 1;
		this.state = {
			page:1,
			respuestas:[
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false,
				false
			]
		}
		
		global.trackScreenView("Cuestionario",{ tipocliente: global.user.tipocliente}); 
	
	}
	onCheck(){
		var {navigation} = this.props;


		

	  	if(currentPregunta==6){
		  	navigation.navigate('Resultados', { respuestas:this.state.respuestas });
		  	return;
	  	}
	  	currentPregunta++;
	  	this.setState({page:currentPregunta});

  	}

	responder( rpta ){
		let r = this.state.respuestas;
		r[ rpta ] = !r[ rpta ];

		this.setState({ respuestas: this.state.respuestas })
	}

	eleccion( opciones, rpta ){
		let r = this.state.respuestas;
		//r[ rpta ] = !r[ rpta ];

		opciones.split("|").forEach( item => r[item] = false );
		r[ rpta ] = true;

		this.setState({ respuestas: this.state.respuestas })
	}
	componetWillMount(){
		currentPregunta = 1;
	}
	//navigation.navigate('Resultados');
	render() {
		var {navigation} = this.props;
		var icoSize = 60;

		var circle = {width:14,height:14,borderRadius:7,backgroundColor:'white',justifyContent:'center',
			alignItems:'center',borderColor:'gray',borderWidth:1
		};

		var circleUncheck = {width:14,height:14,borderRadius:7,backgroundColor:'#0096d6',
			justifyContent:'center',alignItems:'center'};

		var titleCircle = {textAlign:'center',color:'#00b3e3',fontSize:20,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};

		var checkBoxcontent = {width:'50%',height:30,backgroundColor:'transparent',flexDirection:'row',paddingLeft:15,marginTop:10}
		var checkBoxLabel = {marginLeft:10,marginTop:4,textAlign:'center',color:'#000',fontSize:14,fontWeight:'200',textAlign:'left',
								 fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',flex:1,backgroundColor:'transparent'}

		var checkBox ={width:24,height:26,backgroundColor:'white',borderWidth:1,borderColor:'#999', flex:0, alignItems:"center", justifyContent:"center", flexShrink:1, flexGrow:0 }
		var radio = { borderRadius: 10 }

		var pregunta = <View style={{flex:1,backgroundColor:'transparent'}}/>;

		var icons = new Array();
		for(var p=0;p<25;p++){
			icons.push((<View/>));
		}
		
		for(var p=0;p<25;p++){
			if(this.state.respuestas[p]==true){
				icons[p] = (<Icon name={(p>6)?"circle":"check"} color="#00b3e3" size={15}></Icon>);
			}
		}
		icons[0] = (<Icon name={"check"} color="#00b3e3" size={15}></Icon>);
		
		if(this.state.respuestas[0]==true){
			icon_1 = (<Icon name="check" color="#00b3e3" size={15}></Icon>)
		}
		var pregunta_1 = "¿Qué funciones busca en su impresora?";
		var pregunta_2 = "¿En que tono necesitas imprimir?";
		var pregunta_3 = "¿Necesita impresión (A3)?";
		var pregunta_4 = "¿Qué velocidad de impresión espera de su impresora?";
		var pregunta_5 = "¿Para cuántos usuarios estima el uso de su impresora?";
		var pregunta_6 = "¿Cuál es su volumen mensual de impresión?";
		
		var indicatorStyle = {fontSize:17,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginBottom:3};
		var labelStyle = {textAlign:'left',color:'#000',fontSize:16, fontWeight:'200',
							 fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',marginBottom:10};
		var topQuestion = {width:'90%', backgroundColor:'white' }
		
		var style_1 = {width:'100%',justifyContent:'center',alignItems:'center',padding:10,paddingBottom:0};
		var style_2 = {flex:1,flexDirection:'row',flexWrap:'wrap',paddingBottom:0,paddingTop:0,
								 justifyContent:'space-between',backgroundColor:'transparent'};
							 
		if(this.state.page==1){
			pregunta = (<View style={{flex:1,backgroundColor:'transparent'}}>
							
							
							
						<View style={{width:'100%',height:150}}>
        					<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.decicion_maker_1 } />
						</View>
						
						
						
							<View style={style_1}>
							<View style={topQuestion}>
								<Text style={indicatorStyle}> {'Pregunta 1/6'}</Text>
							 	<Text style={labelStyle}>{pregunta_1.toUpperCase()}</Text>
						 	</View>
						 	</View>
						 	
							 <View style={style_2}>

								 
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { this.responder(1) }}>
									 	{icons[1]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Fax</Text>
								 </View>

								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { this.responder(2) }}>
									 	{icons[2]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Copia</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { this.responder(3) }}>
									 	{icons[3]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Duplex</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { this.responder(4) }}>
									 	{icons[4]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Escanea</Text>
								 </View>

								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { this.responder(5) }}>
									 	{icons[5]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Ethernet</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={checkBox} onPress={ () => { this.responder(6) }}>
									 	{icons[6]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>WiFi</Text>
								 </View>
							 </View>
						 </View>);
		}
		
		if(this.state.page==2){
			pregunta = (<View style={{flex:1,backgroundColor:'transparent'}}>
							<View style={{width:'100%',height:150}}>
        						<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.decicion_maker_2 } />
							</View>
						 	<View style={style_1}>
								<View style={topQuestion}>
									<Text style={indicatorStyle}> {'Pregunta 2/6'}</Text>
								 	<Text style={labelStyle}>{pregunta_2.toUpperCase()}</Text>
							 	</View>
						 	</View>
							 <View style={style_2}>
								 <View style={[checkBoxcontent,{width:'100%'}]}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "7|8", 7) }}>
									 	{icons[7]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Impresión a Color</Text>
								 </View>
								 <View style={[checkBoxcontent,{width:'100%'}]}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "7|8", 8) }}>
									 	{icons[8]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Impresión en Blanco y Negro</Text>
								 </View>
							 </View>
						 </View>);
		}

		if(this.state.page==3){
			pregunta = (<View style={{flex:1,backgroundColor:'transparent'}}>
							<View style={{width:'100%',height:150}}>
        						<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.decicion_maker_3 } />
							</View>
						 	<View style={style_1}>
								<View style={topQuestion}>
									<Text style={indicatorStyle}> {'Pregunta 3/6'}</Text>
								 	<Text style={labelStyle}>{pregunta_3.toUpperCase()}</Text>
							 	</View>
						 	</View>
							 <View style={style_2}>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "9|10", 9) }}>
									 	{icons[9]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>Sí necesito</Text>
								 </View>
								  <View style={checkBoxcontent}>
								  </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "9|10", 10) }}>
									 	{icons[10]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>No necesito</Text>
								 </View>
							 </View>
						 </View>);
		}
		if(this.state.page==4){
			pregunta = (<View style={{flex:1,backgroundColor:'transparent'}}>
							<View style={{width:'100%',height:150}}>
        						<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.decicion_maker_4 } />
							</View>
						 	<View style={style_1}>
								<View style={topQuestion}>
									<Text style={indicatorStyle}> {'Pregunta 4/6'}</Text>
								 	<Text style={labelStyle}>{pregunta_4.toUpperCase()}</Text>
							 	</View>
						 	</View>
							 <View style={style_2}>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "11|12|13|14", 11) }}>
									 	{icons[11]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>10 - 30 ppm</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "11|12|13|14", 12) }}>
									 	{icons[12]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>40 - 50 ppm</Text>
								 </View>

								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "11|12|13|14", 13) }}>
									 	{icons[13]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>30 - 40 ppm</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "11|12|13|14", 14) }}>
									 	{icons[14]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>> 50 ppm</Text>
								 </View>

							 </View>
						 </View>);
		}
		if(this.state.page==5){
			pregunta = (<View style={{flex:1,backgroundColor:'transparent'}}>
							<View style={{width:'100%',height:150}}>
        						<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.decicion_maker_5 } />
							</View>
						 	<View style={style_1}>
								<View style={topQuestion}>
									<Text style={indicatorStyle}> {'Pregunta 5/6'}</Text>
								 	<Text style={labelStyle}>{pregunta_5.toUpperCase()}</Text>
							 	</View>
						 	</View>
							 <View style={style_2}>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "15|16|17|18", 15) }}>
									 	{icons[15]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>1 a 5</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "15|16|17|18", 16) }}>
									 	{icons[16]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>5 a 15</Text>
								 </View>

								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "15|16|17|18", 17) }}>
									 	{icons[17]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>15 a 30</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "15|16|17|18", 18) }}>
									 	{icons[18]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>+ de 30</Text>
								 </View>

							 </View>
						 </View>);
		}
		if(this.state.page==6){
			pregunta = (<View style={{flex:1,backgroundColor:'transparent'}}>
			
							<View style={{width:'100%',height:150}}>
        						<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ img.decicion_maker_6 } />
							</View>
						 	<View style={style_1}>
								<View style={topQuestion}>
									<Text style={indicatorStyle}> {'Pregunta 6/6'}</Text>
								 	<Text style={labelStyle}>{pregunta_6.toUpperCase()}</Text>
							 	</View>
						 	</View>
							 <View style={style_2}>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "19|20|21|22|23", 19) }}>
									 	{icons[19]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>500 - 1000</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "19|20|21|22|23", 20) }}>
									 	{icons[20]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>1000 - 3000</Text>
								 </View>

								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "19|20|21|22|23", 21) }}>
									 	{icons[21]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>3000 - 7000</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "19|20|21|22|23", 22) }}>
									 	{icons[22]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>7000 - 10000</Text>
								 </View>
								 <View style={checkBoxcontent}>
									 <TouchableOpacity style={[checkBox,radio]} onPress={ () => { this.eleccion( "19|20|21|22|23", 23) }}>
									 	{icons[23]}
									 </TouchableOpacity>
									 <Text style={checkBoxLabel}>> 10000</Text>
								 </View>

							 </View>
						 </View>);
		}


		return (<View style={{flex:1}} >
        			<Navbar onBack={()=>{ navigation.pop(); }} onHome={()=>{navigation.navigate('Home')}}/>
        			<View style={{flex:1,backgroundColor:'white',padding:0 }}>
					 	
					 	<View style={{flex:1}}>
					 	

						 

						 {pregunta}


						<View style={{width:'100%',justifyContent:'center',alignItems:'center',marginBottom:20}}> 
						<View style={{width:'60%',backgroundColor:'transparent',flexDirection:'row',justifyContent:'space-between',paddingRight:0,paddingLeft:0}} >

							<TouchableOpacity onPress={()=>{this.setState({page:1});
							currentPregunta=1 }} style={(this.state.page>=1)?circle:circleUncheck}>
							</TouchableOpacity>

							<TouchableOpacity onPress={()=>{this.setState({page:2});
							currentPregunta=2}} style={(this.state.page>=2)?circle:circleUncheck}>
							</TouchableOpacity>

							<TouchableOpacity 
							onPress={()=>{this.setState({page:3});
							currentPregunta=3}} style={(this.state.page>=3)?circle:circleUncheck}>
							</TouchableOpacity>

							<TouchableOpacity 
							onPress={()=>{this.setState({page:4});
							currentPregunta=4}} style={(this.state.page>=4)?circle:circleUncheck}>
							</TouchableOpacity>

							<TouchableOpacity 
							onPress={()=>{this.setState({page:5});
							currentPregunta=5}} style={(this.state.page>=5)?circle:circleUncheck}>
							</TouchableOpacity>

							<TouchableOpacity 
							onPress={()=>{this.setState({page:6});
							currentPregunta=6}} style={(this.state.page>=6)?circle:circleUncheck}>
							</TouchableOpacity>

						</View>
						</View>
						
						
						
						</View>

						<BottomBar onSiguiente={()=>{ this.onCheck() }}
						onSearch={()=>{ }}/>
				 	</View>
			</View>)
	}
}

export default Question
