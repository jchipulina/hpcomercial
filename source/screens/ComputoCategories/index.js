import React, { Component } from "react";
import {
  View,
  Text
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import { Categories } from "@containers";
import { Navbar,BottomBar } from "@components";
import Reactotron from 'reactotron-react-native'
var parseString = require('react-native-xml2js').parseString;

export default class ComputoCategories extends Component {
	
	constructor(props){
		super(props);
		this.state = { novedosos:global.featureComp};
		global.trackScreenView("ComputoCategories",{ tipocliente: global.user.tipocliente}); 
	}
	componentWillMount(){
		this.getProductosNovedosos(); 
	}
	async getProductosNovedosos(){
		var url =  global.api+'/ProductosNovedososListado?int_IdTipoProducto=1&int_IdUsuario='+global.user.id;
		let response = await fetch(url,{method: "GET"});
		let responseText = await response.text();
		global.featureComp = new Array();
		var scope = this;
		parseString(responseText, function (err, result) {
			var resp = JSON.parse(result.string._);
			for(var i=0;i<resp.length;i++){
				global.featureComp.push(resp[i].productonovedoso);
			}
			Reactotron.log( global.featureComp );
			scope.setState({ novedosos:global.featureComp})
		});
	}
	render() {
    var {navigation} = this.props;

		const list = [
			{
				key: "notebooks",
				title: "Notebooks",
				image: img.cat1,
				top: img.cat1,
				idcategoria: 1
			},{
				key: "desktop",
				title: "Desktops",
				image: img.cat2,
				top: img.cat2,
				idcategoria: 2
			},{
				key: "workstations",
				title: "Workstations",
				image: img.cat3,
				top: img.cat3,
				idcategoria: 4
			},{
				key: "monitores",
				title: "Monitores",
				image: img.cat4,
				top: img.cat4,
				idcategoria: 3
			},{
				key: "pos",
				title: "Retail POS",
				image: img.cat5,
				top: img.cat5,
				idcategoria: 5
			},{
				key: "care-packs",
				title: "HP Care Pack",
				image: img.cat6,
				top: img.cat6,
				idcategoria: 6
			},
		]
		return(<View style={{flex:1}}>
            		<Navbar onBack={()=>{navigation.pop()}} showWinPro={true} onHome={()=>{navigation.navigate('Home')}}/>
            		<View style={{flex:1,marginLeft:8,marginRight:8,marginBottom:8}}>
						<Categories title="Cómputo" list={list} navigation={this.props.navigation} novedosos={this.state.novedosos}/>
					</View>
					<BottomBar onImpresion={()=>{
						 navigation.navigate('ImpresionCategories');
					}} onSearch={()=>{
						navigation.navigate('Buscar',{term:'',type:'computo'});
					}} />
		       </View>)
	}
}
