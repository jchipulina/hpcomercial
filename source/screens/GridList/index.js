import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  Linking,
  SafeAreaView,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { img } from "@media"
import { Navbar } from "@components";
var {height, width} = Dimensions.get('window');
import Dash from 'react-native-dash';
import { BottomBar } from "@components"
import Reactotron from 'reactotron-react-native'
import OfflineFirstAPI from 'react-native-offline-api';
import FastImage from 'react-native-fast-image'
var laptops = new Array();
var scope;
var parseString = require('react-native-xml2js').parseString;
var partnersData;
	
class GridList extends Component {
	constructor(props){
		super(props);
		scope = this;
		this.state = {laptops:[],tabSelected:0,showButtonUp:false,showPartner:false}
		
		 
	}
	async fetchDataPartners(){
		if(global.user.pais!='Colombia'){
			try { 
				//242 costa
				//243 Puerto rico
				var url = global.api+"/PlanetPartner_Detalle?int_IdUsuario="+global.user.id;
				let response = await fetch(url);
				let responseText = await response.text();
				laptops = new Array();
	            parseString(responseText, function (err, result) {
		            if(result.string._!=undefined){
			            var resp = JSON.parse(result.string._);
			            
			            partnersData = resp[0].programa;
			            Reactotron.log(partnersData);
			            scope.setState({showPartner:true});
		            }
				});
	        } catch (err) {
	            Reactotron.log("ERROR "+err);
	        }			
		}

	}
    async fetchData () {
	    var {item} = this.props.navigation.state.params;

		global.trackScreenView(item.title, { tipocliente: global.user.tipocliente} );
	    
		if(item.key=="Suministros"){
			var laptops = new Array();
				var series = new Array();
				//Cartuchos Originales 
				series.push({id:1,shortname:'Benefician a la Productividad'});
				series.push({id:2,shortname:'Cartuchos Originales HP VS No Originales'});
				series.push({id:3,shortname:'Los expertos en Impresoras Recomiendan HP'});
				series.push({id:4,shortname:'Desenpeño Excepcional'});
				series.push({id:5,shortname:'Características de los Cartuchos Originales HP'});
				series.push({id:6,shortname:'Cómo identificar un Cartucho Falsificado'});
				series.push({id:7,shortname:'Resultados Confiables y Consistentes'});
				series.push({id:8,shortname:'Mitos y Realidades'});
				series.push({id:9,shortname:'Remanufacturar no es Reciclar'});

	        	laptops.push({id:1,nombre:'RAZONES PARA USAR CARTUCHOS ORIGINALES HP',shortname:'RAZONES PARA USAR CARTUCHOS ORIGINALES HP ',series:series});
	        	laptops.push({id:10,nombre:'PROPUESTA DE VALOR',shortname:'PROPUESTA DE VALOR',series:[]});
	        	laptops.push({id:11,nombre:'AHORRE TIEMPO Y DINERO',shortname:'AHORRE TIEMPO Y DINERO',series:[]});
	        	laptops.push({id:12,nombre:'ELIGIENDO CARTUCHOS ORIGINALES HP AYUDA A IMPULSAR UN CAMBIO POSITIVO',shortname:'ELIGIENDO CARTUCHOS ORIGINALES HP AYUDA A IMPULSAR UN CAMBIO POSITIVO',series:[]});
		      	
			  	scope.setState({laptops:laptops});
			  	return;
		}
		if(item.key=="OfficeJet" || item.key=="PageWide" || item.key=="LaserJet" || item.key=="ScanJet"){
		
		
		
		try { 

			var url = global.api+'/ImpresoraSerieListado?int_IdProductoGrupo='+item.idcategoria+"&int_IdUsuario="+global.user.id;
			
			


			let response = await fetch(url);
			let responseText = await response.text();
			laptops = new Array();
            parseString(responseText, function (err, result) {
	            var resp = JSON.parse(result.string._);
	            //Reactotron.log('ImpresoraSerieListado');
	            //Reactotron.log(resp);
	            for(var i=0;i<resp[0].categorias.length;i++){
		            var categoria = resp[0].categorias[i].categoria;
		            var cat = {};
		            cat.id = categoria.id;
		            cat.nombre = categoria.nombre;
		            cat.descripcion = categoria.descripcion;
		            cat.series = new Array();
		           // Reactotron.log(categoria.categoria.series);
		            //Reactotron.log(categoria);
		            for(var k=0;k<categoria.series.length;k++){
			            var item = {};
			            item.id = categoria.series[k].idserie;
				      	item.idserie = categoria.series[k].idserie;
				      	item.shortname = categoria.series[k].shortname;
				      	//item.descripcion = categoria.series[k].descripcion;
				      	item.largename = categoria.series[k].largename;
				      	item.img_url = categoria.series[k].image;
				      	item.icon1 = categoria.series[k].icon1;
				      	item.icon2 = categoria.series[k].icon2;
				      	item.icon3 = categoria.series[k].icon3;
				      	//item.idcategoria = categoria.series[k].idcategoria;

				      	cat.series.push(item);
		            }
		            
			      	laptops.push(cat);
		        }
			  	scope.setState({laptops:laptops});
			});
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
        
        
        
        
        	
		}else{
			
		
		
		
        try { 
 	
			var url = global.api+'/ProductoSerieListado?int_IdProductoCategoria='+item.idcategoria+"&int_IdUsuario="+global.user.id;
			Reactotron.log("url "+url);
			let response = await fetch(url);
			let responseText = await response.text();
            parseString(responseText, function (err, result) {
	            //Reactotron.log("*");
	            //Reactotron.log(result.string._);
	            
	            var resp = JSON.parse(result.string._);
			    laptops = new Array();
		      	for(var i=0;i<resp.length;i++){
			      	var item = {};
			      	item.id = resp[i].productoserie.idserie;
			      	item.idserie = resp[i].productoserie.idserie;
			      	item.shortname = resp[i].productoserie.shortname;
			      	item.descripcion = resp[i].productoserie.descripcion;
			      	item.largename = resp[i].productoserie.largename;
			      	item.img_url = resp[i].productoserie.image;
			      	item.icon1 = resp[i].productoserie.icon1;
			      	item.icon2 = resp[i].productoserie.icon2;
			      	item.icon3 = resp[i].productoserie.icon3;
			      	
			      	item.idcategoria = resp[i].productoserie.idcategoria;
			      	if(Number(item.idcategoria)==15 || Number(item.idcategoria)==6){

				      	item.pdf = resp[i].productoserie.pdf;
			      	}
			      	//Reactotron.log("item push");
			      	laptops.push(item);
		      	}
		      	//Reactotron.log("laptops");
			  	scope.setState({laptops:laptops});
			  
			});
			
        } catch (err) {
            Reactotron.log("ERROR "+err);
        }
        
        }
    }
	componentWillMount(){
		this.fetchData();
		this.fetchDataPartners();
	}
	check(idserie){
		for(var i=0;i<global.featureComp.length;i++){
			if(Number(global.featureComp[i].idserie)==Number(idserie)){
				return true;
			}
		}
		return false;
	}
	render() {

    var {navigation} = this.props;
    var {item} = this.props.navigation.state.params;
	var {type} = this.props.navigation.state.params;

	var icoSize = 60;
    var boxContainerStyle = {width:'85%',backgroundColor:'transparent', flexDirection:'column'};
    var boxStyle = {backgroundColor:'white'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};

	var elijaserie = (<View style={{height:10}}/>);
	var planetpartners = (<View/>);
	
	//Reactotron.log('item.key '+item.key)
	if(item.key!='Suministros'){
		elijaserie = (<Text style={{padding:20,paddingBottom:10,paddingTop:30,fontSize:18,paddingLeft:30,color:'#000',fontWeight:'200',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'ELIJA UNA SERIE:'}</Text>);
		                
		
	}
	//Reactotron.log(global.user);
	
	//Pánama, Costa Rica, Puerto Rico y Ecuador
	var partner = (<View></View>);
	if(this.state.showPartner){
		partner = (<TouchableOpacity onPress={()=>{
				
				 navigation.navigate('PlanetPartners',{data:partnersData});
				
			}} style={{width:'80%',justifyContent:'center',alignItems:'center',height:60,backgroundColor:'#bf951e',flexDirection:'row',marginTop:10}}>
			
					<Text style={{flex:1,paddingLeft:10,fontSize:17,color:'#fff',lineHeight:15,
						fontWeight:'300',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
						{'Descubre lo nuevo de HP Planet Partner'}</Text>
		             
		             <View style={{width:130,justifyContent:'center',alignItems:'center',backgroundColor:'transparent'}}>   
		             	
		             	<Image style={{width:50,height:40,resizeMode:'contain'}} source={ img.triangulo } />
		             	
					 </View>
			</TouchableOpacity>);
	}
	if(item.key=='Suministros'){
		planetpartners = (<View style={{width:'100%',alignItems:'center'}}>
		
			<TouchableOpacity onPress={()=>{
				
				navigation.navigate('EncuentraLaTinta');
				
				
			}} style={{width:'80%',justifyContent:'center',alignItems:'center',height:60,backgroundColor:'#60338c',flexDirection:'row',marginTop:20}}>
			
					<Text style={{flex:1,paddingLeft:10,fontSize:16,color:'#fff',lineHeight:15,
						fontWeight:'300',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
						{'Encuentra aqui la tinta y toner original para tu impresora'}</Text>
		             
		             <View style={{width:130,justifyContent:'center',alignItems:'center',backgroundColor:'transparent'}}>   
		             	<Image style={{width:80,height:40,resizeMode:'contain'}} source={ img.impresora_morado } />
					 </View>
			</TouchableOpacity>
			
			
			{partner}
			
			
		</View>
		)
	}
	
		var bajada = (<View/>);
		 var contenido;
		 
		 var preloader = (<View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center',backgroundColor:'white'}}>
		 	<Image style={{width:40,height:40,resizeMode:'cover'}} source={ img.loading } />
		 </View>);
		 
		 
		 if(item.key=="OfficeJet" || item.key=="PageWide" || item.key=="LaserJet" || item.key=="ScanJet"){
			 
			 
	             if(this.state.laptops.length>0){
		             Reactotron.log(this.state.laptops);
		             contenido = (<Listado items={this.state.laptops} navigation={this.props.navigation}
			 		onSelected={(index)=>{
	                 this.setState({tabSelected:index});
	             }} />);
	             }else{
		             contenido = preloader;
	             }
	             
	             
	      }else if(item.key=="Suministros"){ 
		      bajada = (<Text style={{textAlign:'center',color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15}}>{'Impresión'}</Text>);      
			contenido = (<Listado2 items={this.state.laptops} navigation={this.props.navigation}
			 		onSelected={(index)=>{
	                 this.setState({tabSelected:index});
	             }} />);
	             
	              
		 }else{
			 
			 Reactotron.log("items:"+this.state.laptops.length);
	           if(this.state.laptops.length>0){
			 contenido = (this.state.laptops.map(r => <View  key={r.id} style={{backgroundColor:'white'}}>
                <TouchableOpacity onPress={()=>{
	                if(r.pdf!=null){
		                 navigation.navigate('PDFVista',{pdf:r.pdf});
	                }else{
		                 navigation.navigate('ItemProfile',{type:type,item:r});
	                }
                }} style={{flex:1,alignItems:'center'}}>
                <View style={{marginLeft:10,width:'95%',height:1,backgroundColor:'#eaebeb'}}></View>
                <View style={{flex:1, flexDirection:'row'}}>
                <View style={{marginTop:8,width:20,height:20,backgroundColor:'transparent'}}>
                	{this.check(r.idserie) && (
	                	<Image style={{width:20,height:20,resizeMode:'contain'}} source={ img.star_solid } />
	                )}
                </View>
                <View style={{flex:1,backgroundColor:'white',paddingLeft:10,paddingBottom:8 }}>
	                <Text style={{marginTop:6,marginBottom:0,fontSize:16,color:(item.key=='care-packs')?'black':'#35b3e2',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.shortname.toUpperCase()}</Text>
		              <Text style={{color:'black', fontWeight:'200',fontSize:14,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
		            {r.descripcion}</Text>   
                </View>
				<View style={{width:100,paddingRight:10, backgroundColor:'white',justifyContent:'center',alignItems:'flex-end'}}>
				<Octicons name={'chevron-right'} size={16} color="#35b3e2"/>					
				</View>
                </View>
                </TouchableOpacity>
    					</View>));
    					 }else{
		             contenido = preloader;
	             }
    					
		 }    
		 
		 
		 
		 
		 
		            
		 var btnUp = (<View/>);
			if(this.state.showButtonUp){
				btnUp = (<TouchableOpacity onPress={()=>{ 
					this.scrollView.scrollTo({y: 0})
					
				}} 
				//,bottom:100,right:10
				style={{width:30, height:30,borderRadius:15,justifyContent:'center',alignItems:'center',
					backgroundColor:'rgba(0,0,0,0.5)',position:'absolute',bottom:100,right:15}}>
					<Icon name={'angle-up'} size={15} color="#fff"/>
				</TouchableOpacity>);
			}
			//<Text style={{textAlign:'center',color:'white',fontSize:15,lineHeight:15,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{(type=='computo')?'Cómputo':'Impresión'}</Text>
		return (<View style={{flex:1,backgroundColor:'#fff'}} >

				 	<Navbar onBack={()=>{navigation.pop()}}  showWinPro={(type=='computo')?true:false} onHome={()=>{navigation.navigate('Home')}}/>
				 	<View style={{flex:1}} >
				 	
				 	
					 	<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ item.top } />
					 	
					 	

            <View style={{backgroundColor:'#0096d6',width:'100%',paddingTop:20,paddingBottom:15}}>
            
            <Text style={{textAlign:'center',color:'white',fontSize:22,lineHeight:20,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{item.title.toUpperCase()}</Text>
            {bajada}
 
            </View>
			
			{planetpartners}
			
			<ScrollView style={{flex:1}} contentContainerStyle={{alignItems:'center'}} horizontal={false}
				ref={ref => this.scrollView = ref}
				onScroll={(event)=>{
					if(event.nativeEvent.contentOffset.y>200){
						this.setState({showButtonUp:true});
					}else{
						this.setState({showButtonUp:false});
					}
				}}
			>

            
    				  <View style={boxContainerStyle}>
					  
					 
					  	{elijaserie}  
					  	
					  	
					  	
					  	
					  	{contenido}
    					
    					</View>
    				</ScrollView>
    				{btnUp}
            <BottomBar onComparar={()=>{
                navigation.navigate('Comparar',{type:type});
            }} onSearch={()=>{
				navigation.navigate('Buscar',{term:'',type:type});
            }}/>
				 	</View>

			</View>)
	}
}

			
	             
	             
	             
class Listado extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selected : 0
		}
	}
	check(id){
		for(var i=0;i<global.featurePrint.length;i++){
			if(Number(global.featurePrint[i].idcategoria)==Number(id)){
				return true;
			}
		}
		return false;
	}
	check_serie(idcategoria,id){
		for(var i=0;i<global.featurePrint.length;i++){
			if(Number(global.featurePrint[i].idcategoria)==Number(idcategoria)){
				if(Number(global.featurePrint[i].idserie)==Number(id)){
					return true;
				}
			}
		}
		return false;
	}
	render() {
		var boxStyle = {backgroundColor:'white'};
		var label1 = {marginTop:6,marginBottom:0,fontSize:16,color:'#35b3e2',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		 var label2 = {color:'black', fontWeight:'200',fontSize:14,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};               
		   var label3 = {marginTop:6,marginBottom:0,fontSize:16,color:'#000',fontWeight:'200',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
         
         

                        
		return (this.props.items.map(r => <View  key={r.id} style={boxStyle}>
                <View onPress={()=>{
		                // navigation.navigate('ItemProfile',{type:type,item:r});
                }} style={{flex:1,alignItems:'center'}}>
                
                <View style={{marginLeft:10,width:'95%',height:1,backgroundColor:'#eaebeb'}}></View>
                <View style={{flex:1, flexDirection:'row'}}>
                
                 <View style={{marginTop:8,width:20,height:20,backgroundColor:'transparent'}}>
                	{this.check(r.id) && (
	                	<Image style={{width:20,height:20,resizeMode:'contain'}} source={ img.star_solid } />
	                )}
                </View>
                
                <TouchableOpacity onPress={()=>{
					if(r.id!=this.state.selected){ this.setState({ selected:r.id })
					}else{ this.setState({ selected:0 }) }
                }} style={{flex:1,backgroundColor:'white',paddingLeft:10,paddingBottom:8}}>
	                	<Text style={label1}>{r.nombre.toUpperCase()}</Text>
		                <Text style={label2}>{r.descripcion}</Text>   
                </TouchableOpacity>
				<View style={{width:100,paddingRight:10, backgroundColor:'white',justifyContent:'center',alignItems:'flex-end'}}>
				
				
				
				{r.series.length>0 ? (
			        <TouchableOpacity onPress={()=>{
					if(r.id!=this.state.selected){ this.setState({ selected:r.id })
					}else{ this.setState({ selected:0 }) }
                }} style={{width:33, height:33, backgroundColor:'white'}}>

					<EvilIcons name={(this.state.selected!=r.id)?'plus':'minus'} size={33} color="gray"/>
			
				</TouchableOpacity>
			      ) : (
			        <View><Octicons name={'chevron-right'} size={16} color="#35b3e2"/></View>
			      )}
      
				
				</View>
                </View>
                </View>
                
                
                
				{this.state.selected==r.id &&
					<View>
				        {r.series.map(b => <TouchableOpacity onPress={()=>{
				                 this.props.navigation.navigate('ItemProfile',{type:'impresion',item:b});
		                }} style={{flex:1,alignItems:'center'}}>
		                <View style={{width:'100%',height:1,backgroundColor:'#eaebeb'}}></View>
		                <View style={{flex:1, flexDirection:'row'}}>
		                <View style={{flex:1,backgroundColor:'transparent',paddingLeft:30,paddingBottom:10,paddingTop:8 }}>
			                <Text style={[label3,{color:(this.check_serie(r.id,b.id))?'#a53894':'#000'}]}>{b.shortname.toUpperCase()}</Text>
						</View>
						<View style={{width:100,paddingRight:10, backgroundColor:'white',justifyContent:'center',alignItems:'flex-end'}}>
						<Octicons name={'chevron-right'} size={16} color="#35b3e2"/>					
						</View>
		                </View>
		                </TouchableOpacity>)}
	                </View>}
      
                	
                	
    					</View>));

	}
}


class Listado2 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selected : 0
		}
	}
	
	render() {
		var boxStyle = {backgroundColor:'white'};
		var label1 = {marginTop:13,marginBottom:13,fontSize:16,color:'#000',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		 var label2 = {color:'black', fontWeight:'200',fontSize:14,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};               
		   var label3 = {marginTop:6,marginBottom:0,fontSize:16,color:'#000',fontWeight:'200',
		                fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
         
         var navigation = this.props.navigation;

                        
		return (this.props.items.map(r => <View  key={r.id} style={boxStyle}>
                <View onPress={()=>{
		                // navigation.navigate('ItemProfile',{type:type,item:r});
                }} style={{flex:1,alignItems:'center'}}>
                
                {r.id!=1 ? (
	                <View style={{width:'100%',height:1,backgroundColor:'#eaebeb'}}></View>
	                ) : (
		            <View></View>
                )}
                <View style={{flex:1, flexDirection:'row'}}>
                
                <TouchableOpacity onPress={()=>{
					if(r.id!=this.state.selected){ this.setState({ selected:r.id })
					}else{ this.setState({ selected:0 }) }
					if(r.series.length==0){
						this.props.navigation.navigate('SuministroScreen',{section:r.id,title:r.shortname});
					}	
					
                }} style={{flex:1,backgroundColor:'transparent',paddingLeft:0,paddingBottom:0}}>
	                	<Text style={label1}>{r.nombre.toUpperCase()}</Text>  
                </TouchableOpacity>
				<View style={{width:40,paddingRight:5, backgroundColor:'white',justifyContent:'center',alignItems:'flex-end'}}>
				
				
				
				{r.series.length>0 ? (
			        <TouchableOpacity onPress={()=>{
					if(r.id!=this.state.selected){ this.setState({ selected:r.id })
					}else{ this.setState({ selected:0 }) }
                }} style={{width:33, height:33, backgroundColor:'white'}}>

					<EvilIcons name={(this.state.selected!=r.id)?'plus':'minus'} size={33} color="gray"/>
			
				</TouchableOpacity>
			      ) : (
			        <TouchableOpacity onPress={()=>{

						this.props.navigation.navigate('SuministroScreen',{section:r.id,title:r.shortname});
				
					
                }}>
	                	<Octicons name={'chevron-right'} size={16} color="#35b3e2"/> 
                </TouchableOpacity>
			      )}
      
				
				</View>
                </View>
                </View>
                
                
                
				{this.state.selected==r.id &&
					<View>
				        {r.series.map(b => <TouchableOpacity onPress={()=>{
				                 
				                  this.props.navigation.navigate('SuministroScreen',{section:b.id,title:b.shortname});
				                 
		                }} style={{flex:1,alignItems:'center'}}>
		                <View style={{width:'100%',height:1,backgroundColor:'#eaebeb'}}></View>
		                <View style={{flex:1, flexDirection:'row'}}>
		                <View style={{flex:1,backgroundColor:'transparent',paddingLeft:5,paddingBottom:10,paddingTop:8 }}>
			                <Text style={label3}>{b.shortname}</Text>
		                </View>
						<View style={{width:100,paddingRight:10, backgroundColor:'white',justifyContent:'center',alignItems:'flex-end'}}>
						<Octicons name={'chevron-right'} size={16} color="#35b3e2"/>					
						</View>
		                </View>
		                </TouchableOpacity>)}
	                </View>}
      
                	
                	
    					</View>));

	}
}
/*
	<Text style={{color:'black', fontWeight:'200',fontSize:14,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
		            {''}</Text>
		            */
export default GridList
