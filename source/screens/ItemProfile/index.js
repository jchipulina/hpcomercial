import React, { Component } from "react";

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  Linking,
  View,
  Image,
  Dimensions,
  Platform,
  ScrollView,
} from 'react-native';
import { connect } from "react-redux";
import Toast from 'react-native-easy-toast'
import { Inicio,FeaturesBar } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import FastImage from 'react-native-fast-image'
import EvilIcons  from 'react-native-vector-icons/EvilIcons';
import Reactotron from 'reactotron-react-native';
import Swiper from 'react-native-swiper';
import { img } from "@media"
import { Navbar } from "@components";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
var {height, width} = Dimensions.get('window');
import Dash from 'react-native-dash';
import { Selectable } from "@components";
import {Select, Option} from "react-native-chooser";
var parseString = require('react-native-xml2js').parseString;
var pBottom = 0;
var hHeight = 55;
var scope;
var sentGoogle=true;
var currentTab = 0;
var productos = new Array();
class ItemProfile extends Component {
  constructor(props){
    super(props);
    scope = this;
    if(this.isIphoneX()){
	    	hHeight = 80
	    	pBottom = 25;
    	}
    this.state = {
	    currentSlide:0,
	    productos:[],
	    tabSelected:0,
	    page:'t0',
	    contizacionIn:false,
	    accesoriosSelected:false,
	    settingsSelected:false,
	    tintaSelected:false,

	    swiperIndex:0,
	    itemSelected:0,
	    isFavorite:false,
	    accesorioSelected:0,
	    suministroSelected:0,
	    value : "Printer 1"};



  }

  onPressFavorite(){

	  var producto = this.state.productos[this.state.tabSelected];
	  this.setState({isFavorite:!this.state.isFavorite});
	  if(!this.state.isFavorite){
		  this.refs.toast.show('Agregado a favoritos');
		  this.addFavorite(producto);
	  }else{
		  this.removeFavorite(producto);
	  }
  }
  onPressCotizador(flag){
	  var producto = this.state.productos[this.state.tabSelected];
	  if(flag){
		  this.addCotizador(producto);
	  }else{
		  this.removeCotizador(producto);
	  }
  }
  addCotizador(producto){
	  var exist = false;
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id == producto.id){
			  exist = true;
		  }
	  }
	  if(!exist){
		  global.user.cotizador.push(producto);
	  }
	  global.saveUser();
	  this.navbar.update();
  }
  removeCotizador(producto){
	  var temp = new Array();
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id != producto.id){
		  	temp.push(global.user.cotizador[i]);
		  }
	  }
	  global.user.cotizador = temp;
	  global.saveUser();
	  this.navbar.update();
  }
  addFavorite(producto){
	  var exist = false;
	  for(var i=0;i<global.user.favoritos.length;i++){
		  if(global.user.favoritos[i].id == producto.id){
			  exist = true;
		  }
	  }
	  if(!exist){
		  global.user.favoritos.push(producto);
	  }
	  global.saveUser();
  }
  removeFavorite(producto){
	  var temp = new Array();
	  for(var i=0;i<global.user.favoritos.length;i++){
		  if(global.user.favoritos[i].id != producto.id){
		  	temp.push(global.user.favoritos[i]);
		  }
	  }
	  global.user.favoritos = temp;
	  global.saveUser();
  }
  checkLikes(index){
	  this.navbar.update();
	  if(productos.length>0){
		  var producto_id = productos[index].id;
			if(this.searchLikeById(producto_id)){
				this.setState({isFavorite:true});
			}else{
				this.setState({isFavorite:false});
			}
			if(this.searchCotById(producto_id)){
				this.setState({contizacionIn:true});
			}else{
				this.setState({contizacionIn:false});
			}
	  }

  }
  searchLikeById(producto_id){
	  for(var i=0;i<global.user.favoritos.length;i++){
		  if(global.user.favoritos[i].id == producto_id){
			  return true;
		  }
	  }
	  return false;
  }
  searchCotById(producto_id){
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id == producto_id){
			  return true;
		  }
	  }
	  return false;
  }
  resetSwiper(){

		this.swiper.scrollBy((this.state.currentSlide*-1),false);
  }
  async fetchData () {
	    var {item} = this.props.navigation.state.params;
	    var {type} = this.props.navigation.state.params;
		var scope = this;
        try {
			var url = global.api+'/ProductoListado?int_IdProductoSerie='+item.idserie+"&int_IdUsuario="+global.user.id;

			let response = await fetch(url);
			let responseText = await response.text();
            parseString(responseText, function (err, result) {
	            var resp = JSON.parse(result.string._);
	            var pserver = resp[0];
	            productos = new Array();
	            for(var i=0;i<pserver.productos.length;i++){
		            var obj = {}
		            obj.tab = "t"+i;

		            obj.id = pserver.productos[i].producto.id;
		            obj.name =  pserver.productos[i].producto.name;
		            obj.pdf =  pserver.productos[i].producto.pdf;
		            obj.employee = false;

		            if(global.user.codigo!=""){
			            if(pserver.productos[i].producto.datasheet!=""){
				            obj.datasheet = pserver.productos[i].producto.datasheet;
				            obj.employee = true;
			            }
		            }

					obj.type = type;
					obj.serie = item;
		            obj.imagenes = new Array();
		            if(pserver.productos[i].producto.imagenes != null){
			            if(pserver.productos[i].producto.imagenes.length>0){
				            obj.imagenes =  pserver.productos[i].producto.imagenes;
			            }
		            }else{
			            obj.imagenes = new Array();
		            }
		            if(pserver.productos[i].producto.especificaciones != null){
			            if(pserver.productos[i].producto.especificaciones.length>0){
				            obj.especificaciones =  pserver.productos[i].producto.especificaciones;
			            }
		            }else{
			            obj.especificaciones = new Array();
		            }

		            if(pserver.productos[i].producto.accesorios != null){
			            if(pserver.productos[i].producto.accesorios.length>0){
				            obj.accesorios =  pserver.productos[i].producto.accesorios;
			            }
		            }else{
			            obj.accesorios = new Array();
		            }
		            if(pserver.productos[i].producto.suministros != null){
			            if(pserver.productos[i].producto.suministros.length>0){
				            obj.suministros =  pserver.productos[i].producto.suministros;
			            }
		            }else{
			            obj.suministros = new Array();
		            }



		            productos.push(obj);
	            }
	            var tabSelected = scope.getTabSelectedFeatured(type,productos);
	            
	            scope.setState({productos:productos,tabSelected:tabSelected});
	            currentTab = tabSelected;
	            scope.checkLikes(0);
			});
        } catch (err) {
       
        }
    }
    getTabSelectedFeatured(type,productos){
	    if(type=="impresion"){
		            for(var i=0;i<global.featurePrint.length;i++){
			            var idproducto = Number(global.featurePrint[i].idproducto);
						if(idproducto!=0){
							for(var p=0;p<productos.length;p++){
								if(Number(productos[p].id)==idproducto){
									return p;
								}
							}
						}
					}
	            }else{
		            for(var i=0;i<global.featureComp.length;i++){
						var idproducto = Number(global.featureComp[i].idproducto);
						if(idproducto!=0){
							for(var p=0;p<productos.length;p++){
								if(Number(productos[p].id)==idproducto){
									return p;
								}
							}
						}
					}
	            }
	            return 0;
    }
	componentWillMount(){
		var {producto} = this.props.navigation.state.params;
		productos = new Array();
		if(producto==undefined){
			this.fetchData();
			global.updateLikes = ()=>{
				scope.checkLikes(currentTab);
  			}
		}else{
			productos.push(producto);
			scope.setState({productos:productos});
			scope.checkLikes(0);
		}
	}
	isIphoneX() {
	    const dimen = Dimensions.get('window');
	    return (
	        Platform.OS === 'ios' &&
	        !Platform.isPad &&
	        !Platform.isTVOS &&
	        (dimen.height === 812 || dimen.width === 812)
	    );
	}
	onSelect(value, label) {
	    //this.setState({value : value});
	}
	render() {
		var {item} = this.props.navigation.state.params;
		var {type} = this.props.navigation.state.params;

		var fromResults = false;
	    var {navigation} = this.props
		var icoSize = 60;
	    var page1 = (<View/>);

	if(this.state.productos.length>0){


		 if(this.state.productos[this.state.tabSelected]!=undefined){
			 		setTimeout(()=>{
				 		sentGoogle=true;
			 		}, 1000);
			 		if(sentGoogle){
				 		sentGoogle = false;
					 var gf=this.state.productos[this.state.tabSelected];
					 var stype = type;
					 if(stype=='Cómputo')stype='computo';
					 if(stype=='Impresión')stype='impresion';
					 //gf.serie.largename.toUpperCase()+' '+
					 var product_name = gf.name.toUpperCase();
					 global.trackScreenView('Producto',{ tipocliente: global.user.tipocliente , tipo: stype.toLowerCase(),producto:product_name});
			 		}

		}

		var imagenes = new Array();
		for(var i=0;i<this.state.productos[this.state.tabSelected].imagenes.length;i++){
			var g = this.state.productos[this.state.tabSelected].imagenes[i];
			imagenes.push(g);
		}

			page1 = (
          <View style={{flex:1}}>
            <Swiper
            dotColor={'#707070'}
            activeDotColor={'#ccc'}

            ref={(swiper) => {this.swiper = swiper;}}
            onIndexChanged = {(index)=>{this.setState({currentSlide:index});}}
            nextButton={<EvilIcons name={'chevron-right'} size={45} color="gray"/>}
            prevButton={<EvilIcons name={'chevron-left'} size={45} color="gray"/>}
            loop={false} style={styles3.wrapper} showsButtons={false} showsPagination={true}>
				{imagenes.map(r => <View key={r.id} style={styles3.slide1}>
                <FastImage
                	  style={{width:'100%',flex:1}}
					  source={{
					    uri: r.imagen,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
              </View>)}
            </Swiper>
          </View>);

	}

    var btnBar = {flex:1,backgroundColor:'#00a1dd',alignItems:'center',justifyContent:'center'};

    var features_bar = (<View/>);

	var headerStyle = {backgroundColor:'white',width:width,paddingTop:20,paddingBottom:8,alignItems:'center' };
	var headerTitle = {textAlign:'center',color:'#707070',fontSize:21,lineHeight:20, fontWeight:'200',
		fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',paddingLeft:20,paddingRight:20}



	var headerSubTitle = {textAlign:'center',color:'#1259a1',fontSize:20,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}
    if(this.state.settingsSelected){


      features_bar = (<View style={{width:width,height:130,position:'absolute',bottom:0,opacity:1}}>
      		<FeaturesBar onViewPDF={()=>{
	      		this.props.navigation.navigate('PDFVista',{pdf:this.state.productos[this.state.tabSelected].pdf});
      		}} type={type} specs={this.state.productos[this.state.tabSelected].especificaciones} /></View>);
    }

	{features_bar}
		var title = (type=="impresion")?'HP Laser Jet Pro 400':'Serie 1000 Notebook PCs';
		var subtitle = (type=="impresion")?'Color MFP M477fdw':'con Windows Pro';
		var content1 = (<View/>)
		var specs = (<View/>)


		var boxSmall = {marginLeft:0,marginRight:0,flexDirection:'row',justifyContent:'space-between',
			alignItems:'center',backgroundColor:'transparent',width:'33.3%'};
		var textSmall = {fontSize:10,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',color:'#87898b',flex:1,marginRight:0,backgroundColor:'transparent'}

			var img_ico1 = (<View></View>);
				var img_ico2 = (<View></View>);
				var img_ico3 = (<View></View>);
				var icoStyle = {height:'100%',resizeMode:'contain', backgroundColor:'white'}


				if(producto!=undefined){
					if(producto.serie.icon1!=""){
					img_ico1 = (<Image style={icoStyle} source={{uri:producto.serie.icon1 }}/>);
					}
					if(producto.serie.icon2!=""){
						img_ico2 = (<Image style={icoStyle} source={{uri:producto.serie.icon2 }}/>);
					}
					if(producto.serie.icon3!=""){
						img_ico3 = (<Image style={icoStyle} source={{uri:producto.serie.icon3 }}/>);
					}
				}else{
					if(item.icon1!=""){
					img_ico1 = (<Image style={icoStyle} source={{uri:item.icon1 }}/>);
					}
					if(item.icon2!=""){
						img_ico2 = (<Image style={icoStyle} source={{uri:item.icon2 }}/>);
					}
					if(item.icon3!=""){
						img_ico3 = (<Image style={icoStyle} source={{uri:item.icon3 }}/>);
					}
				}


              specs = (<View style={{backgroundColor:'white',width:'100%',
            flexDirection:'row',justifyContent:'center',height:80,marginBottom:20}}>

            	<View style={{justifyContent:'center',flex:1}}>{img_ico1}</View>
            	<View style={{justifyContent:'center',flex:1,backgroundColor:'transparent'}}>{img_ico2}</View>
            	<View style={{justifyContent:'center',flex:1}}>{img_ico3}</View>

               </View>)




		var tabLabel = {textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15,paddingLeft:10,paddingRight:10}
		var tabWidth = (width/3)-10


		var tabs = (<View/>);
		var like1 = (<View/>);
		var like2 = (<View/>);
		var like3 = (<View/>);
		if(global.user.codigo!=""){
			if(this.state.productos[this.state.tabSelected]!=undefined){
				if(this.state.productos[this.state.tabSelected].employee && type=="computo"){

				like3 = (<TouchableOpacity onPress={()=>{
					var datasheet = this.state.productos[this.state.tabSelected].datasheet;
					var p = this.state.productos[this.state.tabSelected];
					
					global.trackScreenView("Assets - Botón Ingreso",{ tipocliente: global.user.tipocliente });
					navigation.navigate('Employee',{datasheet:datasheet,producto:p});

					}}
		style={{width: 44,height: 44,alignItems:'center',marginTop:8,
	              justifyContent:'center',backgroundColor:'#15509d'}}>
				  	<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'#FFF'}}
				  	source={ img.employee_ico } />
			  </TouchableOpacity>)
			}}
		}

			like2 = (<TouchableOpacity onPress={()=>{

		navigation.navigate('Comparar',{type:type,producto1:this.state.productos[this.state.tabSelected]});

		            }} style={{width: 44,height: 44,alignItems:'center',marginTop:8,
	              justifyContent:'center',backgroundColor:'#00a1dd'}}>
				  	<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'#FFF'}} source={ img.ico_comparar } />
			  		</TouchableOpacity>)

			  like1 = (<TouchableOpacity onPress={()=>{ this.onPressFavorite(); }} style={{width: 44,height: 44,alignItems:'center', justifyContent:'center',backgroundColor:'#bababa'}}>
				  	<Image style={{width:25,height:25,resizeMode:'contain',tintColor:'#FFF'}} source={(this.state.isFavorite)?img.heart_w:img.heart} />
			  		</TouchableOpacity>);


                 tabs = (<Tabs2 type={type} selectedTab={this.state.tabSelected} items={this.state.productos} onSelected={(index)=>{
	                 this.setState({tabSelected:index});
	                 currentTab = index;
	                 this.resetSwiper();
	                 this.checkLikes(index);
	             }} />);






		var boxContainerStyle = {flex:1,backgroundColor:'#ebf9ff', flexDirection:'row'};
		content1 = (

          <View style={{flex:1,width:'100%'}} >



          <View style={headerStyle}>
            <Text style={headerTitle}>{(producto!=undefined)?producto.serie.largename.toUpperCase():item.largename.toUpperCase()}</Text>

            <View style={{width:'84%',height:1,backgroundColor:'#ccc',marginTop:10}}></View>

          </View>
          <View style={{ flex:1,width:'100%',height:'100%',backgroundColor: 'white'}}>
              {tabs}
              <View style={{flex:1,width:'100%',backgroundColor:'white'}}>
              {page1}
              <View style={{position:'absolute',top:0,right:0,backgroundColor:'transparent',
	              width:45,height:150}}>
			  	{like1}
		  		{like2}
		  		{like3}
			  </View>


              </View>
          </View>
				{specs}
				{features_bar}

            </View>)


			var boxStyle = {flexBasis:'33.3%',backgroundColor:'white',flexDirection:'row'};
			 var labelStyle = {flex:1,marginLeft:10,marginRight:10,marginTop:5,marginBottom:0,height:15,textAlign:'center',fontSize:9,color:'#87898b',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
          var boxContainerStyle = {flexWrap:'wrap',width:width,backgroundColor:'white', flexDirection:'row'};



          if(this.state.accesoriosSelected){
	          var producto = this.state.productos[this.state.tabSelected];
			  if(producto.accesorios.length>0){



			content1 = (<View style={{flex:1,backgroundColor:'#fff'}}>
          	<View style={headerStyle}>
            <Text style={[headerTitle,{fontSize:16}]}> {'ACCESORIOS'}</Text>
            <View style={{width:'84%',height:1,backgroundColor:'#ccc',marginTop:10,marginBottom:20 }}></View>
          </View>
          	<View style={{flex:1,backgroundColor:'white'}}>
            <Swiper dotColor={'#707070'}
            activeDotColor={'#ccc'}  nextButton={<EvilIcons name={'chevron-right'} size={45} color="gray"/>}
            prevButton={<EvilIcons name={'chevron-left'} size={45} color="gray"/>}
            loop={false} style={[styles3.wrapper]} showsButtons={false} showsPagination={true}>


            {producto.accesorios.map(r => <View style={styles3.slide1}>
            <Text style={{textAlign:'center',color:'#707070',fontSize:22,lineHeight:21, fontWeight:'400',
				fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
				paddingLeft:50,paddingRight:50,color:'#2e95d5'}}> {r.accesorio}</Text>
            <FastImage
                	  style={{width:'100%',height:80,flex:1}}
					  source={{
					    uri: r.imagen,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
            </View>)}

            </Swiper>
            <View style={{height:70}}></View>
          </View>
          	</View>);
          	 }
          }

          if(this.state.tintaSelected){
	          var producto = this.state.productos[this.state.tabSelected];

			  if(producto.suministros.length>0){




			  content1 = (<View style={{flex:1,backgroundColor:'#fff'}}>
          	<View style={headerStyle}>
            <Text style={[headerTitle,{fontSize:16}]}> {'SUMINISTROS'}</Text>
            <View style={{width:'84%',height:1,backgroundColor:'#ccc',marginTop:10,marginBottom:20 }}></View>
          </View>
          	<View style={{flex:1,backgroundColor:'white'}}>
            <Swiper dotColor={'#707070'}
            activeDotColor={'#ccc'}  nextButton={<EvilIcons name={'chevron-right'} size={45} color="gray"/>}
            prevButton={<EvilIcons name={'chevron-left'} size={45} color="gray"/>}
            loop={false} style={[styles3.wrapper]} showsButtons={false} showsPagination={true}>


            {producto.suministros.map(r => <View style={styles3.slide1}>
            <Text style={{textAlign:'center',color:'#707070',fontSize:22,lineHeight:21, fontWeight:'400',
				fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
				paddingLeft:50,paddingRight:50,color:'#2e95d5'}}> {r.suministro}</Text>
            <FastImage
                	  style={{width:'100%',height:80,flex:1}}
					  source={{
					    uri: r.imagen,
					    headers:{ Authorization: 'someToken' },
					    priority: FastImage.PriorityNormal
					  }}
					  resizeMode={FastImage.resizeMode.contain}
					/>
            </View>)}

            </Swiper>
            <View style={{height:70}}></View>
          </View>
          	</View>);


          }

         }





		var btnPrinter = (<View/>);
		var btnAccesorios = (<View/>);
		if(type=="impresion"){

			if(this.state.productos[this.state.tabSelected]!=undefined){
			 if(this.state.productos[this.state.tabSelected].accesorios.length>0){

			btnPrinter = (<TouchableOpacity style={[btnBar,{backgroundColor:(this.state.accesoriosSelected)?'#1259a1':'#00a1dd'}]}
                onPress={()=>{ if(this.state.accesoriosSelected){
                                this.setState({accesoriosSelected:false});
                              }else{
								  this.setState({accesoriosSelected:true,
									  tintaSelected:false,settingsSelected:false});
                              }
                }}>
                	<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'white'}} source={ img.ico_print_3 } />
                </TouchableOpacity>);



                }
             }
		}
		if(type=="impresion"){

			if(this.state.productos[this.state.tabSelected]!=undefined){
				if(this.state.productos[this.state.tabSelected].suministros.length>0){
			btnAccesorios = (<TouchableOpacity style={[btnBar,{backgroundColor:(this.state.tintaSelected)?'#1259a1':'#00a1dd'}]}
                onPress={()=>{ if(this.state.tintaSelected){
                                this.setState({tintaSelected:false});
                              }else{
                               this.setState({tintaSelected:true,
	                               settingsSelected:false,accesoriosSelected:false});
                              }
                }}>
                	<Image style={{width:55,height:30,resizeMode:'contain',tintColor:'white'}} source={ img.ico_print_2 } />
                </TouchableOpacity>)
                 }
            }
         }else{
	         if(this.state.productos[this.state.tabSelected]!=undefined){
			 if(this.state.productos[this.state.tabSelected].accesorios.length>0){
	         btnAccesorios = (<TouchableOpacity style={[btnBar,{backgroundColor:(this.state.accesoriosSelected)?'#1259a1':'#00a1dd'}]}
                onPress={()=>{ if(this.state.accesoriosSelected){
                                this.setState({accesoriosSelected:false});
                              }else{
                                this.setState({accesoriosSelected:true,
	                                settingsSelected:false,tintaSelected:false});
                              }
                }}>
                	<Image style={{width:30,height:30,resizeMode:'contain',tintColor:'white'}} source={ img.ico_cd } />
                </TouchableOpacity>)
              }
             }
         }

		return (

            <View style={{flex:1,backgroundColor:'#fff'}}>

				 	<Navbar
				 	ref={navbar => this.navbar = navbar}
				 	onBack={()=>{navigation.pop()}} onLike={()=>{

					 	navigation.navigate('Favoritos');

				 	}} onSketch={()=>{ navigation.navigate('Cotizador'); }} showWinPro={(type=='computo')?true:false}
				 	onHome={()=>{navigation.navigate('Home')}}/>


            {content1}



            <View style={{width:width,height:hHeight,backgroundColor:'#00a1dd',flexDirection:'row',paddingBottom:pBottom}}>

                <TouchableOpacity style={[btnBar,{backgroundColor:(this.state.settingsSelected)?'#1259a1':'#00a1dd'}]}
                onPress={()=>{ if(this.state.settingsSelected){
                                this.setState({settingsSelected:false});
                              }else{
                                	this.setState({settingsSelected:true,accesoriosSelected:false,tintaSelected:false});

                              }
                }}>
                <Image style={{width:30,height:30,resizeMode:'contain',tintColor:'white'}} source={ (type=='computo')?img.ico_hd:img.ico_print_1 } />
                </TouchableOpacity>

                {btnAccesorios}
				{btnPrinter}

				<TouchableOpacity style={btnBar} onPress={()=>{

							if(this.state.contizacionIn){
                                this.setState({contizacionIn:false});
                                 this.onPressCotizador(false);
                              }else{
                                this.setState({contizacionIn:true});
                                 this.onPressCotizador(true);
                              }

                }}>
                <Image style={{width:30,height:30,resizeMode:'contain'}} source={ (this.state.contizacionIn)?img.ico_cot_in:img.ico_cot } />
                </TouchableOpacity>



				<View style={{width:65,borderLeftWidth:2,borderColor:'white'}}>
                <TouchableOpacity style={[btnBar,{backgroundColor:'#a53894'}]} onPress={()=>{ navigation.navigate('Buscar',{type:type,term:''}); }}>
                <Image style={{width:25,height:25,resizeMode:'contain',tintColor:'white'}} source={ img.lupa } />
                </TouchableOpacity>
                </View>

			</View>
			<Toast ref="toast"/>
			</View>)
	}
	renderItems(producto){
		var boxContainerStyle = {flexWrap:'wrap',width:width,backgroundColor:'white', flexDirection:'row'};
		var boxStyle = {flexBasis:'50%',backgroundColor:'white',flexDirection:'row'};
		var boxStyle = {flexBasis:'33.3%',backgroundColor:'white',flexDirection:'row'};
		var labelStyle = {flex:1,marginLeft:10,marginRight:10,marginTop:5,marginBottom:0,height:15,textAlign:'center',fontSize:9,color:'#87898b',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};

		var laptops = new Array();

		for(var i=0;i<producto.accesorios.length;i++){
			laptops.push({id:0,label:producto.accesorios[i].accesorio,
				item_img:producto.accesorios[i].imagen,selected:false});
		}

	   return ( <Selectable items={laptops} setAccesorio={(i)=>{this.onChangeAccesorio(i)}}/>)



	}
	onChangeAccesorio(i){
		this.setState({accesorioSelected:i});
	}
	onChangeSuministro(i){
		this.setState({suministroSelected:i});
	}
	renderItemsSuministro(producto){
		var boxContainerStyle = {flexWrap:'wrap',width:width,backgroundColor:'white', flexDirection:'row'};
		var boxStyle = {flexBasis:'50%',backgroundColor:'white',flexDirection:'row'};
		var boxStyle = {flexBasis:'33.3%',backgroundColor:'white',flexDirection:'row'};
		var labelStyle = {flex:1,marginLeft:10,marginRight:10,marginTop:5,marginBottom:0,height:15,textAlign:'center',fontSize:9,color:'#87898b',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};

		var laptops = new Array();

		for(var i=0;i<producto.suministros.length;i++){
			laptops.push({id:0,label:producto.suministros[i].suministro,
				item_img:producto.suministros[i].imagen,selected:false});
		}

	   return ( <Selectable items={laptops} setAccesorio={(i)=>{this.onChangeSuministro(i)}}/>)
	}
}

/**/
var userAction=false;
class Tabs2 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selected : this.props.selectedTab
		}
	}
	update(){
		
	}
	getColor(selectedTab,id,itemid){
		 if(this.props.type=="impresion"){
		            for(var i=0;i<global.featurePrint.length;i++){
			            var idproducto = Number(global.featurePrint[i].idproducto);
						if(idproducto!=0){
							if(Number(itemid)==idproducto){
								return '#a53894';
							}
						}
					}
	    }else{
		            for(var i=0;i<global.featureComp.length;i++){
			            var idproducto = Number(global.featureComp[i].idproducto);
						if(idproducto!=0){
							if(Number(itemid)==idproducto){
								return '#a53894';
							}
						}
					}
	    }
	            
	            
		if(this.props.selectedTab!=id){
			return '#707070';
		}else{
			return '#3392d0'; //AZUL
		}
		//return "red";
	}
	render() {
		if(!userAction){
			if(this.props.selectedTab>2){
				setTimeout(()=>{
					var spaces = this.props.selectedTab-2;
					var wt = ((width-20)/3)*spaces;
					userAction = false;
					this.refs._scrollView.scrollTo({x:wt,animated: false});
				}, 10)
			}
		}
		userAction = false;
		var tabStyle = {width:(width-20)/3,height:50,justifyContent:'center'}
		var tabLabel = {textAlign:'center',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:14,lineHeight:14,paddingLeft:10,paddingRight:10}
		var labelStyle = {flex:1,marginLeft:10,marginRight:10,marginTop:5,marginBottom:0,height:15,textAlign:'center',fontSize:9,color:'#87898b',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		
		return (<View style={{width:'100%',height:60,
			justifyContent:'center',alignItems:'center'}}>

				<View style={{width:'95%',height:60,
					alignItems:'center',justifyContent:'center'}}>
		  		<ScrollView ref='_scrollView' bounces={false} horizontal={true} style={{flexDirection:'row',backgroundColor:'white'}}>
			{
				this.props.items.map( ( item, id ) =>{
					return (<TouchableHighlight
							underlayColor={'#edefef'}
					        key={id}
					        style={[tabStyle,
					        	{backgroundColor:'transparent'},
					        	((this.props.selectedTab==id)?{backgroundColor:'transparent'}:null)
					        ]}
									onPress={ () => {
										userAction = true;
										this.setState({ selected:id })
										this.props.onSelected(id);
									}}>
				                <View style={{flex:1,alignItems: "center", justifyContent: "center"}}>
									<Text style={[tabLabel,{color:this.getColor(this.props.selectedTab,id,item.id),fontWeight:(this.props.selectedTab!=id)?'200':'400'}]}>{item.name.toUpperCase()}</Text>
									<View style={{width:(this.props.selectedTab!=id)?0:45, height:3, backgroundColor:this.getColor(this.props.selectedTab,id,item.id),marginTop:3 }}></View>
				                </View>

					</TouchableHighlight>)
				})
			}
			</ScrollView></View></View>)
	}
}

class Tabs extends Component {
    onSelect(el){
        if (el.props.onSelect) {
            el.props.onSelect(el);
        } else if (this.props.onSelect) {
            this.props.onSelect(el);
        }
    }

    render(){
        const self = this;
        let selected = this.props.selected
        if (!selected){
            React.Children.forEach(this.props.children.filter(c=>c), el=>{
                if (!selected || el.props.initial){
                    selected = el.props.name || el.key;
                }
            });
        }
        return (
            <View style={[styles.tabbarView, this.props.style]}>
                {React.Children.map(this.props.children.filter(c=>c),(el)=>
                    <TouchableOpacity key={el.props.name+"touch"}
                       style={[styles.iconView, this.props.iconStyle, (el.props.name || el.key) == selected ? this.props.selectedIconStyle || el.props.selectedIconStyle || {} : {} ]}
                       onPress={()=>!self.props.locked && self.onSelect(el)}
                       onLongPress={()=>self.onSelect(el)}
                       activeOpacity={el.props.pressOpacity}>
                         {selected == (el.props.name || el.key) ? React.cloneElement(el, {selected: true, style: [el.props.style, this.props.selectedStyle, el.props.selectedStyle]}) : el}
                    </TouchableOpacity>
                )}
            </View>
        );
    }
}


var styles = StyleSheet.create({
    tabbarView: {
        backgroundColor:'white',
        flexDirection: 'row',
        width:'100%',
        height:45,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconView: {

    }
});

const styles2 = StyleSheet.create({
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


const styles3 = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  }
})

export default ItemProfile
