import React, { Component } from "react";
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Platform,
  Dimensions,
  ScrollView,
  TextInput,
  View,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';
import { FeaturesBar } from "@components";
import { Navbar } from "@components";
import { BottomBar } from "@components";
import { img } from "@media";
import Dash from 'react-native-dash';
import Reactotron from 'reactotron-react-native'
import FastImage from 'react-native-fast-image';
import EvilIcons from 'react-native-vector-icons/EvilIcons';


var parseString = require('react-native-xml2js').parseString;
var {height, width} = Dimensions.get('window');
var scope = this;

class EncuentraLaTinta extends Component {
	constructor(props){
		super(props);
		scope = this;
		global.screenEncuentraLaTinta = this;
		this.state = {page:1,data:global.user.EncuentraLaTinta,resultados:[],currentTab:0,key:'',loading:true,placeholder:"Por Ejemplo HP 507A Magenta"}
		global.trackScreenView("Guía de compatibilidad",{ tipocliente: global.user.tipocliente}); 
	            	
	}
	updateNav(){
		this.navbar.update();
	}
	async obtenerResultados( ){
		
		var url = global.api+'/ProductoSuministros_Listado';
		url += "?busqueda=" + encodeURIComponent(this.state.key);
		if(this.state.currentTab==0){
			url += "&int_IdProducto=0";
		}else{
			url += "&int_IdProducto=1";
		}
		
		let response1 = await fetch(url,{method: "GET"});
		let responseText1 = await response1.text();
		var items = new Array();
		scope.setState({resultados:[],loading:true});
		
		parseString(responseText1, function (err, result) {
			if( result.string._ !=undefined ){
				var resp1 = JSON.parse(result.string._);
				Reactotron.log('------------------');
				Reactotron.log(resp1);
				var response = new Array();
				for(var i=0;i<resp1[0].suministros.length;i++){
					var item = {};
					item.id = resp1[0].suministros[i].id;
					item.suministro = resp1[0].suministros[i].suministro;
					item.name = resp1[0].suministros[i].suministro;
					item.imagen = resp1[0].suministros[i].imagen;
					item.rendimiento = resp1[0].suministros[i].rendimiento;
					item.color = resp1[0].suministros[i].color;
					response.push(item);
				}
				Reactotron.log(response);
				scope.setState({resultados:response});
				Reactotron.log('------------------');
			}else{
				Reactotron.log('------SIN RESULTADOS-----------');
			}
			scope.setState({loading:false});
		});
	}
	
	
	
	onPressCotizador(r){
	  var {navigation} = this.props;
	  this.addCotizador(r);
	  navigation.navigate('Cotizador');
  }
  addCotizador(producto){
	  var exist = false;
	  for(var i=0;i<global.user.cotizador.length;i++){
		  if(global.user.cotizador[i].id == producto.id){
			  exist = true;
		  }
	  }
	  if(!exist){
		  global.user.cotizador.push(producto);
	  }
	  global.saveUser();
	  this.navbar.update();
  }

  	componentDidMount(){
	  	this.obtenerResultados();
  	}	
	render() {

		 var {navigation} = this.props;
		 var txtbase = {color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15}
		var icoSize = 60;
    var boxContainerStyle = {width:'100%',backgroundColor:'white', flexDirection:'column'};
    var boxStyle = {flex:1,backgroundColor:'transparent',flexDirection:'row'};
    var labelStyle = {flex:1,textAlign:'center',fontSize:11,color:'#1259a1',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
    
				
	 	/*
		 	<View style={{width:17,height:17,borderRadius:17/2,borderColor:'#0096d6',borderWidth:1,marginRight:5}}>
							<Material name={'check'} size={16} color="#0096d6"/>
						</View>
						*/
		return (<View style={{flex:1}} >
        			<Navbar ref={navbar => this.navbar = navbar} onBack={()=>{navigation.pop();}}  onSketch={()=>{ navigation.navigate('Cotizador'); }}  onHome={()=>{navigation.navigate('Home')}}/>
        			<View style={{flex:1,backgroundColor:'white',padding:0 }}>
        				
        				
        				<View style={{width:'100%',height:90,backgroundColor:'#124f97',justifyContent:'center',alignItems:'center'}}>
        				
        				<Text style={{color:'white',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:22}}>{'GUÍA DE COMPATIBILIDAD'}</Text>
        				
        				</View>
        				
        				
        				<View style={{alignItems:'center',backgroundColor:'white',marginBottom:20}}>
			            <View style={{width:'80%',height:45,marginTop:10,backgroundColor:'white',flexDirection:'row'}}>
			            	<TouchableOpacity onPress={()=>{
				            	
				            	this.setState({currentTab:0,placeholder:"Por Ejemplo HP 507A Magenta",resultados:[],loading:false,key:''});
				            	//scope.setState({resultados:[],loading:false});
			            	}} style={{flex:1,backgroundColor:(this.state.currentTab==0)?'#0096d6':'#e1e4e5',justifyContent:'center',alignItems:'center'}}>
			            		<Text style={[txtbase,{color:(this.state.currentTab==0)?'#fff':'#000'}]}>{'Busca por suministro'}</Text>
			            	</TouchableOpacity>
			            	<TouchableOpacity onPress={()=>{
				            	this.setState({currentTab:1,placeholder:"Por Ejemplo HP Deskjet 5160",resultados:[],loading:false,key:''});
				            	//scope.setState({resultados:[],loading:false,key:''});
				            	
				            	
				            	
			            	}} style={{flex:1,backgroundColor:(this.state.currentTab==1)?'#0096d6':'#e1e4e5',justifyContent:'center',alignItems:'center'}}>
			            		<Text style={[txtbase,{color:(this.state.currentTab==1)?'#fff':'#000'}]}>{'Busca por impresora'}</Text>
			            	</TouchableOpacity>
			            </View>
			            
			            <View style={{width:'80%',height:45,marginTop:10,backgroundColor:'transparent',flexDirection:'row'}}>
			            	
			            	<TextInput style={{flex:1,backgroundColor: "#eef2f2", padding:10,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}
							underlineColorAndroid='transparent'
							keyboardAppearance="light"
							returnKeyType="search"
							value={this.state.key}
							onChangeText={(key) => this.setState({key})}
							placeholder={this.state.placeholder} />
							<TouchableOpacity onPress={()=>{
								this.obtenerResultados();
							}} style={{width:45,height:45,backgroundColor: "#9b3189",justifyContent:'center',alignItems:'center'}}>
								<Image style={{width:20,height:20,resizeMode:'contain',tintColor:'#fff'}} source={ img.lupa } />
							</TouchableOpacity>
							
			            </View>
		            </View>
						
						
						
					 	
					 	<View style={{flex:1,padding:0,paddingBottom:0,backgroundColor:'transparent' }}>
					 	
					 							 
						 
						 <ScrollView style={{flex:1}}>
    				  <View style={boxContainerStyle}>

					  
              
				
				
    			{scope.state.resultados.length>0 ? (
	    			
	    			<View style={{flex:1}}>
						{scope.state.resultados.map(r => <View key={r.id} style={boxStyle}>
                <View style={{flex:1, alignItems:'center',backgroundColor:'transparent',flexDirection:'column'}}>
               
               
    				<View style={{width:'82%',backgroundColor:'transparent',flexDirection:'row',paddingBottom:10}}>
    				
    						
    				<View style={{width:100,height:100,marginRight:10}}>
	    				
	    				<FastImage style={{width:100,height:80,marginTop:0}} source={{
						    uri: r.imagen,
						    headers:{ Authorization: 'someToken' },
						    priority: FastImage.PriorityNormal
						  }} resizeMode={FastImage.resizeMode.contain} />
						  
						  
						  <TouchableOpacity onPress={()=>{
						  	this.onPressCotizador(r);  
						}} style={{backgroundColor:'transparent',width:100,height:22,marginTop:0,justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
						
						<Text style={{marginBottom:0,fontSize:15,fontWeight:'200',
					color:'#0096d6',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Cotizar'}</Text>
					
					</TouchableOpacity>
					
					
    				</View>		
    				
    				<View style={{width:160,height:100,flexDirection:'column',backgroundColor:'transparent'}}>
    				
    				<Text style={{marginTop:10,fontSize:14, fontWeight:'400',
	                color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.suministro}</Text>
	                
	                {(r.rendimiento!='') &&
		                <View>
		                	<Text style={{marginTop:2,fontSize:14, fontWeight:'400',
	                color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{'Rendimiento de páginas*'}</Text>
	                
	                		<Text style={{marginTop:0,fontSize:14, fontWeight:'400',
	                color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>{r.rendimiento}</Text>
		                </View>
		            }
	                
	                
	                

    				</View>	
    				<View style={{backgroundColor:'transparent'}}>
    					{	r.color!='' &&
	    					<Image style={{width:23,height:23,resizeMode:'contain',marginTop:15,tintColor:r.color}} source={ img.gota } />	
    					}
    						
    				</View>		
				</View>
				<View style={{width:'80%',backgroundColor:'#999',height:0.5}}></View>
				</View>
				</View>)}
					</View>
						
    				) : (
					<View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
						{scope.state.loading==false &&
							<Text style={{marginBottom:0,fontSize:14,fontWeight:'200',color:'#000',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'}}>
						{'No se encontraron resultados'}</Text>
						}
						
					</View>
				)}	
    					
    					
    					
    					
    					


    					</View>
    				</ScrollView>
						 

						 
						
						</View>
						 	
						
				 	</View>
			</View>)
	}
}

export default EncuentraLaTinta
