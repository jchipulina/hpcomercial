import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
} from 'react-native';
import { connect } from "react-redux";
import { Inicio } from "@components";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Material from 'react-native-vector-icons/MaterialCommunityIcons';

import { Search } from "@containers";
import { Navbar } from "@components";

class Cuestionario extends Component {
	
	constructor(props){
		super(props);
		global.trackScreenView("Cuestionario",{ tipocliente: global.user.tipocliente}); 
	}
	render() {
    	var {navigation} = this.props;
		var icoSize = 60;
		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'white'}} >

				 	<View style={{flex:1}} >
				 	<Navbar onBack={()=>{ navigation.pop() }} onHome={()=>{navigation.navigate('Home')}}/>
					 	<Search/>



				 	</View>

			</View>)
	}
}

export default Cuestionario
