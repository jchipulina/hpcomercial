import React, { Component } from "react";
import {
  View,
  Text
} from 'react-native';
import { Cover } from "@containers";
import Reactotron from 'reactotron-react-native';


export default class CoverApp extends Component {
	
	constructor(props){
		super(props);
		if(global.user!=null){
			global.trackScreenView("Cover",{ tipocliente: global.user.tipocliente });
		}else{
			global.trackScreenView("Cover");
		}
	}
	componentWillMount() {
	
	}
	
	render() {
    	var {navigation} = this.props;
		return ( <Cover style={{flex:1}} onNext={()=>{
			navigation.navigate((global.user==null)?'Registry':'Home') 
		}}
		onNav1={()=>{ navigation.navigate('Cotizador') }} 
		onNav2={()=>{ navigation.navigate('CarePacks') }}
		onNav3={()=>{ navigation.navigate('Question') }}
		>
		 </Cover>);

	}
}
