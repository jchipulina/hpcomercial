import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform
} from 'react-native';
import { Img } from "@media"
import { img } from "@media"
import { Categories } from "@containers";
import { Navbar,BottomBar } from "@components";
import Icon from 'react-native-vector-icons/FontAwesome';
export default class SuministroScreen extends Component {
	
	constructor(props){
		super(props);
		this.state = {showButtonUp:false}
		global.trackScreenView("Interior Suministros",{ tipocliente: global.user.tipocliente}); 
	}
	render() {
    	var {navigation} = this.props;
		var params = this.props.navigation.state.params;
		//params = {section:12,title:'ALGO'};
		
		var topimages  = [
			{top:img.sumi_top_1},
			{top:img.sumi_top_2},
			{top:img.sumi_top_3},
			{top:img.sumi_top_4},
			{top:img.sumi_top_5},
			{top:img.sumi_top_6},
			{top:img.sumi_top_7},
			{top:img.sumi_top_8},
			{top:img.sumi_top_9},
			{top:img.sumi_top_10},
			{top:img.sumi_top_11},
			{top:img.sumi_top_12},
			]
		
		
		var styleContainer = {flex:1,width:'100%',padding:30,paddingTop:20,backgroundColor:'white' };
		var mediumText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:17,lineHeight:18,fontWeight:Platform.OS === 'ios' ? '300' : ''}
		
		var smallText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:15,lineHeight:15,fontWeight:Platform.OS === 'ios' ? '300' : ''}
		
		
		var disclaimerText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',fontSize:11,lineHeight:14,color:'#666',fontWeight:Platform.OS === 'ios' ? '200' : ''};
		var disclaimer = {paddingTop:20};  
		var titleText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',marginTop:10,marginBottom:10,color:'black'}
		var fontText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg'};
		
		
		var strongText = {fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',
			fontSize:16,fontWeight:Platform.OS === 'ios' ? '400' : '',marginTop:10,marginBottom:0,color:'#000'}
		
		var JSXContent = (<View></View>);
		if(params.section==1){
			JSXContent = (<View style={styleContainer}>
				<Text style={mediumText}>
					El uso de cartuchos remanufacturados, rellenados o falsificados puede complicar tu jornada laboral a causa de las dificultades que generan y la calidad de impresión deficiente que ofrecen.
					</Text>
<Text style={titleText}>
Beneficios de los cartuchos de Tóner Originales HP:
</Text>




	<View style={{flexDirection:'row'}}>
		<View style={{flex:1}}>
		<Image style={{width:'100%',height:40,resizeMode:'contain',margin:5}} source={ img.ico_sumi_4 } />
		<Text style={[fontText,{textAlign:'center',color:'#666',fontWeight: '200'}]}>
		Mejor calidad de impresión y mayor confiabilidad</Text>
		</View>
		<View style={{flex:1}}>
		<Image style={{width:'100%',height:40,resizeMode:'contain',margin:5}} source={ img.ico_sumi_5 } />
		<Text style={[fontText,{textAlign:'center',color:'#666',fontWeight: '200'}]}>
		Menor tiempo de mantenimiento</Text>
		</View>
		<View style={{flex:1}}>
		<Image style={{width:'100%',height:40,resizeMode:'contain',margin:5}} source={ img.ico_sumi_3 } />
		<Text style={[fontText,{textAlign:'center',color:'#666',fontWeight: '200'}]}>
		Mayor vida útil de tu impresora</Text>
		</View>
	</View>
	
	
	<View style={{flex:1,marginTop:20,marginBottom:10}}>
		<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
	</View>
	<View style={{flex:1}}>
	
	<View style={{flex:1,flexDirection:'row'}}>
			<View style={{width:50}}>
			
				</View>
				<View style={{flex:1}}>
				<Text style={titleText}>
					Además:
			</Text>
			</View>
		</View>
		

			<View style={{flex:1,flexDirection:'row',marginBottom:10}}>
			<View style={{width:50}}>
			<Image style={{width:50,height:30,resizeMode:'contain'}} source={ img.checksumi } />
				</View>
				<View style={{flex:1}}>
				<Text style={mediumText}>
	HP ofrece resultados de impresión profesionales, los cartuchos de tóner HP suministran páginas aceptables para todos los usos el 95% de las veces en comparación con el 38% de los cartuchos de otras marcas.(1)
			</Text>
			</View>
		</View>
		

		<View style={{flex:1,flexDirection:'row',marginBottom:10}}>
			<View style={{width:50}}>
			<Image style={{width:50,height:30,resizeMode:'contain'}} source={ img.checksumi } />
				</View>
				<View style={{flex:1}}>
				<Text style={mediumText}>
	De los cartuchos de otras marcas probados, casi el 40% no funcionó o falló prematuramente.(1)
			</Text>
			</View>
		</View>
		
		<View style={{flex:1,flexDirection:'row',marginBottom:10}}>
				<View style={{width:50}}>
				
				<Image style={{width:50,height:30,resizeMode:'contain'}} source={ img.checksumi } />
				
				</View>
				<View style={{flex:1}}>
				<Text style={mediumText}>
	Los cartuchos de tóner HP LaserJet originales funcionan desde la primera vez, todas las veces.(1)
			</Text>
			</View>
		</View>
		
	</View>
				<Image style={{width:'100%',height:100,resizeMode:'contain',margin:5}} source={ img.lasertjet_cartucho } />
				
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				1 Un estudio de 2016 de SpencerLab, encargado por HP, comparó los cartuchos originales HP con cinco marcas de cartuchos vendidas en América Latina (AR, BR, CL, CO, MX y PE)
para las impresoras HP Pro M127 y Pro 400, cartuchos HP 83A y 80A. Para conocer más detalles, consulta http://www.spencerlab.com/reports/HPReliability-LA-2016.pdf
				</Text>
				 </View>
				
				</View>)
		}
		if(params.section==2){
			JSXContent = (<View style={styleContainer}>
				<Text style={mediumText}>
					<Text style={{fontWeight:Platform.OS === 'ios' ? '400' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>Cartuchos </Text>
					<Text style={{fontWeight:Platform.OS === 'ios' ? '600' : '',color:'#0096d6',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>originales</Text>{"\n"}
Hasta el 70% de la tecnología de impresión
está en el cartucho original HP(2), protegido mediante patentes.
					</Text>
<View style={{flex:1,marginTop:20,marginBottom:25}}>
		<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
	</View>

	<View style={{flexDirection:'row'}}>
		<View style={{width:100}}>
			<Image style={{width:90,height:40,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_6 } />
		</View>
		<View style={{flex:1,marginBottom:20}}>
		<Text style={[fontText,{textAlign:'left',color:'#000',fontWeight: '200'}]}>
		DESVENTAJAS DE LOS CARTUCHOS DE OTRAS MARCAS <Text style={{color:'#0096d6'}}>VS</Text> LOS CARTUCHOS ORIGINALES HP</Text>
		</View>
	</View>
	
	
	
	<View style={{flex:1}}>
	
			
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>1</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					No tienen el conocimiento ni la experiencia de HP en las impresoras HP
				</Text>
			</View>
		</View>
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>2</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					No tienen el tóner ni los componentes de HP
				</Text>
			</View>
		</View>
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>3</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					No poseen el estándar de calidad en la fabricación de HP
				</Text>
			</View>
		</View>
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>4</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					No son confiables como los son los cartuchos HP
				</Text>
			</View>
		</View>
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>5</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					Sin garantía y soporte de HP
				</Text>
			</View>
		</View>
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>6</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					Sin la calidad de impresión de HP
				</Text>
			</View>
		</View>
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>7</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					Sin la disponibilidad de los productos HP
				</Text>
			</View>
		</View>
		
		
		<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5}}>
			<View style={{width:20,backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#00c7cb',fontSize:20,fontWeight:'600' }]}>8</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText]}>
					Sin la variedad de productos HP para distintas necesidades
				</Text>
			</View>
		</View>
		
	</View>

				<View style={disclaimer}>
				<Text style={disclaimerText}>
				2 Con base en los cartuchos Todo-en-Uno a color y monocromáticos HP originales y en los pasos del proceso de electrofotografía necesarios para imprimir una página.
				</Text>
				 </View>
				
				</View>)
		}
		
		if(params.section==3){
			JSXContent = (<View style={styleContainer}>
				<Text style={mediumText}>
				Los cartuchos originales HP mantienen el equipo funcionando sin inconvenientes,
con increíbles resultados que son avalados por quienes más entienden de impresoras.
				</Text>
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:80}}>
					
					
					<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
					<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_7 } />
					</View>
					
					</View>
					
					<View style={{flex:1}}>
						<Text style={[smallText,{color:'#666'}]}>
						81% de los técnicos en impresoras recomienda los cartuchos originales HP en lugar de otros cartuchos.(3)
						</Text>
					</View>
				</View>
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:80}}>
					<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
					<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_1 } />
					</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[smallText,{color:'#666'}]}>
						Los cartuchos de tóner originales HP son los más confiables.
						</Text>
					</View>
				</View>
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:80}}>
					
					
					<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_5 } />
						</View>
						
						
					
					
					</View>
					
					<View style={{flex:1}}>
						<Text style={[smallText,{color:'#666'}]}>
						Los cartuchos de tóner originales HP requieren menos servicio.
						</Text>
					</View>
				</View>
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<Text style={strongText}>
					9 DE CADA 10 TÉCNICOS OPINAN QUE USAR CARTUCHOS DE TÓNER ORIGINALES HP EN LUGAR DE CARTUCHOS DE TÓNER DE OTRAS MARCAS ES ESENCIAL PARA MINIMIZAR LAS VISITAS DE SERVICIO DE LAS IMPRESORAS HP.
				</Text>
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				3 Con base en los cartuchos Todo-en-Uno a color y monocromáticos HP originales y en los pasos del proceso de electrofotografía necesarios para imprimir una página. Un estudio de 2015 de LA Market Strategies International, encargado por HP. Resultados basados en 317 encuestas de Socios HP ServiceOne que tienen al menos 6 meses de experiencia en el mantenimiento de impresoras LaserJet de HP monocromo y color con cartuchos de tóner de HP y de otras marcas instalados y que lo han hecho dentro de los 12 meses anteriores al estudio. El estudio fue realizado en 6 países: AR, BR, CL, CO, MX y PE. Para conocer más detalles, consulta marketstrategies.com/hp/LA-Technician2015.pdf
4 Un estudio de 2015 de LA Market Strategies International, encargado por HP. Resultados basados en 317 encuestas de Socios HP ServiceOne que tienen al menos 6 meses de experiencia en el mantenimiento de impresoras LaserJet de HP monocromo y color con cartuchos de tóner de HP y de otras marcas instalados y que lo han hecho dentro de los 12 meses anteriores al estudio. El estudio fue realizado en 6 países: AR, BR, CL, CO, MX y PE.  Para conocer más detalles, consulta marketstrategies.com/hp/LA-Technician2015.pdf
				</Text>
				 </View>
	
				
				</View>);
		}
		
		
		
		if(params.section==4){
			JSXContent = (<View style={styleContainer}>
				<Text style={[strongText,{marginTop:0,paddingTop:0}]}>
				<Text style={{color:'#0096d6',fontWeight:'600'}}>4 razones</Text> para decidirse por los cartuchos originales HP
				</Text>
				
				<View style={{flexDirection:'row',marginTop:20}}>
				
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_9 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Valor</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Utilizar cartuchos originales HP evita costos ocultos, como desperdicio de papel, daño a las impresoras, reemplazo o mantenimiento frecuente.
						</Text>
					</View>
				</View>
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_10 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Confiabilidad</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Con los cartuchos originales HP, la vida útil de las impresoras se incrementa. Obtendrás una calidad de impresión uniforme y consistente.
						</Text>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_11 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Liderazgo medioambiental</Text>
						<Text style={[smallText,{color:'#666'}]}>
						HP diseña sus cartuchos pensando
en el medio ambiente. Los clientes tienen acceso a un programa de reciclaje con más de veinticinco años de implementación.
						</Text>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_7 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Calidad</Text>
						<Text style={[smallText,{color:'#666'}]}>
						HP proporciona resultados de impresión profesionales.
						</Text>
					</View>
				</View>
				
					
				
				</View>);
		}
		
		
		
		if(params.section==5){
			JSXContent = (<View style={styleContainer}>

				<View style={{flexDirection:'row',marginTop:0}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_14 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Calidad</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Los cartuchos falsificados pueden generar impresiones inconsistentes o de mala calidad
						</Text>
					</View>
				</View>
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_13 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Precio</Text>
						<Text style={[smallText,{color:'#666'}]}>
						HP no liquida consumibles de impresión ni vende “artículos de segunda mano”. Un precio muy por debajo del promedio en el mercado podría ser señal de un cartucho falsificado
						</Text>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_12 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Caja del cartucho</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Si la caja del cartucho es diferente a la normal, parece haber sido abierta o el sello violado, podría tratarse de productos falsificados</Text>
					</View>
				</View>
				
				
				<View style={{flexDirection:'row',marginTop:20}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_15 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Sello de seguridad </Text>
						<Text style={[smallText,{color:'#666'}]}>
						HP integra en las cajas un sello de seguridad para la verificación de originalidad, el cual cuenta con un holograma y un código QR a través del cual se puede comprobar que el cartucho sea original
						</Text>
					</View>
				</View>
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<Text style={[titleText]}>Distintivos de los cartuchos{"\n"}Originales HP</Text>
				<Text style={[mediumText]}>Revisa las cajas de los Cartuchos de Tóner Originales HP y descubre todos los elementos que los hacen únicos y que demuestren la calidad que tienen.</Text>
				<Image style={{width:'100%',height:130,resizeMode:'cover',marginTop:10,marginBottom:5}} source={ img.sumi_paso4 } />
				
					
				
				</View>);
		}
		
		
		if(params.section==6){
			JSXContent = (<View style={styleContainer}>

				
				<Text style={[titleText]}>Distintivos de los cartuchos{"\n"}originales HP</Text>
				<Text style={[mediumText]}>Para verificar si el sello es original, gira la caja de un lado al otro y asegúrate de que los dos tonos de azul intercambien su lugar. Durante el periodo de transición algunos productos continuarán portando el sello de seguridad anterior. Este es el nuevo sello que viene en la caja de tóner, aprende cómo identificar la nueva etiqueta:</Text>
				
				<View style={{flex:1,width:'100%',backgroundColor:'#f4f7f7',padding:10,marginTop:10}}>
					<Text style={[titleText,{marginBottom:0,marginTop:0}]}>Paso 1</Text>
					<Text style={[titleText,{color:'#0096d6',marginTop:0}]}>Verifica visualmente la etiqueta</Text>
					
					<Image style={{width:'100%',height:130,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.sumi_paso1 } />
					
					<Text style={[smallText,{color:'#666'}]}>La nueva etiqueta de HP Mobile Authentication presenta propiedades holográficas de cambio de imágenes además del código QR.</Text>
				</View>
				
				
				<View style={{flex:1,width:'100%',backgroundColor:'#FFF',padding:10,marginTop:10}}>
					<Text style={[titleText,{marginBottom:0,marginTop:0}]}>Paso 2</Text>
					<Text style={[titleText,{color:'#0096d6',marginTop:0}]}>Ve el código</Text>
					<Image style={{width:'100%',height:130,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.sumi_paso2 } />
					<Text style={[smallText,{color:'#666'}]}>Si el paquete incluye una nueva etiqueta de Mobile Authentication, utiliza el lector de código QR de tu smartphone para ver el código de seguridad (1).
</Text>
				</View>
				
				
				<View style={{flex:1,width:'100%',backgroundColor:'#f4f7f7',padding:10,marginTop:10}}>
					<Text style={[titleText,{marginBottom:0,marginTop:0}]}>Paso 3</Text>
					<Text style={[titleText,{color:'#0096d6',marginTop:0}]}>Ve la respuesta de HP</Text>
					<Image style={{width:'100%',height:130,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.sumi_paso3 } />
					<Text style={[smallText,{color:'#666'}]}>Una respuesta válida indica que el cartucho es genuino, una respuesta inválida indica que el cartucho no es original. Si tu smartphone no tiene un lector de código QR, puedes validar la nueva etiqueta de seguridad en línea. Sólo ve al URL en la etiqueta de seguridad nueva e ingresa el número de serie de la etiqueta.(1)</Text>
				</View>
				
				
				<View style={{flex:1,width:'100%',backgroundColor:'#FFF',padding:10,marginTop:10}}>
				
				<Image style={{width:'100%',height:130,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.sumi_paso4 } />
				
					<Text style={[titleText,{marginBottom:10,marginTop:0}]}>ESCANEA EL CÓDIGO. PROTEGE TUS IMPRESORAS.</Text>
					<Text style={[smallText,{color:'#666'}]}>Los consumibles falsificados no sólo te cuestan tiempo y dinero por las costosas reimpresiones que exigen; también limitan la garantía de tu equipo y pueden arruinar tus impresoras.{"\n"}{"\n"}Para más información visita el sitio {'\n'}<Text style={{color:'#0096d6',textDecorationLine: "underline", textDecorationStyle: "solid", textDecorationColor: "#0096d6"}}>hp.com/la/antipirateria</Text></Text>
				</View>
			
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				1 No todos los cartuchos de tinta en América del Norte y Europa Occidental tiene etiquetas de seguridad. Pueden corresponder tarifas estándar.
				</Text>
				 </View>
				 
				 
				</View>);
		}
		
		
		if(params.section==7){
			JSXContent = (<View style={styleContainer}>

				<View style={{flexDirection:'row',marginBottom:10}}>
					<View style={{width:50,height:50,justifyContent:'flex-start',alignItems:'center',backgroundColor:'transparent',marginRight:10}}>
					<Image style={{width:50,height:50,resizeMode:'contain'}} source={ img.ico_sumi_4 } />
					</View>
					<View style={{flex:1,justifyContent:'center'}}>
						<Text style={[titleText,{fontWeight:'400'}]}>Calidad 
						<Text style={{color:'#0096d6'}}> de impresión</Text></Text>
					</View>
				</View>
				
				<Text style={mediumText}>
				Los técnicos de impresoras lo confirman: el 40% de los problemas causados por cartuchos de tóner de otras marcas se debe a fallas en la calidad de la impresión como rayas, manchas o puntos.(5)
{"\n"}{"\n"}
La calidad de impresión de los cartuchos de tóner originales HP proporciona páginas que son aceptables para todos los usos al menos el 95% de las veces, comparado con el 38% de otras marcas. La calidad de impresión uniforme y confiable significa menos frustración y menos reimpresiones con los cartuchos de tóner originales HP.(6)</Text>
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<View style={{flexDirection:'row',marginBottom:10}}>
					<View style={{width:50,height:50,justifyContent:'flex-start',alignItems:'center',backgroundColor:'transparent',marginRight:10}}>
					<Image style={{width:50,height:50,resizeMode:'contain'}} source={ img.ico_sumi_8 } />
					</View>
					<View style={{flex:1,justifyContent:'center'}}>
						<Text style={[titleText,{fontWeight:'400'}]}>Calidad 
						<Text style={{color:'#0096d6'}}> de impresión</Text></Text>
					</View>
				</View>
				
				<Text style={mediumText}>Los cartuchos de tóner originales HP probados produjeron, en promedio, 27% más páginas utilizables que los cartuchos de otras marcas.(7)
{"\n"}{"\n"}
Además, HP cuenta con ISO/IEC 19752 y 19798, los estándares del sector para medir los rendimientos de páginas de los cartuchos de tóner monocromático y a color, con declaraciones de rendimiento comprobadas en diversos estudios.
{"\n"}{"\n"}
Muchos competidores del mercado, a diferencia de HP, no cuentan con estándares industriales de objetivos de cartuchos de tóner para medir el rendimiento de páginas, lo que hace imposible comprobar su funcionamiento y compararlos con los cartuchos originales HP.</Text>
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				5 Un estudio de LA de 2015 realizado por Market Strategies International, encargado por HP. Resultados basados en 317 encuestas de Socios HP ServiceOne que tienen al menos 6 meses de experiencia en el mantenimiento de impresoras monocromáticas y a color LaserJet de HP con cartuchos de tóner HP y de otros proveedores instalados, y que lo han hecho dentro de los 12 meses anteriores al estudio. El estudio se realizó en 6 países: AR, BR, CL, CO, MX y PE. Para conocer más detalles, consulta marketstrategies.com/hp/LA-Technician2015.pdf
6 Un estudio de confiabilidad de consumibles monocromáticos de 2016 realizado por SpencerLab, encargado por HP, comparó cartuchos originales HP con otras 5 marcas vendidas en América Latina (AR, BR, CL, CO, MX y PE) para las impresoras HP Pro M127 y Pro 400, los cartuchos HP 83A y 80A. Para conocer más detalles, consulta http://www.spencerlab.com/reports/HPReliability-LA-2016.pdf
7 Un estudio de SpencerLab de 2016, encargado por HP, comparó los cartuchos originales HP con cinco marcas de cartuchos vendidas en América Latina (AR, BR, CL, CO, MX y PE) para las impresoras HP Pro M127 y Pro 400, cartuchos HP 83A y 80A. Para conocer más detalles, consulta http://www.spencerlab.com/reports/HPReliability-LA-2016.pdf
				</Text>
				 </View>
				 
				 
				</View>);
		}
		
		
		if(params.section==8){
			JSXContent = (<View style={styleContainer}>
			
			 
			<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',marginBottom:5}}>
			<View style={{backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#000',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginBottom:5}]}>MITO 01</Text>
			</View> 
			 
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}> 
				<Text style={[smallText,{fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontWeight:Platform.OS === 'ios' ? '200' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>
					“Adquirir cartuchos remanufacturados ahorra dinero”.				</Text>
			</View>
		</View>
		<Image style={{width:'100%',height:150,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.mito1 } />
		
		<Text style={[mediumText,{color:'#0096d6'}]}>
				<Text style={{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>REALIDAD</Text>{'\n'}
La remanufactura es un proceso manual que puede conducir a daños en el cartucho, lo que ocasiona problemas de desempeño y calidad de impresión. Los cartuchos de tóner originales HP probados funcionaron la primera vez y en todas las ocasiones, mientras que casi el 40% de los cartuchos remanufacturados tenían defectos de fábrica o presentaron fallas.(7)</Text>
		
		
		

				<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',marginBottom:5,marginTop:10}}>
			<View style={{backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#000',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginBottom:5}]}>MITO 02</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText,{fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontWeight:Platform.OS === 'ios' ? '200' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>
					“El rendimiento de páginas de los cartuchos remanufacturados es equiparable al de los cartuchos HP”.</Text>
			</View>
		</View>
		
		<Image style={{width:'100%',height:150,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.mito2 } />
		<Text style={[mediumText,{color:'#0096d6'}]}>
				<Text style={{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>REALIDAD</Text>{'\n'}
HP calcula el rendimiento de sus cartuchos con base en las Normas ISO/ IEC 19752 y 19798. Los cartuchos remanufacturados no especifican bajo qué normas se rigen, por lo tanto, su rendimiento no es comprobable.</Text>




<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',marginBottom:5,marginTop:10}}>
			<View style={{backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#000',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginBottom:5}]}>MITO 03</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText,{fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontWeight:Platform.OS === 'ios' ? '200' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>
					“Los cartuchos remanufacturados son tan confiables como los cartuchos originales HP”.</Text>
			</View>
		</View>
		
		<Image style={{width:'100%',height:150,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.mito3 } />
		<Text style={[mediumText,{color:'#0096d6'}]}>
				<Text style={{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>REALIDAD</Text>{'\n'}
La remanufactura es un proceso manual que puede conducir a daños en el cartucho, lo que ocasiona problemas de desempeño y calidad de impresión. Los cartuchos de tóner originales HP probados funcionaron la primera vez y en todas las ocasiones, mientras que casi el 40% de los cartuchos
remanufacturados tenían defectos de fábrica o presentaron fallas.(7)</Text>
				
				
				<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',marginBottom:5,marginTop:10}}>
			<View style={{backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#000',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginBottom:5}]}>MITO 04</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText,{fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontWeight:Platform.OS === 'ios' ? '200' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>
					“Los cartuchos remanufacturados no dañan tu impresora”.</Text>
			</View>
		</View>
		
		<Image style={{width:'100%',height:150,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.mito4 } />
		<Text style={[mediumText,{color:'#0096d6'}]}>
				<Text style={{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>REALIDAD</Text>{'\n'}
El 80% de los técnicos expresó que las impresoras HP LaserJet que usan cartuchos de tóner remanufacturados
requirieron más limpiezas, reparaciones y reemplazos que aquellas que usan cartuchos originales HP.(8)
Los problemas de confiabilidad asociados con cartuchos remanufacturados pueden afectar tu presupuesto y dañar tu impresora, incluso, de manera permanente.</Text>



<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',marginBottom:5,marginTop:10}}>
			<View style={{backgroundColor:'transparent'}}>
				<Text style={[fontText,{color:'#000',fontSize:17,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginBottom:5}]}>MITO 05</Text>
			</View>
			<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
				<Text style={[smallText,{fontStyle: Platform.OS === 'ios' ? 'italic' : '',fontWeight:Platform.OS === 'ios' ? '200' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_It'}]}>
					“Los cartuchos remanufacturados son mejores para el medio ambiente”.</Text>
			</View>
		</View>
		
		<Image style={{width:'100%',height:150,resizeMode:'cover',marginTop:5,marginBottom:10}} source={ img.mito5 } />
		<Text style={[mediumText,{color:'#0096d6'}]}>
				<Text style={{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>REALIDAD</Text>{'\n'}
Reutilizar un cartucho no es lo mismo que reciclarlo. Es importante tener en cuenta que casi todos los cartuchos remanufacturados terminan en rellenos
sanitarios. Hoy, más del 80% de los cartuchos de tinta HP y el 100% de los cartuchos HP LaserJet se fabrican con material reciclado.(9)</Text>
				
				
				
				
				
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				8 Un estudio de 2015 de LA Market Strategies International, encargado por HP. Resultados basados en 317 encuestas de Socios HP ServiceOne que tienen al menos 6 meses de experiencia en el mantenimiento de impresoras monocromáticas y color LaserJet de HP con cartuchos de tóner HP y de otros proveedores instalados y que lo han hecho dentro de los 12 meses anteriores al estudio. El estudio fue realizado en 6 países: AR, BR, CL, CO, MX, PE. Para
conocer más detalles, consulta marketstrategies.com/hp/LA-Technician2015.pdf
9 La disponibilidad del programa puede variar. Para obtener más información, visita: hp.com/la/reciclar</Text>






				 </View>
				 
				 
				</View>);
		}
		
		

				
				
				
				
		if(params.section==9){
			JSXContent = (<View style={styleContainer}>

				<View style={{flexDirection:'row',marginBottom:10}}>
					<View style={{width:50,height:55,justifyContent:'flex-start',alignItems:'center',backgroundColor:'transparent',marginRight:15}}>
					<Image style={{marginTop:5,width:50,height:50,resizeMode:'contain'}} source={ img.ico_sumi_16 } />
					</View>
					<View style={{flex:1}}>
						<Text style={[titleText,{fontWeight:'400',marginTop:10}]}>Reutilizar un cartucho de tóner {'\n'}
						<Text style={{color:'#0096d6'}}>no es lo mismo que reciclarlo</Text></Text>
					</View>
				</View>
				
				<Text style={mediumText}>
				Los cartuchos remanufacturados pueden usar doce veces más papel para reimpresión que los cartuchos originales HP, que ayudan a generar menos reimpresiones y desperdicio.(10)
{"\n"}{"\n"}
Cuando nuestros clientes compran cartuchos de tóner originales y los reciclan a través del programa <Text style={{color:'#0096d6'}}>HP Planet Partners</Text>, están reduciendo el impacto ambiental,
ya que nuestros productos no se envían
a un relleno sanitario.</Text>


				<View style={{flexDirection:'row',marginBottom:10,marginTop:10}}>
					<View style={{width:50,height:55,justifyContent:'flex-start',alignItems:'center',backgroundColor:'transparent',marginRight:15}}>
					<Image style={{marginTop:5,width:50,height:50,resizeMode:'contain'}} source={ img.ico_sumi_17 } />
					</View>
					<View style={{flex:1,backgroundColor:'transparent'}}>
						<Text style={[titleText,{fontWeight:'400',marginTop:0}]}>HP Planet Partners es un {'\n'}
						<Text style={{color:'#0096d6'}}>programa de devolución y reciclaje mundial</Text></Text>
					</View>
				</View>
				
				<Text style={mediumText}>
				Con más de 25 años de implementación.
				Todos los cartuchos HP devueltos pasan
				por un proceso de reciclaje de varias etapas que separa y purifica la materia prima para luego utilizarla en la fabricación
				de cartuchos originales HP.
				{"\n"}{"\n"}
				El programa HP Planet Partners ha reciclado millones de botellas de plástico y demás objetos para convertirlos en cartuchos nuevos HP.
				{"\n"}{"\n"}
				<Text style={{fontWeight:'400'}}>
				Más del 80% de los cartuchos de tinta HP
				y el 100% de los cartuchos LaserJet
				se fabrican con material reciclado.(11)
				</Text>
				{"\n"}{"\n"}
				Para verificar la disponibilidad en tu país visita {'\n'}<Text style={{color:'#0096d6',textDecorationLine: "underline", textDecorationStyle: "solid", textDecorationColor: "#0096d6"}}>hp.com/la/reciclar</Text>
				</Text>

				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				10 Un estudio de evaluación de ciclo de vida (LCA) de Four Elements Consulting de 2016, encargado por HP, comparó cartuchos de tóner
monocromo originales HP 80A y 83A con una muestra de alternativas remanufacturadas en ocho categorías de impacto ambiental.
Para obtener más información, visita hp.com/go/NA-LJLCA-2016 El estudio de evaluación de ciclo de vida (LCA) aprovecha un estudio de SpencerLab de 2016, encargado por HP, que compara cartuchos de tóner originales HP LaserJet con dos marcas de cartuchos de tóner vendidos en América del Norte. Para obtener más detalles, consulta http://www.spencerlab.com/reports/HPReliability-NA-RM2016.pdf
11 La disponibilidad del programa puede variar. Para obtener más información, visita: hp.com/la/reciclar
				</Text>
				 </View>
				 
				 
				</View>);
		}
		if(params.section==10){
			JSXContent = (<View style={styleContainer}>
				<View style={{flexDirection:'row',margin:0,padding:0}}>
					<View style={{flex:1,margin:0,padding:0}}>
						<Text style={[titleText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Precio accesible{'\n'}
						<Text style={{color:'#0096d6',fontWeight:'400'}}>Reducir costos de impresión</Text></Text>
					</View> 
				</View>
				<View style={{flex:1}}>
					<View><Text style={smallText}>• Evite costos ocultos</Text></View>
					<View><Text style={smallText}>• Ahorre hasta 35%con XLs</Text></View>
					<View><Text style={smallText}>• Ahorre hasta 10%con los multipacks HP</Text></View>
				</View>
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				
				
				<View style={{flexDirection:'row'}}>
					<View style={{flex:1}}>
						<Text style={[titleText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Confiabilidad{'\n'}
						<Text style={{color:'#0096d6',fontWeight:'400'}}>Mayor productividad</Text></Text>
					</View>
				</View>
				<View style={{flex:1}}>
					<View><Text style={smallText}>• La competencia falla 40% de las veces.</Text></View>
					<View><Text style={smallText}>• 81% de los técnicos asegura que la vida útil de la impresora se reduce al usar la competencia</Text></View>
				</View>
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				
				

				<View style={{flexDirection:'row'}}>
					<View style={{flex:1}}>
						<Text style={[titleText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Calidad{'\n'}
						<Text style={{color:'#0096d6',fontWeight:'400'}}>Calidad de impresión uniforme</Text></Text>
					</View>
				</View>
				<View style={{flex:1}}>
					<View><Text style={smallText}>• Más de 60% de las páginas inspeccionadas son de uso limitado. Presentan líneas, borrones o puntos</Text></View>
				</View>
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				
				<View style={{flexDirection:'row'}}>
					<View style={{flex:1}}>
						<Text style={[titleText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Liderazgo medioambiental{'\n'}
						<Text style={{color:'#0096d6',fontWeight:'400'}}>Reduzca su impacto en el medio ambiente</Text></Text>
					</View>
				</View>
				<View style={{flex:1}}>
					<View><Text style={smallText}>• Planet Partners</Text></View>
					
					<View><Text style={smallText}>• 98% de los cartuchos remanufacturados vendidos terminan en un relleno sanitario (4)</Text></View>
					
				</View>
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				
				
				
			
			</View>);
		}
		
		if(params.section==11){
			JSXContent = (<View style={styleContainer}>
					<View style={{flex:1}}>
						<Text style={[titleText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd',marginTop:0}]}>Presentación de los cartuchos de tóner Originales HP con JetIntelligence{'\n'}
						<Text style={{color:'#0096d6'}}>Más. Páginas, rendimiento y protección.</Text></Text>
					</View>
					<Text style={mediumText}>
						Proporcione a su empresa más páginas de calidad profesional,(1) el máximo rendimiento de impresión y la protección adicional de la tecnología antifraude, algo que la competencia simplemente no puede igualar.
					</Text>
					
					
					<View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5,marginTop:20}}>
						<View style={{width:55,backgroundColor:'transparent'}}> 
						<Image style={{width:40,height:40,resizeMode:'contain',marginRight:15}} source={ img.ico_sumi_20 } />
						</View>
						<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
							<Text style={[mediumText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
								Más páginas
							</Text>
						</View>
					</View>
					<Text style={mediumText}>
						Los cartuchos de tóner Originales de HP con JetIntelligence fueron rediseñados para proporcionar más rendimiento ISO y ofrecer más confiabilidad sobre el estado del nivel de tóner.
					</Text>
					<Text style={[smallText,{marginTop:20,color:'#666',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
						Tecnología que maximiza el número de páginas					
					</Text>
					
					   
					   <View style={{width:'100%',backgroundColor:'transparent',marginTop:10}}>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Hasta un 33 % más de páginas por cartucho que las generaciones anteriores de cartuchos de tóner HP (1)</Text></View>
					   		</View>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}><Text style={[smallText]}>Piezas más pequeñas y más resistentes (3)</Text></View>
					   		</View>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}><Text style={[smallText]}>Menos desgaste del cartucho (3)</Text></View>
					   		</View>
					   </View>
					    
					  
					  
					  
					  <Text style={[smallText,{marginTop:20,color:'#666',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
						Tecnología de medición de impresión					
					</Text>
					   <View style={{width:'100%',backgroundColor:'transparent',marginTop:10}}>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Mediciones más confiables(4) para asegurarse de que obtendrá la máxima cantidad de impresiones posible de sus cartuchos</Text></View>
					   		</View>
					   </View>
					   
					   
					    
					   
					   <View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5,marginTop:20}}>
						<View style={{width:55,backgroundColor:'transparent'}}> 
						<Image style={{width:40,height:40,resizeMode:'contain',marginRight:15}} source={ img.ico_sumi_21 } />
						</View>
						<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
							<Text style={[mediumText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
								 Más rendimiento
							</Text>
						</View>
					</View>
					<Text style={mediumText}>
						Tóneres reformulados que admiten más páginas (1) en una impresora más pequeña y más rápida que utiliza menos energía para imprimir una página (2)
					</Text>
					
					
					<Text style={[smallText,{marginTop:20,color:'#666',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
						Tóner ColorSphere (3)				
					</Text>
					   <View style={{width:'100%',backgroundColor:'transparent',marginTop:10}}>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Núcleo con bajo punto de fusión = más velocidad de impresión y la legendaria calidad HP</Text></View>
					   		</View>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Exterior duro = tóner durable para alto rendimiento</Text></View>
					   		</View>
					   </View>
					   
					   <Text style={[smallText,{marginTop:20,color:'#666',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
						Tóner negro de precisión				
					</Text>
					   <View style={{width:'100%',backgroundColor:'transparent',marginTop:10}}>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Forma esférica = más páginas, texto definido, negros vivos y gráficos nítidos</Text></View>
					   		</View>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Diseño encapsulado = menos energía, posibilita una velocidad de impresión más rápida</Text></View>
					   		</View>
					   </View>
					   
					   
	
					   

					   <View style={{flex:1,padding:10,paddingBottom:15,paddingTop:15,backgroundColor:'#f4f7f7',flexDirection:'row',marginBottom:5,marginTop:20}}>
						<View style={{width:55,backgroundColor:'transparent'}}> 
						<Image style={{width:40,height:40,resizeMode:'contain',marginRight:15}} source={ img.ico_sumi_22 } />
						</View>
						<View style={{flex:1,backgroundColor:'transparent',justifyContent:'center'}}>
							<Text style={[mediumText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
								 Más protección
							</Text>
						</View>
					</View>
					<Text style={mediumText}>
						La comunicación segura optimizada entre los cartuchos Originales de HP y las impresoras lo ayuda a obtener la calidad por la que pagó y a evitar problemas potenciales.
					</Text>
					
					
					
					<Text style={[smallText,{marginTop:20,color:'#666',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
						Tecnología antifraude			
					</Text>
					   <View style={{width:'100%',backgroundColor:'transparent',marginTop:10}}>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Lo ayuda a identificar la diferencia entre los cartuchos Originales de HP y los cartuchos usados o falsificados</Text></View>
					   		</View>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Esto respalda la política establecida para sus dispositivos de impresión</Text></View>
					   		</View>
					   </View>
					   <Text style={[smallText,{marginTop:20,color:'#666',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>
						Eliminación automática del sello	
					</Text>
					   <View style={{width:'100%',backgroundColor:'transparent',marginTop:10}}>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Salte un paso: el sello del tóner se elimina automáticamente</Text></View>
					   		</View>
					   		<View style={{width:'100%',flexDirection:'row'}}>
					   			<View style={{width:15}}><Text style={[smallText]}>•</Text></View>
					   			<View style={{flex:1}}>
					   			<Text style={[smallText]}>Comience a imprimir directamente sin demoras</Text></View>
					   		</View>
					   </View>
					   
					   
					   
					   <View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<View style={{flex:1}}>
						<Text style={[titleText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Obtenga más información{'\n'}
						<Text style={{color:'#0096d6'}}>hp.com/go/toner</Text></Text>
					</View>
				
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
					
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				1 Basado en rendimientos de cartuchos ISO/IEC 19752 e ISO/IEC 19798 para los cartuchos de tóner originales HP 131A, 507A, 55X, 80X y 305A/X en comparación con HP 201X, 508X, 87X, 26X
y 410X. Obtenga más información en hp.com/go/learnaboutsupplies.
2 En comparación con la generación anterior de impresoras HP LaserJet.
3 En comparación con la generación anterior de cartuchos de tóner HP LaserJet.
4 En comparación con los medidores de cartuchos de los productos anteriores.
5 Rendimiento declarado en conformidad con ISO/IEC 19798 e impresión continua. Los rendimientos reales varían considerablemente en función de las imágenes impresas y otros factores.
				</Text>
				 </View>
				 
				 
			</View>);
		}
		
		if(params.section==12){
			JSXContent = (<View style={styleContainer}>
			<Text style={[mediumText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Los consumibles de impresión HP se diseñan pensando en el medio ambiente{'\n'}</Text>

					<Text style={mediumText}>Confíe en los cartuchos originales HP para crear el impacto que su empresa merece. Las alternativas pueden hacerle creer que son una buena opción para el medio ambiente, pero una mirada más de cerca
muestra una historia diferente. Aquí le presentamos un resumen de por qué los consumibles originales HP son la eleccion correcta para las mejores prácticas medioambientales.</Text>



			<View style={{flexDirection:'row',marginTop:10}}>
			
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_26 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>HP fabrica los cartuchos usando contenido reciclado</Text>
						<Text style={[smallText,{color:'#666'}]}>
						HP recicla sus cartuchos con un proceso de reciclaje de circuito cerrado en el que los cartuchos que usted recicla mediante el programa HP Planet Partners se usan como materia prima en los cartuchos HP nuevos.
						</Text>
					</View>
				</View>
				<View style={{flexDirection:'row',marginTop:10}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_19 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Impresiones de alta calidad significan menos impresiones desperdiciadas</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Las páginas desperdiciadas tienen un mayor impacto medioambiental en términos de impresión. Los cartuchos de tóner originales HP proporcionan resultados impresionantes y confiables, lo que significa menos reimpresiones y menos desperdicio.(1)
						</Text>
					</View>
				</View>
				<View style={{flexDirection:'row',marginTop:10}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_16 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>HP ofrece reciclaje práctico y gratis</Text>
						<Text style={[smallText,{color:'#666'}]}>
						El programa de reciclaje HP Planet Partners le permite devolver sus cartuchos originales HP con facilidad y sin costo y le ayuda a reducir el impacto de la impresión en el medio ambiente.(1,2)
						</Text>
					</View>
				</View>
				<View style={{flexDirection:'row',marginTop:10}}>
					<View style={{width:90,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_27 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>HP utiliza manufactura de bajo impacto</Text>
						<Text style={[smallText,{color:'#666'}]}>
						HP ha sido premiada por cumplir con sus metas de huella de carbono en todos sus negocios y cadenas de suministro, incluyendo a los proveedores.
						</Text>
					</View>
				</View>

				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>

<Text style={[mediumText,{fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Juntos, estamos reciclando para  un mundo mejor (3){'\n'}</Text>

					<Text style={mediumText}>Con su ayuda, estamos cerrando el círculo: el 100 % de los cartuchos de tóner originales HP y el 80 % de los cartuchos de tinta originales HP ahora se fabrican con contenido reciclado de cartuchos devueltos por clientes como usted.(4) Aquí le mostramos la diferencia que HP hizo en
tres años usando plástico reciclado en los cartuchos de tinta, en lugar de plástico nuevo:</Text>




<View style={{flexDirection:'row',marginTop:10}}>
					<View style={{width:70,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_23 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>De reducción en el consumo de combustible fósil</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Ahorro de más de 120 000 barriles de petróleo (5)						</Text>
					</View>
				</View>
				<View style={{flexDirection:'row',marginTop:10}}>
					<View style={{width:70,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_24 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Menos agua utilizada</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Suficiente para abastecer a 283 millones de hogares durante un día (6)
						</Text>
					</View>
				</View>
				<View style={{flexDirection:'row',marginTop:10}}>
					<View style={{width:70,justifyContent:'flex-start',paddingTop:12}}>
						<View style={{padding:10,marginRight:10,marginLeft:0, backgroundColor:'#f4f7f7'}}>
							<Image style={{width:'100%',height:35,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_25 } />
						</View>
					</View>
					
					<View style={{flex:1}}>
						<Text style={[strongText,{color:'#666',marginBottom:3,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>De reducción de la huella de carbono</Text>
						<Text style={[smallText,{color:'#666'}]}>
						Equivale a sacar 4125 coches de circulación durante un año (7)
						</Text>
					</View>
				</View>
				
				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
				
				<View style={{flex:1,flexDirection:'row'}}>
					<View style={{flex:1}}><Text style={[strongText,{color:'#666',marginBottom:10,fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}]}>Recicle sus cartuchos de tinta y tóner HP con HP Planet Partners,  ¡es fácil y gratis!(2)</Text></View>
					<View style={{width:100,justifyContent:'center'}}>
					<Image style={{width:'100%',height:45,resizeMode:'contain',marginTop:5}} source={ img.ico_sumi_16 } />
					</View>	
				</View>
				<View style={{flex:1}}> 
						<Text style={[smallText,{color:'#666'}]}>
						Con el programa de devolución y reciclaje HP Planet Partners, reciclar sus cartuchos originales HP es más fácil que nunca, con menos impacto para el medio ambiente.
						{'\n'}{'\n'}
						Solicite su recolección en{'\n'}
						<Text style={{color:'#0096d6',fontWeight:Platform.OS === 'ios' ? '600' : '',fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Bd'}}>hp.com/la/reciclar</Text>
						</Text>
					</View>
					

				<View style={{flex:1,marginTop:20,marginBottom:10}}>
					<View style={{width:'100%',height:1,backgroundColor:'#ccc'}}></View>
				</View>
					
				<View style={disclaimer}>
				<Text style={disclaimerText}>
				1 Un estudio de evaluación de ciclo de vida (LCA) de Four Elements Consulting de 2016, encargado por HP, comparó cartuchos de tóner monocromo originales HP 80A y 83A con una muestra
de alternativas remanufacturadas en ocho categorías de impacto ambiental. Para obtener más información, visite hp.com/go/NA-LJLCA-2016. El estudio de evaluación de ciclo de vida de
SpencerLab de 2016, encargado por HP, compara cartuchos de tóner originales HP LaserJet con dos marcas de cartuchos de tóner vendidos en América del Norte. Para obtener más detalles,
consulte www.spencerlab.com/reports/HPReliability-NA-RM2016.pdf (informe de marca).
2 La disponibilidad del programa puede variar. Para obtener más información, visite hp.com/la/reciclar.
3 Para los cartuchos de rPET producidos en el 2013 y posteriormente. Basado en una evaluación del ciclo de vida (LCA) de 2014 realizada por Four Elements Consulting y encargada por HP. El
estudio comparó el impacto ambiental utilizando plástico de polietilenotereftalato (PET) con impacto en el medio ambiente con la utilización de PET reciclado para fabricar los nuevos cartuchos
de tinta originales. Para obtener detalles consulte hp.com/go/recycledplasticslca.
4 El 80 % de los cartuchos de tinta originales HP incluyen de 45 a 70 % de contenido reciclado. El 100 % de los cartuchos de tóner originales HP incluyen de 10 a 33 % de contenido reciclado.
5 Se considera 7,5 barriles de petróleo en una tonelada métrica.
6 Calculado según los datos del Servicio Geológico de los Estados Unidos, http://ga.water.usgs.gov/edu/qahome.html.
7 Calculado con la Calculadora de Equivalencias de Gas de efecto invernadero de la EPA. Para conocer más detalles, consulte epa.gov/cleanenergy/energy-resources/calculator.html.
				</Text>
				 </View>

			</View>);
		}
		
		var btnUp = (<View/>);
			if(this.state.showButtonUp){
				btnUp = (<TouchableOpacity onPress={()=>{ 
					this.scrollView.scrollTo({y: 0})
					
				}} 
				//,bottom:100,right:10
				style={{width:30, height:30,borderRadius:15,justifyContent:'center',alignItems:'center',
					backgroundColor:'rgba(0,0,0,0.5)',position:'absolute',bottom:100,right:15}}>
					<Icon name={'angle-up'} size={15} color="#fff"/>
				</TouchableOpacity>);
			}
		return(<View style={{flex:1}}>
            		<Navbar onBack={()=>{navigation.pop()}} showWinPro={true} onHome={()=>{navigation.navigate('Home')}}/>
            		<View style={{flex:1}}>

						<Image style={{width:'100%',height:150,resizeMode:'cover'}} source={ topimages[params.section-1].top } />

            <View style={{backgroundColor:'#0096d6',width:'100%',paddingTop:12,paddingBottom:7,paddingLeft:15,paddingRight:15}}>
            
             <Text style={{textAlign:'center',color:'white',fontSize:22,lineHeight:20,fontFamily:Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg',marginLeft:25,marginRight:25,backgroundColor:'transparent',padding:3}}>{params.title.toUpperCase()}</Text>
            
 
            </View>
			
			
			<ScrollView style={{flex:1}} contentContainerStyle={{alignItems:'center'}} horizontal={false}
				ref={ref => this.scrollView = ref}
				onScroll={(event)=>{
					if(event.nativeEvent.contentOffset.y>200){
						this.setState({showButtonUp:true});
					}else{
						this.setState({showButtonUp:false});
					}
				}}
			>
				{JSXContent}
			</ScrollView>
			{btnUp}
			<BottomBar onComparar={()=>{
                navigation.navigate('Comparar',{type:type});
            }} onSearch={()=>{
				navigation.navigate('Buscar',{term:'',type:type});
            }}/>
					</View>

		       </View>)
	}
}
