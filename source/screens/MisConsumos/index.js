import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
} from 'react-native';

import { connect } from "react-redux";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Navbar } from "@components";

class MisConsumos extends Component {
	

	render() {
		return (<View style={{flex:1,alignItems:'center',justifyContent:'center',backgroundColor:'#f8d12c'}} >
				 	 
				 	 <Navbar onBack={()=>{ this.props.navigation.goBack(); }} />
				 	 <View style={{flex:1,alignItems:'center',justifyContent:'center'}} >
				 	 	
					</View>
				 	 
			</View>
		);
	}
}
const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});

export default MisConsumos
