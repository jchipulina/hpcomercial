import _Color from "./Color"
export const Color = _Color;

import _Style from "./Style";
export const Style = _Style;

import _Estilo from "./Estilo";
export const Estilo = _Estilo;
