import React, { Component } from "react"
import { View, Text, AsyncStorage, Platform } from 'react-native'

import Reactotron from 'reactotron-react-native'
import "./reactotron"

import { applyMiddleware, compose, createStore } from "redux"
import { Provider } from 'react-redux'
import thunk from "redux-thunk"
import redux from "@redux"
import Estructura from "@navigation"

//global.api = 'http://atomikalperu.com/hp-service/WS_AppHP.asmx';
//global.servicios = 'http://atomikalperu.com/hp-service/HPServicios.svc?wsdl';
	        
//global.api = 'http://atkdigital.com/hp-service/WS_AppHP.asmx';
//global.servicios = 'http://atkdigital.com/hp-service/HPServicios.svc?wsdl';	  

//global.api = 'http://atkdigital.com/hp-service/WS_AppHP.asmx';

global.api = 'http://hpcomercial.marketinghp.net/WS_AppHP.asmx';
global.servicios = 'http://hpcomercial.marketinghp.net/HPServicios.svc?wsdl';	        
	        
	        
global.HPSimplified = Platform.OS === 'ios' ? 'HP Simplified' : 'HPSimplified_Rg';
 
//console.disableYellowBox = true;
const thunks = [ thunk ];

let almacen = null;

if ( __DEV__ ) {
	almacen = Reactotron.createStore(
		redux,
		compose( applyMiddleware( ...thunks ))
	)
}
else {
	almacen = createStore(
		redux,
		compose( applyMiddleware( ...thunks ))
	)
}


class Aplicacion extends React.Component {
	
	constructor(props){
		super(props);
		this.state = {load:false}
	}
	
	checkUser(){
		global.user = null;
		global.saveUser = function(){
			AsyncStorage.setItem("user",JSON.stringify(global.user)).then((value) => { });
		}
		
		//Reactotron.log("getItem");
		
		AsyncStorage.getItem("user").then((value) => {
			global.user = JSON.parse(value);
			//Reactotron.log("RESPONSE "+value);
			
			if(value == null){
				Reactotron.log("USER NULL");
			}else{
				Reactotron.log("USER EXIST");
			}
			this.setState({load:true});
		});
		
	}
	componentWillMount(){
		Reactotron.log("componentWillMount");
		this.checkUser();
	}
	componentDidMount(){
		Reactotron.log("componentDidMount");
		//this.checkUser();
	}
	render() {
		
		if(this.state.load){
			return (
				<Provider store={almacen}>
					<Estructura />
	  			</Provider>
  			);
		}
		return (<View style={{flex:1,backgroundColor:'#168fcc'}}  />);
	}
}

export default ( __DEV__ ? Reactotron.overlay( Aplicacion ) : Aplicacion )
